package com.miercolesdefut.background;

import android.Manifest;
import android.app.IntentService;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.miercolesdefut.ServiceCoronaImp;
import com.miercolesdefut.logic.ServiceCorona;
import com.miercolesdefut.logic.configurations.FacebookUtils;
import com.miercolesdefut.logic.dao.Perfil;
import com.miercolesdefut.view.MainActivity;
import com.miercolesdefut.view.buissnes.alerts.CoronaAlertDialog;
import com.miercolesdefut.view.buissnes.teams.detalle.DetalleEquipoActivoFragment;
import com.google.android.gms.location.LocationRequest;

import java.util.List;

import pl.charmas.android.reactivelocation.ReactiveLocationProvider;
import rx.functions.Action1;

/**
 * Created by lauro on 10/02/2017.
 */

public class CheckinService extends IntentService {

    public CheckinService()
    {
        super("CheckingService");
    }

    //private List<com.corona.logic.dao.gps.Location> locations;
    private int maximaDistancia = 200;
    @Override
    protected void onHandleIntent(Intent intent) {
        ServiceCorona serviceCorona = new ServiceCoronaImp(getApplicationContext());
        int permissionCheck = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION);
        if(permissionCheck != PackageManager.PERMISSION_GRANTED){
            return;
        }
        serviceCorona.getReferenciaUsuario(FacebookUtils.getId(getApplicationContext()), response->{
            if(response.isSuccessful()) {
                trackPosition(currenLocation->{
                    List<com.miercolesdefut.logic.dao.gps.Location> locations = response.body();
                    for(com.miercolesdefut.logic.dao.gps.Location locationFor: locations){
                        Location referencesTeam = new Location("location");
                        referencesTeam.setLatitude(locationFor.getLatitude());
                        referencesTeam.setLongitude(locationFor.getLongitude());
                        if(currenLocation.distanceTo(referencesTeam) < maximaDistancia){
                            Log.i("","En el Rango");
                        } else {
                            Log.i("","Fuera de rango");
                        }
                    }
                });
            }
        });

        /*serviceCorona.getDetalleUsuario(FacebookUtils.getId(getApplicationContext()),response->{
            if(response.isSuccessful()){
                Perfil perfil = response.body();
                sendBroadcastPerfil(perfil);
                if(!perfil.hasMakeCheck()){
                    //Log.i("CheckingService", "has no make checking !!");
                    for(DetailMatchPerfil detailMatch :perfil.getMatches()){
                        if(detailMatch.getTeams() == null){
                            continue;
                        }
                        for(CheckedTeams team: detailMatch.getTeams()){
                            if(team.hasCheckedMembers()){
                                trackPosition(location->{
                                    Reference reference = new Reference();
                                    reference.setFacebook_id(FacebookUtils.getId(getApplicationContext()));
                                    reference.setTeam_id(team.getTeam().getId());
                                    reference.setMatch_id(detailMatch.getMatch().getId());
                                    reference.setLatitude(String.valueOf(location.getLatitude()));
                                    reference.setLongitude(String.valueOf(location.getLongitude()));
                                    serviceCorona.setCheckIn(reference,checkInResponse->{
                                        if(checkInResponse.isSuccessful()){
                                            CheckPointResult ckeckPoint = checkInResponse.body();
                                            sendBroadcast(ckeckPoint.reference.team.getName(), ckeckPoint.points.total);
                                            //Log.i("CheckingService", "Checking con exito.");
                                        } else{
                                            Intent updateLocation = new Intent(DetalleEquipoActivoFragment.UpdateLocation.CURRENT_LOCATION_FROM_BACKGROUND);
                                            getApplicationContext().sendBroadcast(updateLocation);
                                            //Log.i("CheckingService", "Checking invalido");
                                        }
                                    });
                                });
                            }
                        }
                    }
                }
            }
        });*/
        //Log.i("CheckingService", "Service running");
    }

    private void sendBroadcast(String teamName, int point){
        Intent msgIntent = new Intent();
        msgIntent.putExtra(CoronaAlertDialog.ACTION_AUTO_CHECKING_TEAM, teamName);
        msgIntent.putExtra(CoronaAlertDialog.ACTION_AUTO_CHECKING_POINTS, String.valueOf(point));
        msgIntent.setAction(MainActivity.CheckingRequestReceiver.PROCESS_CHECKING);
        sendBroadcast(msgIntent);
    }

    private void sendBroadcastPerfil(Perfil perfil){
        Intent msgIntent = new Intent();
        msgIntent.putExtra(DetalleEquipoActivoFragment.KEY_PERFIL_UPDATE, perfil);
        msgIntent.setAction(DetalleEquipoActivoFragment.PROCCESS_DETAIL_PERFIL_RECEIVER);
        sendBroadcast(msgIntent);
    }

    private void trackPosition(Action1<Location> action1){
        //showDialog();
        LocationRequest req= LocationRequest
                .create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setNumUpdates(1);
        ReactiveLocationProvider locationProvider = new ReactiveLocationProvider(getApplicationContext());
        locationProvider.getUpdatedLocation(req)
                .subscribe(action1);
    }
}
