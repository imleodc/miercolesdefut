package com.miercolesdefut.background;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by lauro on 10/02/2017.
 */

public class CheckinReceiver extends BroadcastReceiver {

    public static final int REQUEST_CODE = 12345;
    public static final String ACTION = "com.corona.Ckeckin.alarm";

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent i = new Intent(context, CheckinService.class);
        context.startService(i);
    }
}
