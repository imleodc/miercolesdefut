package com.miercolesdefut.logic;

import android.content.Context;

import com.miercolesdefut.logic.dao.UserDataDAO;
import com.miercolesdefut.logic.dao.android.UserDataDAOAndroid;

import java.util.Date;

/**
 * Created by lauro on 30/12/2016.
 */

public class ServiceUserImp implements ServiceUser {

    UserDataDAO userData;

    public ServiceUserImp(Context context){
        userData = new UserDataDAOAndroid(context);
    }
    @Override
    public boolean hasBirthday() {
        //return true ;
        return userData.getBirthday() != null;
    }

    @Override
    public boolean hasFBId() {
        return false;
    }

    @Override
    public void saveBirthday(Date date) {
        userData.saveBirthDay(date);
    }

    @Override
    public Date getBirthday() {
        return userData.getBirthday();
    }

    @Override
    public void clear() {
        userData.clear();
    }
}
