package com.miercolesdefut.logic.model;

/**
 * Created by lauro on 24/01/2017.
 */

public interface UpdaterListData<T> {
    void addItemsToList(T data);
    void clearList();
}
