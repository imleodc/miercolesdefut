package com.miercolesdefut.logic.model.gps;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import com.miercolesdefut.logic.configurations.TestNotificationGPS;
import com.miercolesdefut.view.user.location.LocationService;

/**
 * Created by lauro on 15/01/2017.
 */

public class NotificationPartido extends IntentService {
    public static final String LAUCH_BY_NOTIFICATION = "lauch_by_notification";
    private static final int  NOTIFICATION_PARTIDO_ID = 10001;
    public NotificationPartido(){
        super(NotificationPartido.class.getName());
    }
    @Override
    protected void onHandleIntent(Intent intent) {
        NotificationPartido.sendNotificationPartido(getApplicationContext(),"Notification 1 ", "Nuevo Partido");
        TestNotificationGPS.completeWakefulIntent(intent);
    }

    public static void sendNotificationPartido(Context context, String nombre, String message){
        NotificationManager nm = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        PendingIntent pIntent = PendingIntent.getActivity(
                context,
                0,
                new Intent(
                        context,
                        LocationService.class
                ).putExtra(LAUCH_BY_NOTIFICATION,true),
                PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder nBuilder =  new NotificationCompat.Builder(context)
                .setSmallIcon(android.support.design.R.drawable.abc_ic_go_search_api_material)
                .setContentTitle(nombre)
                .setStyle(
                        new NotificationCompat.BigTextStyle()
                        .bigText(nombre)
                ).setContentText(message)
                .setVibrate(new long[]{200l,500l,300l});
        nBuilder.setContentIntent(pIntent);
        nBuilder.setAutoCancel(true);
        nm.notify(NOTIFICATION_PARTIDO_ID, nBuilder.build());
    }
}
