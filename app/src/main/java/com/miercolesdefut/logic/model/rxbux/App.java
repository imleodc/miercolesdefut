package com.miercolesdefut.logic.model.rxbux;

import android.app.Application;
import android.telephony.TelephonyManager;

import com.appsflyer.AppsFlyerLib;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.miercolesdefut.R;
import com.miercolesdefut.logic.dao.Matche;
import com.miercolesdefut.logic.dao.Perfil;

import java.util.List;

public class App extends Application {
    private static final String DEV_KEY = "336y9YKemowbGveMN6pfkB";
    private static App instance;

    private ProfileUpdateModel profileUpdate;
    private MatchUpdateModel matchUpdate;
    private Perfil perfileCache;
    private List<Matche> matchesCache;

    private static GoogleAnalytics sAnalytics;
    private static Tracker sTracker;

    /**
     * Gets the default {@link Tracker} for this {@link Application}.
     * @return tracker
     */
    synchronized public Tracker getDefaultTracker() {
        // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
        if (sTracker == null) {
            sTracker = sAnalytics.newTracker(R.xml.global_tracker);
        }

        return sTracker;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        profileUpdate = new ProfileUpdateModel();
        matchUpdate = new MatchUpdateModel();
        sAnalytics = GoogleAnalytics.getInstance(this);


        AppsFlyerLib.getInstance().setCollectAndroidID(true);
        AppsFlyerLib.getInstance().setCollectIMEI(true);

        AppsFlyerLib.getInstance().startTracking(this, DEV_KEY);
    }

    public static App get(){
        return instance;
    }

    public ProfileUpdateModel busProfile(){
        return profileUpdate;
    }

    public MatchUpdateModel busMatch(){
        return matchUpdate;
    }


    public Perfil getPerfileCache() {
        return perfileCache;
    }

    public void setPerfileCache(Perfil perfileCache) {
        this.perfileCache = perfileCache;
    }

    public List<Matche> getMatchesCache() {
        return matchesCache;
    }

    public void setMatchesCache(List<Matche> matchesCache) {
        this.matchesCache = matchesCache;
    }

    public void updateWithCache(){
        if(perfileCache != null){
            profileUpdate.send(perfileCache);
        }
        if(matchesCache != null){
            matchUpdate.send(matchesCache);
        }
    }

}
