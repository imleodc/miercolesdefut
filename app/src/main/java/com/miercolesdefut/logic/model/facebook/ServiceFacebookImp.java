package com.miercolesdefut.logic.model.facebook;

import android.content.Context;
import android.os.Bundle;

import com.miercolesdefut.logic.configurations.FacebookUtils;
import com.miercolesdefut.logic.dao.FriendDAO;
import com.miercolesdefut.logic.dao.Picture;
import com.miercolesdefut.logic.dao.android.FriendDAOFB;
import com.miercolesdefut.logic.model.DataUser;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import rx.functions.Action1;

/**
 * Created by lauro on 13/01/2017.
 */

public class ServiceFacebookImp implements ServiceFacebook {
    CallbackManager callbackManager;
    @Override
    public void initializeFacebook(Context context) {
        if(!FacebookSdk.isInitialized() ){
            FacebookSdk.sdkInitialize(context);
        }
        callbackManager = CallbackManager.Factory.create();
    }

    @Override
    public CallbackManager getCallBackManager() {
        return callbackManager;
    }

    public void registerPermissions(LoginButton login,List permissions){
        //Arrays.asList("user_friends")
        login.setReadPermissions(permissions);
        //login.setPublishPermissions(permissions);
    }

    @Override
    public void registerCallBack(LoginButton login, FacebookCallback<LoginResult> facebookCallback) {
        login.registerCallback(callbackManager, facebookCallback);
    }

    @Override
    public void callFacebookFriends(Context context,final Action1<List<FriendDAO>> action) {
        final AccessToken token = FacebookUtils.restoreCredentials(context);
        GraphRequest.newMyFriendsRequest(
                token,
                new GraphRequest.GraphJSONArrayCallback(){
                    @Override
                    public void onCompleted(final JSONArray objects, GraphResponse response) {
                        if(objects == null){
                            return;
                        }
                        if(objects.length() > 0){
                            List<FriendDAO> friends = new ArrayList<>();
                            for(int i = 0; i < objects.length(); i ++ ) {
                                try {
                                    FriendDAO friend = new FriendDAOFB(objects.getJSONObject(i));
                                    friends.add(friend);
                                    completeFriendPicture(token,friend.getId(), friend);//getFriendPicture(token, friend);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            Collections.sort(friends, (o1, o2) -> o1.getName().compareTo(o2.getName()));
                            action.call(friends);
                        }
                    }
                }).executeAsync();
    }

    public void getFriendPicture(AccessToken token, final FriendDAO friend) {
        Bundle parametros = new Bundle();
        parametros.putString("redirect", "0");
        new GraphRequest(
                token, "/"+friend.getId()+"/picture",
                parametros, HttpMethod.GET,
                new GraphRequest.Callback(){
                    @Override
                    public void onCompleted(GraphResponse response) {
                        try {
                            ((FriendDAOFB)friend).setPicture(response.getJSONObject().getJSONObject("data").getString("url"));
                            //
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
        ).executeAsync();
    }

    @Override
    public void callFacebookDataUser(Context context, final Action1<DataUser> action){
        GraphRequest request = GraphRequest.newMeRequest(
                FacebookUtils.restoreCredentials(context),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        try {
                            action.call(new DataUser(object));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch(NullPointerException e) {
                            e.printStackTrace();
                        }
                    }
                }
        );
        Bundle parameters = new Bundle();
        parameters.putString("fields", "");
        request.setParameters(parameters);
        request.executeAsync();
    }

    @Override
    public void completeFriendPicture(AccessToken token, String idfb, final Picture picture) {
        Bundle parametros = new Bundle();
        parametros.putString("redirect", "0");
        new GraphRequest(
                token, "/"+idfb+"/picture",
                parametros, HttpMethod.GET,
                new GraphRequest.Callback(){
                    @Override
                    public void onCompleted(GraphResponse response) {
                        try {
                            if(picture != null) {
                                if(response == null || response.getJSONObject() == null || response.getJSONObject().getJSONObject("data")==null){
                                    return;
                                }
                                picture.setPicture(response.getJSONObject().getJSONObject("data").getString("url"));
                            }
                            //
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
        ).executeAsync();
    }
}
