package com.miercolesdefut.logic.model;

import com.miercolesdefut.logic.dao.AcceptTeam;
import com.miercolesdefut.logic.dao.DeclineTeam;
import com.miercolesdefut.logic.dao.HistoriUser;
import com.miercolesdefut.logic.dao.Invitacion;
import com.miercolesdefut.logic.dao.InvitarAEquipo;
import com.miercolesdefut.logic.dao.Invited;
import com.miercolesdefut.logic.dao.Matche;
import com.miercolesdefut.logic.dao.Perfil;
import com.miercolesdefut.logic.dao.PuntosPorCompartir;
import com.miercolesdefut.logic.dao.Reference;
import com.miercolesdefut.logic.dao.Registro;
import com.miercolesdefut.logic.dao.Team;
import com.miercolesdefut.logic.dao.TeamStatusMembers;
import com.miercolesdefut.logic.dao.gps.CheckPointResult;
import com.miercolesdefut.logic.dao.gps.Location;

import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by lauro on 22/01/2017.
 */

public interface CoronaAPIService {
    @GET("api/matches")
    Call<List<Matche>> getMatches();

    @GET("api/matches/{facebookId}")
    Call<List<Matche>> getMatches(@Path("facebookId") String facebookId);

    @POST("api/sign-up")
    Call<ResponseBody> registrar(@Body Registro registro);

    @POST("api/team")
    Call<ResponseBody> createTeam(@Body Team team);

    @GET("api/user/invitations/{facebookId}")
    Call<List<Invitacion>> getInvitacionesPendientes(@Path("facebookId") String id);

    @POST("api/team/accept")
    Call<ResponseBody> acceptTeam(@Body AcceptTeam acceptTeam);

    @POST("api/team/decline")
    Call<ResponseBody> declineTeam(@Body DeclineTeam declineTeam);

    @GET("api/user/teams/{facebookId}")
    Call<List<Team>> getTeams(@Path("facebookId") String fbId);

    @POST("api/set-reference")
    Call<ResponseBody> setPuntoReferencia(@Body Reference reference);

    @POST("api/check-in")
    Call<CheckPointResult> setCheckIn(@Body Reference reference);

    @POST("api/team/points")
    Call<HistoriUser> getHistoriPoints(@Body AcceptTeam acceptTeam);

    @POST("api/team/close")
    Call<ResponseBody> setCloseTeam(@Body AcceptTeam team);

    @POST("api/team/invite")
    Call<ResponseBody> inviteToTeam(@Body InvitarAEquipo invitar);

    @POST("api/team/pending")
    Call<TeamStatusMembers> pendingInvitationTeam(@Query("team_id") int idTeam);

    @GET("api/user/{facebookId}")
    Call<Perfil> getProfileUser(@Path("facebookId")String facebookId);

    @POST("api/team/can-be-invited")
    Call<Map<String, Boolean>> canBeInvited(@Body Invited invited);

    @GET("api/user/references/{facebook_id}")
    Call<List<Location>> getReferencesUser(@Path("facebook_id") String facebookId);

    @Multipart
    @POST("api/share")
    Call<PuntosPorCompartir> sharePhoto(@PartMap() Map<String, RequestBody> partMap, @Part MultipartBody.Part file);

    @POST("api/share/post")
    Call<ResponseBody> confirmPostPhoto(@Query("facebook_id") String facebookId, @Query("match_id") int matchId, @Query("team_id") int teamId, @Query("post_id") String postId);
}
