package com.miercolesdefut.logic.model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by lauro on 17/01/2017.
 */

public class DataUser {
    private String nombre;
    private String email;
    private String telefono;
    private Estados estado;
    public DataUser(){}

    public DataUser(JSONObject data) throws JSONException{
        String nombreLoc = null, apellido = null;
        if(data.has("first_name")){
            nombreLoc = data.getString("first_name");
        }
        if(data.has("last_name")){
            apellido = data.getString("last_name");
        }
        StringBuilder nombreBld =new StringBuilder("");
        if(nombreLoc != null) {
            nombreBld.append(nombreLoc).append(" ");
        }
        if(apellido != null) {
            nombreBld.append(apellido);
        }
        if(data.has("name")){
            nombreBld.append(data.getString("name"));
        }
        nombre = nombreBld.toString();
        if(data.has("email")){
            email = data.getString("email");
        }
    }


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Estados getEstado() {
        return estado;
    }

    public void setEstado(Estados estado) {
        this.estado = estado;
    }
}
