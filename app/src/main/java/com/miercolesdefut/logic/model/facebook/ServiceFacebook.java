package com.miercolesdefut.logic.model.facebook;

import android.content.Context;

import com.miercolesdefut.logic.dao.FriendDAO;
import com.miercolesdefut.logic.dao.Picture;
import com.miercolesdefut.logic.model.DataUser;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import java.util.List;

import rx.functions.Action1;

/**
 * Created by lauro on 13/01/2017.
 */

public interface ServiceFacebook {
    void initializeFacebook(Context context);
    CallbackManager getCallBackManager();
    void registerPermissions(LoginButton login,List permissions);
    void registerCallBack(LoginButton loginButton, FacebookCallback<LoginResult> facebookCallback);
    void callFacebookFriends(Context context, Action1<List<FriendDAO>> action);
    void callFacebookDataUser(Context context,Action1<DataUser> action);
    void completeFriendPicture(AccessToken token, String idfb, Picture picture);
}
