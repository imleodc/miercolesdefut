package com.miercolesdefut.logic.model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by lauro on 11/01/2017.
 */

public class Estados {
    private int id;
    private String nombre;
    private String slug;

    public Estados(JSONObject data) throws JSONException {
        id = data.getInt("id");
        nombre = data.getString("name");
        slug = data.getString("slug");
    }

    public Estados(int id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return nombre;
    }

    public String getSlug() {
        return slug;
    }
}
