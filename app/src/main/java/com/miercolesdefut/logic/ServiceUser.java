package com.miercolesdefut.logic;

import java.util.Date;

/**
 * Created by lauro on 30/12/2016.
 */

public interface ServiceUser  {
    boolean hasBirthday();
    boolean hasFBId();
    void saveBirthday(Date date);
    Date getBirthday();
    void clear();
}
