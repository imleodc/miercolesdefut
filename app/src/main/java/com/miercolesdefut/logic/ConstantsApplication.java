package com.miercolesdefut.logic;

import android.support.v4.app.Fragment;

/**
 * Created by lauro on 19/02/2017.
 */

public class ConstantsApplication {

    private boolean createTeams = true;
    private Fragment  fragmentDialog;
    private boolean makeChecking = false;

    private static ConstantsApplication instance;
    private ConstantsApplication(){}

    public static ConstantsApplication getInstances(){
        if(instance == null) {
            instance = new ConstantsApplication();
        }
        return instance;
    }

    public boolean isCreateTeams() {
        return createTeams;
    }

    public void setCreateTeams(boolean createTeams) {
        this.createTeams = createTeams;
    }


    public Fragment getFragmentDialog() {
        return fragmentDialog;
    }

    public void setFragmentDialog(Fragment fragmentDialog) {
        this.fragmentDialog = fragmentDialog;
    }

    public void setMakeChecking(boolean makeChecking){
        this.makeChecking = makeChecking;
    }

    public boolean isMakeChecking(){
        return makeChecking;
    }
}
