package com.miercolesdefut.logic;

import com.miercolesdefut.logic.dao.AcceptTeam;
import com.miercolesdefut.logic.dao.DeclineTeam;
import com.miercolesdefut.logic.dao.HistoriUser;
import com.miercolesdefut.logic.dao.Invitacion;
import com.miercolesdefut.logic.dao.InvitarAEquipo;
import com.miercolesdefut.logic.dao.Invited;
import com.miercolesdefut.logic.dao.Matche;
import com.miercolesdefut.logic.dao.Perfil;
import com.miercolesdefut.logic.dao.PuntosPorCompartir;
import com.miercolesdefut.logic.dao.Reference;
import com.miercolesdefut.logic.dao.Registro;
import com.miercolesdefut.logic.dao.Team;
import com.miercolesdefut.logic.dao.TeamStatusMembers;
import com.miercolesdefut.logic.dao.gps.CheckPointResult;
import com.miercolesdefut.logic.dao.gps.Location;

import java.io.File;
import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Response;
import rx.functions.Action1;

/**
 * Created by lauro on 23/01/2017.
 */

public interface ServiceCorona {
    void registroUsuario(Registro registro, Action1<Response<ResponseBody>> action);
    void registraEquipo(Team equipo, Action1<Response<ResponseBody>> action);
    void getInvitaciones(String idFacebook,Action1<Response<List<Invitacion>>> action);
    void aceptarInvitacion(AcceptTeam acceptTeam, Action1<Response<ResponseBody>> action);
    void cancelarInvitacion(DeclineTeam declineTeam, Action1<Response<ResponseBody>> action);
    void getEquipos(String idFb, Action1<Response<List<Team>>> action);
    List<Team> getEquipos(String idFb);
    void getPartidos(Action1<Response<List<Matche>>> action);
    void getPartidos(String facebookId,Action1<Response<List<Matche>>> action);
    List<Matche> getPartidos();
    List<Matche> getPartidos(String facebookId);
    void setCheckIn(Reference reference,Action1<Response<CheckPointResult>> action);
    void setReference(Reference reference,Action1<Response<ResponseBody>> action);
    void getPartidosPuntos(AcceptTeam acceptTeam, Action1<Response<HistoriUser>> action1);
    void cerrarEquipo(AcceptTeam acceptTeam, Action1<Response<ResponseBody>> action);
    void invitarAEquipo(InvitarAEquipo invitar, Action1<Response<ResponseBody>> action);
    void invitacionesPendientesEquipo(int equipoId, Action1<Response<TeamStatusMembers>> action);
    void getDetalleUsuario(String facebookId, Action1<Response<Perfil>> action);
    Response<Perfil> getDetalleUsuario(String facebookId);
    void canBeInvited(Invited invited, Action1<Response<Map<String, Boolean>>> action);
    void getReferenciaUsuario(String facebookIs, Action1<Response<List<Location>>> action);
    void sharePhoto(String facebookId, int matchId, int teamId, double latitude, double longitude, byte[] photo, Action1<Response<PuntosPorCompartir>> action);
    void sharePhoto(String facebookId, int matchId, int teamId, double latitude, double longitude, File photo, Action1<Response<PuntosPorCompartir>> action);
    void confirmPostPhoto(String facebookId, int matchId, int teamId, String postId, Action1<Response> actions);
}
