package com.miercolesdefut.logic.configurations;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

import com.miercolesdefut.logic.model.gps.NotificationPartido;

/**
 * Created by lauro on 15/01/2017.
 */

public class TestNotificationGPS extends WakefulBroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent notificationPartido = new Intent(context, NotificationPartido.class);
        startWakefulService(context, notificationPartido);
    }
}
