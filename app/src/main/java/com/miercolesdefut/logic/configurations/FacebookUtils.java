package com.miercolesdefut.logic.configurations;

import android.content.Context;
import android.content.SharedPreferences;

import com.miercolesdefut.R;
import com.facebook.AccessToken;

/**
 * Created by lauro on 11/01/2017.
 */

public class FacebookUtils {
    private static final String FB_KEY ="fb-creedentials";
    private static final String FB_TOKEN ="fb-token";
    private static final String FB_USER_ID ="fb-user-id";
    private static AccessToken token;

    public static boolean isLogin(Context context) {
        return restoreCredentials(context) != null;
    }

    public static String getId(Context context){
        AccessToken token = restoreCredentials(context);
        return token.getUserId();
    }

    public static boolean saveCredentials(Context context, AccessToken accessToken){
        SharedPreferences.Editor editor = context.getSharedPreferences(FB_KEY,Context.MODE_PRIVATE).edit();
        editor.putString(FB_TOKEN, accessToken.getToken());
        editor.putString(FB_USER_ID, accessToken.getUserId());
        return editor.commit();
    }

    public static AccessToken restoreCredentials(Context context){
        AccessToken accessToken;
        if(context == null) {
            return null;
        }
        SharedPreferences shp = context.getSharedPreferences(FB_KEY, Context.MODE_PRIVATE);
        String token = shp.getString(FB_TOKEN, null);
        if(token == null) {
            return null;
        }
        accessToken = new AccessToken(
                token,
                context.getResources().getString(R.string.facebook_app_id),
                shp.getString(FB_USER_ID, null),null, null, null, null, null);
        return accessToken;
    }
}
