package com.miercolesdefut.logic.configurations;

import com.miercolesdefut.logic.configurations.interfaces.Message;

/**
 * Created by lauro on 10/01/2017.
 */

public final class CustomMessage implements Message{
    private String title;
    private String content;

    public CustomMessage(String title, String content){
        this.title = title;
        this.content = content;
    }

    public CustomMessage(String content){
        this(null, content);
    }
    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getContent() {
        return content;
    }
}
