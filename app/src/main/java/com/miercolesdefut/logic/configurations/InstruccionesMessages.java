package com.miercolesdefut.logic.configurations;

import com.miercolesdefut.logic.configurations.interfaces.Message;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lauro on 10/01/2017.
 */

public class InstruccionesMessages {
    private static final InstruccionesMessages instructions = new InstruccionesMessages();
    public List<Message> messages = new ArrayList<>();

    public void init(){
        messages.add(new CustomMessage("1","Inicia sesión con tu cuenta de Facebook y completa tus datos."));
        messages.add(new CustomMessage("2","Acepta los fichajes de tus amigos para pertenecer a su equipo o crea el tuyo poniéndole nombre y ficha a tus amigos."));
        messages.add(new CustomMessage("3","Si tus amigos no han descargado la app, invítalos antes de que puedas ficharlos."));
        messages.add(new CustomMessage("4","Reúnete y hagan check-in. Con 3 o + personas reunidas acumulan puntos por cada asistente.\nGanen puntos extra si todo el equipo se junta."));
        messages.add(new CustomMessage("5","Tómense una foto y compártanla en Facebook para obtener más puntos."));
        messages.add(new CustomMessage("6","¡El equipo que reúna más puntos para el final de la promoción ganará un viaje a Rusia!"));
    }

    public int getLength(){
        return messages.size();
    }

    public Message getMessage(int i){
        return messages.get(i);
    }

    private InstruccionesMessages(){
        init();
    }

    public InstruccionesMessages(boolean complete){
        messages.add(new CustomMessage("1", "Acepta los fichajes de tus amigos para pertenecer a su equipo o crea el tuyo poniéndole nombre y ficha a tus amigos."));
        messages.add(new CustomMessage("2", "Si tus amigos no han descargado la app, invitalos antes de que puedas ficharlos. Cuando todos hayan entrado a la app y aceptado el fichaje, cierra tu equipo."));
        messages.add(new CustomMessage("3", "Reúnanse y hagan check-in.\nGanen puntos extra si todo el equipo se junta y también por ver los partidos en los siguientes lugares."));
        messages.add(new CustomMessage("4", "Tómense una foto y compártanla en facebook para obtener más puntos."));
        //messages.add(new CustomMessage("5", "¡El equipo que reúna más puntos para el final de la promoción ganará un viaje a Rusia!"));
        messages.add(new CustomMessage("", ""));
    }

    public static final InstruccionesMessages getInstances(){
        return instructions;
    }

}
