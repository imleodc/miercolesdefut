package com.miercolesdefut.logic.configurations.interfaces;

/**
 * Created by lauro on 10/01/2017.
 */

public interface Message {
    String getTitle();
    String getContent();
}
