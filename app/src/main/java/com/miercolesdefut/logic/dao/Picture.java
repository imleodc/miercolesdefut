package com.miercolesdefut.logic.dao;

/**
 * Created by lauro on 24/01/2017.
 */

public interface Picture {
    void setPicture(String url);
    String getPicture();
}
