package com.miercolesdefut.logic.dao;

/**
 * Created by lauro on 31/01/2017.
 */

public class InvitarAEquipo {
    private String facebook_id;
    private int team_id;
    private String invited;

    public String getFacebook_id() {
        return facebook_id;
    }

    public void setFacebook_id(String facebook_id) {
        this.facebook_id = facebook_id;
    }

    public int getTeam_id() {
        return team_id;
    }

    public void setTeam_id(int team_id) {
        this.team_id = team_id;
    }

    public String getInvited() {
        return invited;
    }

    public void setInvited(String invited) {
        this.invited = invited;
    }
}
