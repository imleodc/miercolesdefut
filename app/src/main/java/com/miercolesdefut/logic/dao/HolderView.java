package com.miercolesdefut.logic.dao;

import android.support.v7.widget.RecyclerView;

/**
 * Created by lauro on 24/01/2017.
 */

public interface HolderView {
    void setHolder(RecyclerView.ViewHolder holder);
}
