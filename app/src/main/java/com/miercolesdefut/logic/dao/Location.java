package com.miercolesdefut.logic.dao;

import java.io.Serializable;

/**
 * Created by lauro on 26/01/2017.
 */

public class Location implements Serializable{
    private String total;
    private String location;
    private Double latitude;
    private Double longitude;
    private String start_date;
    private String start_date_format;
    private String start_time_format;
    private String finish_date;
    private String finish_date_format;
    private String end_time_format;

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getStart_date_format() {
        return start_date_format;
    }

    public void setStart_date_format(String start_date_format) {
        this.start_date_format = start_date_format;
    }

    public String getStart_time_format() {
        return start_time_format;
    }

    public void setStart_time_format(String start_time_format) {
        this.start_time_format = start_time_format;
    }

    public String getFinish_date() {
        return finish_date;
    }

    public void setFinish_date(String finish_date) {
        this.finish_date = finish_date;
    }

    public String getFinish_date_format() {
        return finish_date_format;
    }

    public void setFinish_date_format(String finish_date_format) {
        this.finish_date_format = finish_date_format;
    }

    public String getEnd_time_format() {
        return end_time_format;
    }

    public void setEnd_time_format(String end_time_format) {
        this.end_time_format = end_time_format;
    }
}
