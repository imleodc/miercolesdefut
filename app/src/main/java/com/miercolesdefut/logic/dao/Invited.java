package com.miercolesdefut.logic.dao;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by lauro on 11/02/2017.
 */

public class Invited implements Serializable{
    @SerializedName("facebook_id")
    private String facebookId;
    @SerializedName("ids")
    private List<String> friensIds;

    public String getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public List<String> getFriensIds() {
        if(friensIds == null){
            friensIds = new ArrayList<>();
        }
        return friensIds;
    }

    public void setFriensIds(List<String> friensIds) {
        this.friensIds = friensIds;
    }
}
