package com.miercolesdefut.logic.dao.android;

import com.miercolesdefut.logic.dao.MetaDataDAO;
import com.miercolesdefut.logic.model.Estados;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lauro on 11/01/2017.
 */

public class MetaDataDAOAndroid implements MetaDataDAO{
    private static final String ESTADOS_JSON = "[                                    "+
            "  {                                  "+
            "    'id': 1,                         "+
            "    'name': 'Aguascalientes',    " +
            "     'slug':'aguascalientes'     "+
            "  },                                 "+
            "  {                                  "+
            "    'id': 2,                         "+
            "    'name': 'Baja California' " +
            "    ,'slug':'baja-california'        "+
            "  },                                 "+
            "  {                                  "+
            "    'id': 3,                         "+
            "    'name': 'Baja California Sur'    " +
            "    ,'slug': 'baja-california-sur'   "+
            "  },                                 "+
            "  {                                  "+
            "    'id': 4,                         "+
            "    'name': 'Campeche'               " +
            "    ,'slug':'campeche'  "+
            "  },                                 "+
            "  {                                  "+
            "    'id': 5,                         "+
            "    'name': 'Coahuila'               "+
            "    ,'slug':'coahuila'  "+
            "  },                                 "+
            "  {                                  "+
            "    'id': 6,                         "+
            "    'name': 'Colima'                 "+
            "    ,'slug':'colima'  "+
            "  },                                 "+
            "  {                                  "+
            "    'id': 7,                         "+
            "    'name': 'Chiapas'                "+
            "    ,'slug':'chiapas'  "+
            "  },                                 "+
            "  {                                  "+
            "    'id': 8,                         "+
            "    'name': 'Chihuahua'              "+
            "    ,'slug':'chihuahua'  "+
            "  },                                 "+
            "  {                                  "+
            "    'id': 9,                         "+
            "    'name': 'Ciudad de Mexico'       "+
            "    ,'slug':'ciudad-de-mexico'  "+
            "  },                                 "+
            "  {                                  "+
            "    'id': 10,                        "+
            "    'name': 'Durango'                "+
            "    ,'slug':'durango'  "+
            "  },                                 "+
            "  {                                  "+
            "    'id': 11,                        "+
            "    'name': 'Guanajuato'             "+
            "    ,'slug':'guanajuato'  "+
            "  },                                 "+
            "  {                                  "+
            "    'id': 12,                        "+
            "    'name': 'Guerrero'               "+
            "    ,'slug':'guerrero'  "+
            "  },                                 "+
            "  {                                  "+
            "    'id': 13,                        "+
            "    'name': 'Hidalgo'                "+
            "    ,'slug':'hidalgo'  "+
            "  },                                 "+
            "  {                                  "+
            "    'id': 14,                        "+
            "    'name': 'Jalisco'                "+
            "    ,'slug':'jalisco'  "+
            "  },                                 "+
            "  {                                  "+
            "    'id': 15,                        "+
            "    'name': 'Estado de Mexico'                 "+
            "    ,'slug':'estado-de-mexico'  "+
            "  },                                 "+
            "  {                                  "+
            "    'id': 16,                        "+
            "    'name': 'Michoacán'              "+
            "    ,'slug':'michoacan'  "+
            "  },                                 "+
            "  {                                  "+
            "    'id': 17,                        "+
            "    'name': 'Morelos'                "+
            "    ,'slug':'morelos'  "+
            "  },                                 "+
            "  {                                  "+
            "    'id': 18,                        "+
            "    'name': 'Nayarit'                "+
            "    ,'slug':'nayarit'  "+
            "  },                                 "+
            "  {                                  "+
            "    'id': 19,                        "+
            "    'name': 'Nuevo León'             "+
            "    ,'slug':'nuevo-leon'  "+
            "  },                                 "+
            "  {                                  "+
            "    'id': 20,                        "+
            "    'name': 'Oaxaca'                 "+
            "    ,'slug':'oaxaca'  "+
            "  },                                 "+
            "  {                                  "+
            "    'id': 21,                        "+
            "    'name': 'Puebla'                 "+
            "    ,'slug':'puebla'  "+
            "  },                                 "+
            "  {                                  "+
            "    'id': 22,                        "+
            "    'name': 'Querétaro'              "+
            "    ,'slug':'queretaro'  "+
            "  },                                 "+
            "  {                                  "+
            "    'id': 23,                        "+
            "    'name': 'Quintana Roo'           "+
            "    ,'slug':'quintana-roo'  "+
            "  },                                 "+
            "  {                                  "+
            "    'id': 24,                        "+
            "    'name': 'San Luis Potosí'        "+
            "    ,'slug':'san-luis-potosi'  "+
            "  },                                 "+
            "  {                                  "+
            "    'id': 25,                        "+
            "    'name': 'Sinaloa'                "+
            "    ,'slug':'sinaloa'  "+
            "  },                                 "+
            "  {                                  "+
            "    'id': 26,                        "+
            "    'name': 'Sonora'                 "+
            "    ,'slug':'sonora'  "+
            "  },                                 "+
            "  {                                  "+
            "    'id': 27,                        "+
            "    'name': 'Tabasco'                "+
            "    ,'slug':'tabasco'  "+
            "  },                                 "+
            "  {                                  "+
            "    'id': 28,                        "+
            "    'name': 'Tamaulipas'             "+
            "    ,'slug':'tamaulipas'  "+
            "  },                                 "+
            "  {                                  "+
            "    'id': 29,                        "+
            "    'name': 'Tlaxcala'               "+
            "    ,'slug':'tlaxcala'  "+
            "  },                                 "+
            "  {                                  "+
            "    'id': 30,                        "+
            "    'name': 'Veracruz'               "+
            "    ,'slug':'veracruz'  "+
            "  },                                 "+
            "  {                                  "+
            "    'id': 31,                        "+
            "    'name': 'Yucatán'                "+
            "    ,'slug':'yucatan'  "+
            "  },                                 "+
            "  {                                  "+
            "    'id': 32,                        "+
            "    'name': 'Zacatecas'              "+
            "    ,'slug':'zacatecas'  "+
            "  }                                  "+
            "]                                    ";
    @Override
    public List<Estados> getEstados() {
        List<Estados> estados= new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(ESTADOS_JSON);
            for(int i = 0; i < jsonArray.length(); i++){
                JSONObject json = jsonArray.getJSONObject(i);
                estados.add(new Estados(json));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return estados;
    }
}
