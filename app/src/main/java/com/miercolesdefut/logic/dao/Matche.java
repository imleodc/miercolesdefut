package com.miercolesdefut.logic.dao;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by lauro on 23/01/2017.
 */

public class Matche implements Serializable{
    private Integer id;
    private Date start_date;
    private String start_date_format;
    private String start_time_format;
    private Date finish_date;
    private String finish_date_format;
    private String end_time_format;
    private Player team_a;
    private Player team_b;
    private Puntos points;
    private Members members;
    private int is_active;
    private List<CheckedTeams> user_teams;
    //DateFormat format;



    public boolean partidoEnCurso(){
        Date curren = new Date();
        return !(start_date.compareTo(curren)*finish_date.compareTo(curren) >= 0);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getStart_date() {
        return start_date;
    }

    public void setStart_date(Date start_date) {
        this.start_date = start_date;
    }

    public String getStart_date_format() {
        return start_date_format;
    }

    public void setStart_date_format(String start_date_format) {
        this.start_date_format = start_date_format;
    }

    public String getStart_time_format() {
        return start_time_format;
    }

    public void setStart_time_format(String start_time_format) {
        this.start_time_format = start_time_format;
    }

    public Date getFinish_date() {
        return finish_date;
    }

    public void setFinish_date(Date finish_date) {
        this.finish_date = finish_date;
    }

    public String getFinish_date_format() {
        return finish_date_format;
    }

    public void setFinish_date_format(String finish_date_format) {
        this.finish_date_format = finish_date_format;
    }

    public String getEnd_time_format() {
        return end_time_format;
    }

    public void setEnd_time_format(String end_time_format) {
        this.end_time_format = end_time_format;
    }

    public Player getTeam_a() {
        return team_a;
    }

    public void setTeam_a(Player team_a) {
        this.team_a = team_a;
    }

    public Player getTeam_b() {
        return team_b;
    }

    public void setTeam_b(Player team_b) {
        this.team_b = team_b;
    }

    public Puntos getPoints() {
        return points;
    }

    public void setPoints(Puntos points) {
        this.points = points;
    }

    public Members getMembers() {
        return members;
    }

    public void setMembers(Members members) {
        this.members = members;
    }

    public List<CheckedTeams> getUser_teams() {
        return user_teams;
    }

    public void setUser_teams(List<CheckedTeams> user_teams) {
        this.user_teams = user_teams;
    }

    public boolean hasCheckedTeam(){
        List<CheckedTeams> teams = user_teams;
        if(teams == null){
            return false;
        }
        for(CheckedTeams checkedTeam: teams){
            if(checkedTeam.isUser_has_checked()){
                checkedTeam.getTeam();
                return true;
            }
        }
        return false;
    }

    public int is_active() {
        return is_active;
    }

    public boolean isActive(){
        return is_active == 1;
    }

    public void setIs_active(int is_active) {
        this.is_active = is_active;
    }
}
