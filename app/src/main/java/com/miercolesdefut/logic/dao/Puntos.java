package com.miercolesdefut.logic.dao;

import com.miercolesdefut.logic.dao.gps.CheckPointResult;

import java.io.Serializable;

/**
 * Created by lauro on 26/01/2017.
 */

public class Puntos implements Serializable{
    private String share;
    private String full_team;
    private String member;
    private Integer total;
    private CheckPointResult.PointsLocation location;

    public String getShare() {
        return share;
    }

    public void setShare(String share) {
        this.share = share;
    }

    public String getFull_team() {
        return full_team;
    }

    public void setFull_team(String full_team) {
        this.full_team = full_team;
    }

    public String getMember() {
        return member;
    }

    public void setMember(String member) {
        this.member = member;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }
    public CheckPointResult.PointsLocation getLocation(){
        return location;
    }
}
