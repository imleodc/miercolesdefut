package com.miercolesdefut.logic.dao;

import java.io.Serializable;

/**
 * Created by lcastaneda005 on 09/03/2017.
 */

public class Granted implements Serializable {
    public int share;
    public int member;
    public int full_team;
}
