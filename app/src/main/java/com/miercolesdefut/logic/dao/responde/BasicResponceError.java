package com.miercolesdefut.logic.dao.responde;

import java.util.List;

/**
 * Created by lauro on 30/01/2017.
 */

public class BasicResponceError {

    private BasicError error;

    public BasicError getError() {
        return error;
    }

    public void setError(BasicError error) {
        this.error = error;
    }

    public class BasicError{
        private List<String> facebook_id;

        public List<String> getFacebook_id() {
            return facebook_id;
        }

        public void setFacebook_id(List<String> facebook_id) {
            this.facebook_id = facebook_id;
        }
    }
}
