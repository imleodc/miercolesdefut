package com.miercolesdefut.logic.dao;

import java.io.Serializable;

/**
 * Created by lauro on 01/02/2017.
 */

public class Members implements Serializable{
    private int total;
    private int checked;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getChecked() {
        return checked;
    }

    public void setChecked(int checked) {
        this.checked = checked;
    }
}
