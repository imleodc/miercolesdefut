package com.miercolesdefut.logic.dao;

import java.io.Serializable;
import java.util.List;

/**
 * Created by lauro on 09/02/2017.
 */

public class DetailMatchPerfil implements Serializable {
    private Matche match;
    private List<CheckedTeams> teams;

    public Matche getMatch() {
        return match;
    }

    public void setMatch(Matche match) {
        this.match = match;
    }

    public List<CheckedTeams> getTeams() {
        return teams;
    }

    public void setTeams(List<CheckedTeams> teams) {
        this.teams = teams;
    }
}
