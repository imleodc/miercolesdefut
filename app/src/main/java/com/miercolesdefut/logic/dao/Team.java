package com.miercolesdefut.logic.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by lauro on 23/01/2017.
 */

public class Team implements Serializable {
    private String facebook_id;
    private String name;
    private List<String> ids;
    private List<Registro> members;
    private int is_valid;
    private int is_closed;
    private int id;
    private Registro owner;
    private transient boolean newTeam = false;
    private transient List<String> invitationSended = new ArrayList<>();
    private transient List<String> invitationAccepted = new ArrayList<>();
    private transient Map<String, Boolean> canBeInvited = new HashMap<>();
    private transient int espacio;

    public String getFacebook_id() {
        return facebook_id;
    }

    public void setFacebook_id(String facebook_id) {
        this.facebook_id = facebook_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getIds() {
        if(ids == null){
            ids = new ArrayList<>();
        }
        return ids;
    }

    public void setIds(List<String> ids) {
        this.ids = ids;
    }

    public List<Registro> getMembers() {
        return members;
    }

    public void setMembers(List<Registro> members) {
        this.members = members;
    }

    public boolean is_valid() {
        return is_valid !=0;
    }

    public void setIs_valid(int is_valid) {
        this.is_valid = is_valid;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean is_closed() {
        return is_closed!=0;
    }

    public void setIs_closed(int is_closed) {
        this.is_closed = is_closed;
    }

    public boolean isPendiente(){
        return (!is_valid());
    }

    public boolean isOnwerTeam(String facebook_id){
        if(owner == null){
            return true;
        }
        return owner.facebook_id.equals(facebook_id);
    }

    public boolean isCompletado(){
        return  is_valid() && !is_closed();
    }

    public boolean isValidForMatches(){
        return is_valid() && is_closed();
    }

    public Registro getOwner() {
        return owner;
    }

    public void setOwner(Registro owner) {
        this.owner = owner;
    }

    public boolean add(FriendDAO friend){
        if(!newTeam){
            if(espacio == 0){
                return false;
            }
            espacio--;
        }
        if(ids == null){
            ids = new ArrayList<>();
        }

        if(ids.size() == 6 ){
            return false;
        }

        if(ids.contains(friend.getId())){
           return false;
        }
        return ids.add(friend.getId());
    }

    public boolean containt(FriendDAO friend){
        if(ids == null){
            return false;
        }
        return ids.contains(friend.getId());
    }

    public boolean remove(FriendDAO friend){
        if(ids == null || ids.isEmpty() || !ids.contains(friend.getId())){
            return false;
        }
        return ids.remove(friend.getId());
    }

    public boolean isNewTeam(){
        return newTeam;
    }

    public void setNewTeam(boolean newTeam){
        this.newTeam = newTeam;
    }

    public List<String> getInvitationSended() {
        return invitationSended;
    }

    public List<String> getInvitationAccepted(){
        return invitationAccepted;
    }

    public boolean isAccepted(String idFacebook){
        if(invitationAccepted == null){
            return false;
        }

        for(String idFBA:invitationAccepted){
            if(idFBA.equalsIgnoreCase(idFacebook)){
                return true;
            }
        }
        return false;
    }

    public void setInvitationSended(List<String> invitationSended) {
        this.invitationSended = invitationSended;
    }

    public void setInvitationAccepted(List<String> invitationAccepted) {
        this.invitationAccepted = invitationAccepted;
    }


    public Map<String, Boolean> getCanBeInvited() {
        return canBeInvited;
    }

    public void setCanBeInvited(Map<String, Boolean> canBeInvited) {
        this.canBeInvited = canBeInvited;
    }

    public int getEspacio() {
        return espacio;
    }

    public void setEspacio(int espacio) {
        this.espacio = espacio;
    }
}
