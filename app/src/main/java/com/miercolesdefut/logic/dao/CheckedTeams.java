package com.miercolesdefut.logic.dao;

import java.io.Serializable;
import java.util.List;

/**
 * Created by lauro on 09/02/2017.
 */

public class CheckedTeams implements Serializable {
    private Team team;
    private boolean user_has_checked;
    private List<Registro> member_checks;

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public boolean isUser_has_checked() {
        return user_has_checked;
    }

    public void setUser_has_checked(boolean user_has_checked) {
        this.user_has_checked = user_has_checked;
    }

    public List<Registro> getMember_checks() {
        return member_checks;
    }

    public void setMember_checks(List<Registro> member_checks) {
        this.member_checks = member_checks;
    }

    public boolean hasCheckedMembers(){
        if((member_checks == null || member_checks.isEmpty())){
            return false;
        }
        return true;
    }

}
