package com.miercolesdefut.logic.dao.android;

import com.miercolesdefut.logic.dao.FriendDAO;
import com.miercolesdefut.view.buissnes.teams.MyCreateTeamFriendsRecyclerViewAdapter;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by lauro on 22/01/2017.
 */

public class FriendDAOFB implements FriendDAO {

    String name;
    private String picture;
    private MyCreateTeamFriendsRecyclerViewAdapter.ViewHolder holder;
    String id;

    public FriendDAOFB(){}

    public FriendDAOFB(JSONObject data) throws JSONException {
        name = data.getString("name");
        id = data.getString("id");
    }

    public void setHolder( MyCreateTeamFriendsRecyclerViewAdapter.ViewHolder holder){
        this.holder = holder;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getPicture() {
        return picture;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setPicture(String picture) {
        this.picture = picture;
        if(holder != null){
            holder.updatePicture();
        }
    }
}
