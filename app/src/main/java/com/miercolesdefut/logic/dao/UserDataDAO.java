package com.miercolesdefut.logic.dao;

import java.util.Date;

/**
 * Created by lauro on 30/12/2016.
 */

public interface UserDataDAO {
    Date getBirthday();
    void saveBirthDay(Date date);
    void clear();
}
