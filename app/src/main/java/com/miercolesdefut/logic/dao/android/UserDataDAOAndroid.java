package com.miercolesdefut.logic.dao.android;

import android.content.Context;
import android.content.SharedPreferences;

import com.miercolesdefut.R;
import com.miercolesdefut.logic.dao.UserDataDAO;

import java.util.Date;

/**
 * Created by lauro on 30/12/2016.
 */

public class UserDataDAOAndroid implements UserDataDAO {

    private Context context;
    private SharedPreferences preferences;
    private static final String MESSAGE_ERROR_CONTEXT = "Context is null";
    private static final String BIRTHDAY_USER = "birthday";

    public UserDataDAOAndroid(Context context){
        context(context);
    }

    public UserDataDAO context(Context context){
        if(context == null){
            throw new RuntimeException(MESSAGE_ERROR_CONTEXT);
        }
        this.context = context;
        initSharedPreferences();
        return this;
    }

    void initSharedPreferences(){
        preferences =context.getSharedPreferences(context.getString(R.string.user_data),Context.MODE_PRIVATE);
    }

    @Override
    public Date getBirthday() {
        verifyContext();
        long dateL = preferences.getLong(BIRTHDAY_USER, -1);
        if(dateL == -1)
            return null;
        return new Date(dateL);
    }

    @Override
    public void saveBirthDay(Date date){
        verifyContext();
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong(BIRTHDAY_USER,date.getTime());
        editor.commit();
    }

    @Override
    public void clear() {
        preferences.edit().clear().commit();
    }

    public void verifyContext(){
        if(context == null)
            throw new RuntimeException(MESSAGE_ERROR_CONTEXT);
    }
}