package com.miercolesdefut.logic.dao;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by lauro on 09/02/2017.
 */

public class LocationError  {
    private List<LocalError> team_id;
    public List<String> facebook_id;
    public List<String> photo;

    public List<LocalError> getTeam_id() {
        return team_id;
    }

    public void setTeam_id(List<LocalError> team_id) {
        this.team_id = team_id;
    }

    public String getMessage(){
        StringBuilder stb = new StringBuilder();
        if(team_id != null && !team_id.isEmpty()) {
            for (LocalError localErr : team_id) {
                stb.append(localErr.getMessage()).append(",");
            }
        }
        if(photo != null && !photo.isEmpty()){
            stb.append(photo.get(0));
        }
        return stb.substring(0, stb.length()-1);
    }

    public static class LocalError{
        private String message;
        private ReferencesError reference;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public ReferencesError getReference() {
            return reference;
        }

        public void setReference(ReferencesError reference) {
            this.reference = reference;
        }
    }

    public static class ReferencesError implements Serializable{
        @SerializedName("team_id")
        private
        int teamId;
        @SerializedName("match_id")
        private
        int matchId;
        private double latitude;
        private double longitude;

        public int getTeamId() {
            return teamId;
        }

        public void setTeamId(int teamId) {
            this.teamId = teamId;
        }

        public int getMatchId() {
            return matchId;
        }

        public void setMatchId(int matchId) {
            this.matchId = matchId;
        }

        public double getLatitude() {
            return latitude;
        }

        public void setLatitude(double latitude) {
            this.latitude = latitude;
        }

        public double getLongitude() {
            return longitude;
        }

        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }
    }
}
