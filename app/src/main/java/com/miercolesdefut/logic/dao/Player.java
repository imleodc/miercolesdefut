package com.miercolesdefut.logic.dao;

import java.io.Serializable;

/**
 * Created by lauro on 25/01/2017.
 */

public class Player implements Serializable{
    private String name;
    private String image;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
