package com.miercolesdefut.logic.dao;

/**
 * Created by lauro on 23/01/2017.
 */

public interface PuntoReferencia {
    String getFacebookId();
    Integer getTeamId();
    Integer getMatchId();
    Long getLatitude();
    Long getLongitude();
}
