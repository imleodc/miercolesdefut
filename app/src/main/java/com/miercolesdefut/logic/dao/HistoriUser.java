package com.miercolesdefut.logic.dao;

import java.util.List;

/**
 * Created by lauro on 26/01/2017.
 */

public class HistoriUser {
    private List<Matche> matches;
    private List<Location> locations;
    private Puntos points;

    public List<Matche> getMatches() {
        return matches;
    }

    public void setMatches(List<Matche> matches) {
        this.matches = matches;
    }

    public List<Location> getLocations() {
        return locations;
    }

    public void setLocations(List<Location> locations) {
        this.locations = locations;
    }

    public Puntos getPoints() {
        return points;
    }

    public void setPoints(Puntos points) {
        this.points = points;
    }
}
