package com.miercolesdefut.logic.dao;

/**
 * Created by lauro on 23/01/2017.
 */

public class AcceptTeam {
    private String facebook_id;
    private Integer team_id;

    public String getFacebook_id() {
        return facebook_id;
    }

    public void setFacebook_id(String facebook_id) {
        this.facebook_id = facebook_id;
    }

    public Integer getTeam_id() {
        return team_id;
    }

    public void setTeam_id(Integer team_id) {
        this.team_id = team_id;
    }
}
