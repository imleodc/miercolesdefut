package com.miercolesdefut.logic.dao;

import java.io.Serializable;

/**
 * Created by lauro on 28/03/2017.
 */

public class State implements Serializable {
    private int id;
    private String name;
    private String slug;

    public String getName(){
        return name;
    }

    public String getSlug(){
        return slug;
    }
}
