package com.miercolesdefut.logic.dao;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by lauro on 07/02/2017.
 */

public class StandarError {
    @SerializedName("team_id")
    public List<String> teamId;
    @SerializedName("ids")
    public List<String> ids;
    @SerializedName("facebook_id")
    public List<String> facebookId;
    @SerializedName("name")
    public List<String> nameTeam;


    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(getFormatMessage("Equipo: ", teamId));
        stringBuilder.append(getFormatMessage("", ids));
        stringBuilder.append(getFormatMessage("User:", facebookId));
        stringBuilder.append(getFormatMessage("", nameTeam));
        return stringBuilder.toString();
    }

    private String getFormatMessage(String from, List<String> errors){
        if(errors == null){
            return "";
        }
        StringBuilder message = new StringBuilder();
        for(String error: errors){
            message.append(error).append(",");
        }
        return message.substring(0, message.length()-1);
    }

}
