package com.miercolesdefut.logic.dao;

/**
 * Created by lauro on 22/01/2017.
 */

public interface FriendDAO extends Picture{
    String getName();
    String getPicture();
    String getId();
}
