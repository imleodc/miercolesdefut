package com.miercolesdefut.logic.dao;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by lauro on 08/03/2017.
 */


public class ConfigurationDistance implements Serializable{
    @SerializedName("member_distance")
    private int memberDistance;
    @SerializedName("location_distance")
    private int locationDistance;

    public int getMemberDistance() {
        return memberDistance;
    }

    public int getLocationDistance() {
        return locationDistance;
    }
}
