package com.miercolesdefut.logic.dao;

/**
 * Created by lauro on 23/01/2017.
 */

public class Invitacion {
    private Integer id;
    private String name;
    private Registro owner;
    private transient String urlImage;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    public Registro getOwner() {
        return owner;
    }

    public void setOwner(Registro owner) {
        this.owner = owner;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }
}
