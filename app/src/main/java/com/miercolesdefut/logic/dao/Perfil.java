package com.miercolesdefut.logic.dao;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by lauro on 09/02/2017.
 */

public class Perfil implements Serializable{
    @SerializedName("id")
    private
    long id;
    @SerializedName("state_id")
    private
    int stateId;
    @SerializedName("facebook_id")
    private
    String facebookId;
    @SerializedName("full_name")
    private
    String fullName;
    @SerializedName("email")
    private
    String email;
    @SerializedName("phone")
    private
    String telefono;
    @SerializedName("created_at")
    private
    String createAt;
    @SerializedName("updated_at")
    private
    String updatedAt;
    @SerializedName("can_create_teams")
    private
    boolean canCreateTeam;
    @SerializedName("member_distance")
    private
    int memberDistance;
    @SerializedName("location_distance")
    private
    int locationDistance;
    private State state;

    @SerializedName("config")
    private ConfigurationDistance config;

    private List<DetailMatchPerfil> matches;
    private CheckedTeams checkedTeam = null;
    private Matche matchCheckedTeam = null;

    private transient  Team teamTemporal;

    public void setMemberDistance(int memberDistance) {
        this.memberDistance = memberDistance;
    }

    public void setLocationDistance(int locationDistance) {
        this.locationDistance = locationDistance;
    }

    public int getLocationDistance() {
        return locationDistance;
    }

    public int getMemberDistance() {
        return memberDistance;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getStateId() {
        return stateId;
    }

    public void setStateId(int stateId) {
        this.stateId = stateId;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCreateAt() {
        return createAt;
    }

    public void setCreateAt(String createAt) {
        this.createAt = createAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public boolean isCanCreateTeam() {
        return canCreateTeam;
    }

    public void setCanCreateTeam(boolean canCreateTeam) {
        this.canCreateTeam = canCreateTeam;
    }

    public List<DetailMatchPerfil> getMatches() {
        return matches;
    }

    public void setMatches(List<DetailMatchPerfil> matches) {
        this.matches = matches;
    }

    public boolean hasMakeCheck(){
        List<DetailMatchPerfil> matchs = getMatches();
        for(DetailMatchPerfil detailMatch:matchs){
            List<CheckedTeams> teams = detailMatch.getTeams();
            if(teams == null){
                return false;
            }
            for(CheckedTeams checkedTeam: teams){
                if(checkedTeam.isUser_has_checked()){
                    teamTemporal = checkedTeam.getTeam();
                    return true;
                }
            }
        }
        return false;
    }

    public boolean usersHasChecked(){
        List<DetailMatchPerfil> matchs = getMatches();
        for(DetailMatchPerfil detailMatchPerfil: matchs){
            List<CheckedTeams> teams = detailMatchPerfil.getTeams();
            if(teams == null) {
                return false;
            }
            for(CheckedTeams checkedTeams: teams) {
                if(checkedTeams.hasCheckedMembers()){
                    return true;
                }
            }
        }
        return false;
    }

    public Team getCheckedTeam(){
        return teamTemporal;
    }

    public boolean teamHasCheckedUser(Team team){
        List<DetailMatchPerfil> matchs = getMatches();
        for(DetailMatchPerfil detailMatch: matchs){
            List<CheckedTeams> teamsFor = detailMatch.getTeams();
            if(teamsFor == null) {
                continue;
            } else {
                for(CheckedTeams checkedTeamFor: teamsFor){
                    if(checkedTeamFor.getTeam().getId() == team.getId() && checkedTeamFor.hasCheckedMembers()){
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public DetailMatchPerfil getCheckedTeamUsers(Team team){
        List<DetailMatchPerfil> matchs = getMatches();
        for(DetailMatchPerfil detailMatch: matchs){
            List<CheckedTeams> teamsFor = detailMatch.getTeams();
            if(teamsFor == null) {
                continue;
            } else {
                for(CheckedTeams checkedTeamFor: teamsFor){
                    if(checkedTeamFor.getTeam().getId() == team.getId() && checkedTeamFor.hasCheckedMembers()){
                        return detailMatch;
                    }
                }
            }
        }
        return null;
    }

    public CheckedTeams getCheckedTeamUser(){
        List<DetailMatchPerfil> matchs = getMatches();
        for(DetailMatchPerfil detailMatch: matchs){
            List<CheckedTeams> teamsFor = detailMatch.getTeams();
            if(teamsFor == null) {
                continue;
            } else {
                for(CheckedTeams checkedTeamFor: teamsFor){
                    if(checkedTeamFor.isUser_has_checked()){
                        matchCheckedTeam = detailMatch.getMatch();
                        return checkedTeamFor;
                    }
                }
            }
        }
        return null;
    }

    public Matche getMatchCheckedTeam(){
        return matchCheckedTeam;
    }

    public ConfigurationDistance getConfig() {
        return config;
    }

    public void setConfig(ConfigurationDistance config) {
        this.config = config;
    }

    public State getState(){
        return state;
    }
}
