package com.miercolesdefut.logic.dao;

import java.io.Serializable;
import java.util.List;

/**
 * Created by lauro on 07/02/2017.
 */

public class TeamStatusMembers implements Serializable{
    private List<String> pending;
    private List<String> accepted;
    private int room;

    public List<String> getPending() {
        return pending;
    }

    public void setPending(List<String> pending) {
        this.pending = pending;
    }

    public List<String> getAccepted() {
        return accepted;
    }

    public void setAccepted(List<String> accepted) {
        this.accepted = accepted;
    }

    public int getRoom() {
        return room;
    }

    public void setRoom(int room) {
        this.room = room;
    }
}
