package com.miercolesdefut.logic.dao.gps;

import com.miercolesdefut.logic.dao.Matche;
import com.miercolesdefut.logic.dao.Puntos;
import com.miercolesdefut.logic.dao.Team;

/**
 * Created by lauro on 08/02/2017.
 */

public class Location {
    private double latitude;
    private double longitude;
    //@SerializedName("set_by")
    //private FriendDAOFB setBy;
    private Matche match;
    private Team team;
    //private List<FriendDAOFB> checked;
    private Puntos points;

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    /*public FriendDAOFB getSetBy() {
        return setBy;
    }

    public void setSetBy(FriendDAOFB setBy) {
        this.setBy = setBy;
    }*/

    public Matche getMatch() {
        return match;
    }

    public void setMatch(Matche match) {
        this.match = match;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    /*public List<FriendDAOFB> getChecked() {
        return checked;
    }

    public void setChecked(List<FriendDAOFB> checked) {
        this.checked = checked;
    }*/

    public Puntos getPoints() {
        return points;
    }

    public void setPoints(Puntos points) {
        this.points = points;
    }
}
