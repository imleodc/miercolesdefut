package com.miercolesdefut.logic.dao.gps;

import com.miercolesdefut.logic.dao.Granted;
import com.miercolesdefut.logic.dao.Matche;
import com.miercolesdefut.logic.dao.Registro;
import com.miercolesdefut.logic.dao.Team;

import java.io.Serializable;
import java.util.List;

/**
 * Created by lauro on 12/02/2017.
 */

public class CheckPointResult implements Serializable {
    public Reference reference;
    public List<Registro> checked;
    public Points points;
    public Granted granted;

    public static class Reference {
        public double latitude;
        public double longitude;
        public Registro set_by;
        public Matche match;
        public Team team;
    }

    public static class Points{
        public int share;
        public String full_team;
        public String member;
        public int total;
        public PointsLocation location;
    }

    public static class PointsLocation{
        public int points;
        public String name;
    }

}
