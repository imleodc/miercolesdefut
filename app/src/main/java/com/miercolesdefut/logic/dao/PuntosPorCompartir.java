package com.miercolesdefut.logic.dao;

/**
 * Created by lauro on 17/02/2017.
 */

public class PuntosPorCompartir {
    public String share;
    public String full_team;
    public String member;
    public int total;
    public String image;
    public Granted granted;
}
