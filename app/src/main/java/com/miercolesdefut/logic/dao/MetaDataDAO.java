package com.miercolesdefut.logic.dao;

import com.miercolesdefut.logic.model.Estados;

import java.util.List;

/**
 * Created by lauro on 11/01/2017.
 */

public interface MetaDataDAO {
    public List<Estados> getEstados();
}
