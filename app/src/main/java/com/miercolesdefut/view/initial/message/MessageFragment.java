package com.miercolesdefut.view.initial.message;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.miercolesdefut.R;
import com.miercolesdefut.logic.configurations.interfaces.Message;
import com.miercolesdefut.view.BaseFragment;

/**
 * Created by lauro on 10/01/2017.
 */

public class MessageFragment extends BaseFragment {

    public static final String TITLE = "title";
    public static final String CONTENT = "content";

    public static MessageFragment newInstance(Message message) {
        MessageFragment fragment = new MessageFragment();
        Bundle args = new Bundle();
        args.putString(TITLE, message.getTitle());
        args.putString(CONTENT, message.getContent());
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_message_tab, container,false);
        if(getArguments() != null){
            String titleStr = getArguments().getString(TITLE);
            String contentStr = getArguments().getString(CONTENT);

            if(titleStr != null) {
                if(titleStr.length()>0){
                    TextView title = (TextView) view.findViewById(R.id.messageTitle);
                    title.setText(getArguments().getString(TITLE));
                }else{
                    ((TextView) view.findViewById(R.id.messageTitle)).setVisibility(View.GONE);
                }
            }

            TextView content = (TextView) view.findViewById(R.id.messageContent);
            content.setText(contentStr);
        }
        return view;
    }
}
