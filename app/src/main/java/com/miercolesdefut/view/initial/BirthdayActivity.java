package com.miercolesdefut.view.initial;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.miercolesdefut.R;
import com.miercolesdefut.logic.model.rxbux.App;
import com.miercolesdefut.view.BaseActivity;
import com.miercolesdefut.view.facebook.LoginActivity;
import com.google.android.gms.common.api.GoogleApiClient;
import com.jakewharton.rxbinding.view.RxView;
import com.wx.wheelview.adapter.ArrayWheelAdapter;
import com.wx.wheelview.widget.WheelView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rx.Subscription;
import rx.functions.Action1;
import rx.functions.Func1;

public class BirthdayActivity extends BaseActivity {
    public static final DateFormat dateFormat = new SimpleDateFormat("dd/mm/yyyy");
    public static final String BIRTHDAY_TEXT = "birdayTextParam";

    Spinner dayNumber;
    Spinner monthNumber;
    Spinner yearNumber;
    Subscription subscription;
    //ServiceUser serviceUser;
    String birthdayDate;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    private static final String INIT_SCREEN = "onboarding/age";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Tracker mTracker = ((App) getApplication()).getDefaultTracker();
        mTracker.setScreenName(INIT_SCREEN);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        //serviceUser = new ServiceUserImp(getApplicationContext());
        setContentView(R.layout.activity_birthday);
        dayNumber = (Spinner) findViewById(R.id.dayNumber);
        config(dayNumber, fillDate());
        monthNumber = (Spinner) findViewById(R.id.monthNumber);
        config(monthNumber, fillMonth());
        yearNumber = (Spinner) findViewById(R.id.yearView);
        config(yearNumber, fillYear());
        addBtnListener();
    }

    public void config(Spinner spinner, List<?> data) {
        ArrayAdapter<?> spinnerAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, data){
            @NonNull
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                TextView textSpinner = ((TextView)v.findViewById(android.R.id.text1));
                textSpinner.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
                textSpinner.setTextSize(18.0f);
                textSpinner.setGravity(Gravity.RIGHT);
                return v;
            }
        };
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerAdapter);
        //spinner.setSelection(0);
    }

    public void configWheelView(WheelView wheelView, List<?> data) {
        wheelView.setWheelAdapter(new ArrayWheelAdapter(this));
        wheelView.setSkin(WheelView.Skin.Holo);
        wheelView.setWheelData(data);
        WheelView.WheelViewStyle style = new WheelView.WheelViewStyle();
        style.textColor = getResources().getColor(R.color.color_blue_text);
        style.textSize = 20;//px
        wheelView.setStyle(style);
    }

    protected List<String> fillDate() {
        List<String> list = new ArrayList<>();
        for (int i = 1; i < 32; i++) {
            if(i<10){
                list.add(new StringBuilder().append("0").append(Integer.toString(i)).toString());
            } else {
                list.add(Integer.toString(i));
            }
        }
        list.add(0,"DD");//.add("DD");
        return list;
    }

    protected List<String> fillMonth() {
        List<String> list = new ArrayList<>();
        for (int i = 1; i < 13; i++) {
            if(i<10){
                list.add(new StringBuilder().append("0").append(Integer.toString(i)).toString());
            } else {
                list.add(Integer.toString(i));
            }
        }
        list.add(0,"MM");
        return list;
    }

    protected List<String> fillYear() {
        List<String> list = new ArrayList<>();
        Calendar calendar = GregorianCalendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        for (int i = year; i > 1920; i--) {
            list.add(Integer.toString(i));
        }
        list.add(0,"AAAA");
        return list;
    }

    /*@Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }*/

    protected void addBtnListener() {
        AppCompatButton validate = (AppCompatButton) this.findViewById(R.id.validateBirthdayBtn);
        subscription = RxView.clicks(validate)
                .filter(new Func1<Void, Boolean>() {
                    @Override
                    public Boolean call(Void aVoid) {
                        return isValid();
                    }
                })
                .subscribe(new Action1<Void>() {
                    @Override
                    public void call(Void aVoid) {
                        Map<String, String> parameters =new HashMap<>();
                        parameters.put(BIRTHDAY_TEXT, birthdayDate);
                        callActivy(LoginActivity.class, parameters);
                    }
                });
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        subscription.unsubscribe();
    }

    public boolean isValid() {
        birthdayDate = new StringBuilder(dayNumber.getSelectedItem().toString())
                .append("/")
                .append(monthNumber.getSelectedItem().toString())
                .append("/")
                .append(yearNumber.getSelectedItem().toString()).toString();
        try {
            Date birthday = dateFormat.parse(birthdayDate);
            Calendar calendar = GregorianCalendar.getInstance();
            calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR) - 18);
            boolean valid = calendar.getTime().after(birthday);
            if(!valid){
                new AlertDialog.Builder(this).setMessage(getResources().getString(R.string.birthday_message_1_text)).show();
            }
            return valid;
        } catch (ParseException e) {
            e.printStackTrace();
            new AlertDialog.Builder(this).setMessage("Debe completar los datos requeridos").show();
            return false;
        }
    }

    public boolean isDateComplete(){
        Object day = dayNumber.getSelectedItem();
        Object month = monthNumber.getSelectedItem();
        Object year = yearNumber.getSelectedItem();
        if(day == null){
            new AlertDialog.Builder(this).setMessage("Debe completar los datos requeridos").show();
        }
        return false;
    }
}
