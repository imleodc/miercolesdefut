package com.miercolesdefut.view.initial;

import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationManagerCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.miercolesdefut.R;
import com.miercolesdefut.logic.model.rxbux.App;
import com.miercolesdefut.view.BaseFragment;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;


/**
 * Created by lcastaneda005 on 05/03/2017.
 */

public class ControlsActivity extends BaseFragment implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final String INIT_SCREEN = "config";

    public static ControlsActivity newInstance() {
        Bundle args = new Bundle();
        ControlsActivity fragment = new ControlsActivity();
        fragment.setArguments(args);
        return fragment;
    }

    public ControlsActivity(){
        setImageBackground(R.drawable.historial);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        showActionBar(getResources().getString(R.string.controles));
        View view = inflater.inflate(R.layout.controls_activity, container, false);


        Switch locSwitch = (Switch) view.findViewById(R.id.switch_gps);
        Switch notSwitch = (Switch) view.findViewById(R.id.switch_not);

        LinearLayout layoutSwitch = (LinearLayout) view.findViewById(R.id.layout_switch_gps);
        LinearLayout layoutNotif = (LinearLayout) view.findViewById(R.id.layout_switch_not);

        //LinearLayout bluetoothSwitch = (LinearLayout) view.findViewById(R.id.switch_blue);
        //BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        //bluetoothSwitch.setChecked(mBluetoothAdapter.isEnabled());

        NotificationManagerCompat nmc = NotificationManagerCompat.from(inflater.getContext());
        notSwitch.setChecked(nmc.areNotificationsEnabled());

        /*
        bluetoothSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if(isChecked){
                mBluetoothAdapter.enable();
            }else{
                mBluetoothAdapter.disable();
            }
        });
        */

        LocationManager manager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE );
        boolean statusOfGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        locSwitch.setChecked(statusOfGPS);

        locSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
            buttonView.setChecked(true);
            locSwitchHandler(isChecked);
        });
        layoutSwitch.setOnClickListener(v -> locSwitchHandler(locSwitch.isChecked()));

        notSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
            buttonView.setChecked(true);
            notSwitchHandler();
        });

        layoutNotif.setOnClickListener((v) -> notSwitchHandler());

        return view;
    }

    public void locSwitchHandler(boolean isChecked){
        if(!isChecked){
            Toast.makeText(getContext(), getResources().getString(R.string.location_already_active), Toast.LENGTH_SHORT).show();
            return;
        }
        createLocationRequest();
    }

    public void notSwitchHandler(){
        Context context = getContext();
        String setting = "android.settings.APP_NOTIFICATION_SETTINGS";

        Intent intent = new Intent();
        intent.setAction(setting);
        intent.putExtra("app_package", context.getPackageName());
        intent.putExtra("app_uid", context.getApplicationInfo().uid);
        startActivity(intent);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.controls_activity);
        Tracker mTracker = ((App) getActivity().getApplication()).getDefaultTracker();
        mTracker.setScreenName(INIT_SCREEN);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    protected void createLocationRequest() {
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        GoogleApiClient mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        mGoogleApiClient.connect();

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());

        builder.setAlwaysShow(true);

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result
                        .getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can
                        // initialize location
                        // requests here.
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be
                        // fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling
                            // startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(getActivity(), 1000);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have
                        // no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }

                mGoogleApiClient.disconnect();
            }
        });

    }
}
