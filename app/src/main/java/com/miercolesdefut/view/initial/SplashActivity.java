package com.miercolesdefut.view.initial;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;

import com.miercolesdefut.IntroActivity;
import com.miercolesdefut.logic.ServiceUser;
import com.miercolesdefut.logic.ServiceUserImp;
import com.miercolesdefut.view.BaseActivity;
import com.facebook.FacebookSdk;
import com.facebook.applinks.AppLinkData;
import com.miercolesdefut.view.MainActivity;
import com.miercolesdefut.view.buissnes.alerts.CoronaAlertDialog;

import bolts.AppLinks;

/**
 * Created by lauro on 16/01/2017.
 */

public class SplashActivity extends BaseActivity {

    ServiceUser serviceUser;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        serviceUser = new ServiceUserImp(getApplicationContext());
        setTranslucentStatusBar();
        FacebookSdk.sdkInitialize(this);
        Uri targetUrl = AppLinks.getTargetUrlFromInboundIntent(this, getIntent());
        if(targetUrl != null){
            //Log.i("Activity", "App Link Target URL: " + targetUrl.toString());
        } else {
            AppLinkData.fetchDeferredAppLinkData(this, new AppLinkData.CompletionHandler() {
                @Override
                public void onDeferredAppLinkDataFetched(AppLinkData appLinkData) {
                }
            });
        }

        if(serviceUser.hasBirthday()){
            callMainActivity();
            return;
        }


        callActivy(IntroActivity.class);
    }

    public class NotificationNoConnected extends BroadcastReceiver {

        public static final String PROCESS_DISCONECTED_NETWORK = "com.corona.no.connection.to.internet";

        @Override
        public void onReceive(Context context, Intent arg1) {
            DialogFragment alert = CoronaAlertDialog.newInstance(CoronaAlertDialog.ACTION_MESSAGE, "No cuenta con internet.");
            alert.show(getSupportFragmentManager(), null);

        }
    }
}
