package com.miercolesdefut.view.initial;

import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.miercolesdefut.R;
import com.miercolesdefut.view.BaseFragment;
import com.miercolesdefut.view.buissnes.teams.ProxPartidosFragment;
import com.miercolesdefut.view.buissnes.teams.RegistrarEquipo;

public class MakeTeamsFragment extends BaseFragment implements RegistrarEquipo.OnFragmentInteractionListener{
    public static final String TAG_REGISTRA_EQUIPO ="REGISTRA_EQUIPO";
    boolean onCreate = true;

    public MakeTeamsFragment() {
    }


    public static MakeTeamsFragment newInstance() {
        MakeTeamsFragment fragment = new MakeTeamsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createFragment();
        getFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener(){
            @Override
            public void onBackStackChanged() {
                if(!onCreate && isVisible()) {
                    createFragment();
                }
            }
        });
        onCreate = false;
        ((AppCompatActivity)getActivity()).getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent)));//setDisplayShowTitleEnabled(false);
        //showActionBar();
        hideActionBar();
    }

    private void registraListener(View view) {
        Button btn = (Button) view.findViewById(R.id.registraEquipoBtn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().beginTransaction().addToBackStack(null).replace(R.id.content_frame, RegistrarEquipo.newInstance(),TAG_REGISTRA_EQUIPO).commit();
            }
        });
    }

    public void createFragment(){
        this.getFragmentManager().beginTransaction().replace(R.id.proxPartidosFr, new ProxPartidosFragment()).commit();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_make_teams, container, false);
        registraListener(view);
        return view;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
