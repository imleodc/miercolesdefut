package com.miercolesdefut.view.initial;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.util.Patterns;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.miercolesdefut.R;
import com.miercolesdefut.ServiceCoronaImp;
import com.miercolesdefut.logic.ServiceCorona;
import com.miercolesdefut.logic.ServiceUser;
import com.miercolesdefut.logic.ServiceUserImp;
import com.miercolesdefut.logic.configurations.FacebookUtils;
import com.miercolesdefut.logic.dao.MetaDataDAO;
import com.miercolesdefut.logic.dao.Perfil;
import com.miercolesdefut.logic.dao.Registro;
import com.miercolesdefut.logic.dao.android.MetaDataDAOAndroid;
import com.miercolesdefut.logic.model.DataUser;
import com.miercolesdefut.logic.model.Estados;
import com.miercolesdefut.logic.model.facebook.ServiceFacebook;
import com.miercolesdefut.logic.model.facebook.ServiceFacebookImp;
import com.miercolesdefut.logic.model.rxbux.App;
import com.miercolesdefut.view.BaseActivity;
import com.jakewharton.rxbinding.view.RxView;

import java.text.ParseException;
import java.util.List;
import java.util.regex.Pattern;

import okhttp3.ResponseBody;
import retrofit2.Response;
import rx.Subscription;
import rx.functions.Action1;
import rx.functions.Func1;

/**
 * Created by lauro on 04/01/2017.
 */

public class RegistrationActivity extends BaseActivity{
    ServiceCorona serviceCorona;
    EditText nombre;
    EditText email;
    EditText telefono;
    TextInputLayout nombreUserLayout;
    CheckBox aceptar;
    Spinner estados;
    Subscription subscription;
    String fbId;

    ServiceFacebook serviceFacebook = new ServiceFacebookImp();
    MetaDataDAO datosApplicacion;
    ServiceUser serviceUser;

    private static final String INIT_SCREEN = "register";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Tracker mTracker = ((App) getApplication()).getDefaultTracker();
        mTracker.setScreenName(INIT_SCREEN);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        serviceUser = new ServiceUserImp(getApplicationContext());
        serviceCorona = new ServiceCoronaImp();
        setContentView(R.layout.fragment_user_data);
        datosApplicacion = new MetaDataDAOAndroid();
        final TextView terminosLink = (TextView)findViewById(R.id.terminosLink);
        addLinkListener(terminosLink);
        //terminosLink.setMovementMethod(LinkMovementMethod.getInstance());
        nombre = (EditText)findViewById(R.id.nombreUser);
        email = (EditText)findViewById(R.id.emailUser);
        nombreUserLayout = (TextInputLayout) findViewById(R.id.nombreUserLayout);
        telefono = (EditText)findViewById(R.id.telefonoUser);
        aceptar = (CheckBox)findViewById(R.id.checkBox);
        estados = (Spinner) findViewById(R.id.spinnerEstados);
        fillEstados();
        addBtnListener();
        requestDataUser();
    }

    private void agregarErrorEstados() {
        TextView errorText = (TextView) estados.getSelectedView();
        errorText.setError("Estado no valido.");
        errorText.setTextColor(Color.RED);
        //errorText.setText();
    }

    private void addLinkListener(TextView terminosLink) {
        terminosLink.setTextColor(getResources().getColor(R.color.color_background_title));
        RxView.clicks(terminosLink).subscribe(new Action1<Void>() {
            @Override
            public void call(Void aVoid) {
                showTerminos();
            }
        });
    }

    protected void addBtnListener(){
        AppCompatButton button = (AppCompatButton) findViewById(R.id.registrationBtn);
        subscription = RxView.clicks(button)
                .filter(new Func1<Void, Boolean>() {
                    @Override
                    public Boolean call(Void aVoid) {
                        return isValid();
                    }
                })
                .subscribe(aVoid -> {

                    if(!isConected()){
                        AlertDialog.Builder builder = new AlertDialog.Builder(this);
                        builder.setMessage("Error en la conexión a internet, por favor inténtelo más tarde.");
                        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which){
                                //Do nothing here because we override this button later to change the close behaviour.
                                //However, we still need this because on older versions of Android unless we
                                //pass a handler the button doesn't get instantiated
                            }
                        });

                        final AlertDialog dialog = builder.create();
                        dialog.show();
                        dialog.setCancelable(false);

                        return;
                    }

                    ((App)getApplication()).getDefaultTracker().send(new HitBuilders.EventBuilder().setCategory("button").setAction("touch").setLabel("terms-send").build());

                    Registro registro = new Registro();
                    registro.setEmail(email.getText().toString());
                    registro.setFull_name(nombre.getText().toString());
                    registro.setPhone(telefono.getText().toString());
                    registro.setState(estados.getSelectedItem().toString().toLowerCase().replaceAll(" ", "-"));
                    registro.setDevice_id(Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID));
                    registro.setDevice_type("android");
                    registro.setFacebook_id(FacebookUtils.getId(getApplicationContext()));
                    final ProgressDialog progressDialog =  BaseActivity.createProgressDialog(RegistrationActivity.this);
                    progressDialog.show();
                    serviceCorona.registroUsuario(registro, new Action1<Response<ResponseBody>>(){
                        @Override
                        public void call(Response<ResponseBody> response) {
                            progressDialog.dismiss();
                            if(response.code() != 200){
                                showError(response.message());
                                return;
                            }
                            //callMainActivity();
                            //callActivy(InstruccionesActivity.class);
                            callInitMakeTeamOfMainActivity();
                            try {
                                serviceUser.saveBirthday(BirthdayActivity.dateFormat.parse(getIntent().getStringExtra(BirthdayActivity.BIRTHDAY_TEXT)));
                            } catch (ParseException e) {
                                e.printStackTrace();
                                serviceUser.clear();
                            }
                        }
                    });
                });
    }

    protected void requestDataUser(){
        serviceFacebook.callFacebookDataUser(getApplicationContext(), new Action1<DataUser>() {
            @Override
            public void call(DataUser dataUser) {
                nombre.setText(dataUser.getNombre());
                email.setText(dataUser.getEmail());
                telefono.setText(dataUser.getTelefono());
                serviceCorona.getDetalleUsuario(FacebookUtils.getId(getApplicationContext()), (response)->{
                    if(response.isSuccessful()){
                        Perfil perfil = response.body();
                        email.setText(perfil.getEmail());
                        telefono.setText(perfil.getTelefono());
                        //estados.setSelection(perfil.getStateId());
                        for(int i = 0; i < estados.getAdapter().getCount(); i ++){
                            Object obj = estados.getAdapter().getItem(i);
                            if(obj != null && obj instanceof  Estados) {
                                if(((Estados)obj).getId() == -1){
                                    continue;
                                }
                                if(perfil.getState() != null && perfil.getState().getSlug() != null &&((Estados)obj).getSlug().equalsIgnoreCase(perfil.getState().getSlug())){
                                    estados.setSelection(i);
                                    break;
                                }
                            }
                        }
                    }
                });
            }
        });
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        subscription.unsubscribe();
    }

    @Override
    public void onBackPressed() {
        //moveTaskToBack(true);
    }

    private void fillEstados() {
        Spinner spinner = (Spinner) findViewById(R.id.spinnerEstados);
        List<Estados> estadosList = datosApplicacion.getEstados();
        Estados selecciona = new Estados(-1, getResources().getString(R.string.seleccionaEstado));
        estadosList.add(0,selecciona);
        ArrayAdapter<Estados> estados = new ArrayAdapter<>(this,android.R.layout.simple_spinner_item,estadosList);
        estados.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(estados);
    }

    protected boolean isValid(){
        boolean valid = true;
        valid =   !isEmpty(R.id.nombreUserLayout, nombre, "Nombre requerido.")
                &&!isEmpty(R.id.emailUserLayout, email, "Email no válido.")
                &&isValidPattern(R.id.emailUserLayout,Patterns.EMAIL_ADDRESS, email,"Email no válido.")
                &&isValidPattern(R.id.telefonoUserLayout,Patterns.PHONE, telefono, "Teléfono no valido")
                &&isValidLength(R.id.telefonoUserLayout, telefono, "Teléfono no válido.", 10);

        if(!aceptar.isChecked()){
            aceptar.setError("Aceptar términos y condiciones.");
            valid = false;
        }
        if(((Estados)estados.getSelectedItem()).getId() == -1){
            valid = false;
            agregarErrorEstados();
        }

        return valid;
    }

    private boolean isValidPattern(final int idElement,Pattern pattern, EditText input, String textError){
        boolean valid = pattern.matcher(input.getText()).matches();
        if(!valid){
            showError(idElement, textError);
        } else {
            cleanError(idElement);
        }
        return valid;
    }

    private boolean isValidLength(final int idElement,EditText input, String textError, int length){
        boolean valid = true;
        if(input.length() < length ){
            valid = false;
            showError(idElement, textError);
        } else {
            cleanError(idElement);
        }
        return valid;
    }

    protected boolean isEmpty(final int idElement,EditText input, String textError){
        return !isValidLength(idElement, input, textError, 1);
    }

    protected void showError(final int idElement, String text) {
        TextInputLayout textLayout = (TextInputLayout) findViewById(idElement);
        textLayout.setErrorEnabled(true);
        textLayout.setError(text);
    }
    protected void cleanError(final int idElement) {
        TextInputLayout textLayout = (TextInputLayout) findViewById(idElement);
        textLayout.setErrorEnabled(false);
    }

}
