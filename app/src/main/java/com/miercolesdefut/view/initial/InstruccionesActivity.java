package com.miercolesdefut.view.initial;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.widget.Button;
import android.widget.ImageView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.miercolesdefut.R;
import com.miercolesdefut.logic.configurations.InstruccionesMessages;
import com.miercolesdefut.logic.model.rxbux.App;
import com.miercolesdefut.view.BaseActivity;
import com.miercolesdefut.view.initial.message.MessageFragment;
import com.jakewharton.rxbinding.support.v4.view.RxViewPager;
import com.jakewharton.rxbinding.view.RxView;

import rx.Subscription;

public class InstruccionesActivity extends BaseActivity {
    Subscription subscription;
    Subscription subscriptionPage;

    private static final String INIT_SCREEN = "onboarding";
    private Tracker mTracker;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intrucciones);

        mTracker = ((App) getApplication()).getDefaultTracker();
        mTracker.setScreenName(INIT_SCREEN);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        ViewPager pager = (ViewPager) findViewById(R.id.commentsViewPager);
        pager.setAdapter(new MessagePagerAdapter(getSupportFragmentManager()));

        TabLayout tabLayout = (TabLayout)findViewById(R.id.commentsTab);
        tabLayout.setupWithViewPager(pager);

        Button button = (Button)findViewById(R.id.comenzarBtn);
        button.setText("SIGUIENTE");
        RxView.clicks(button).subscribe(aVoid -> pager.setCurrentItem(pager.getCurrentItem()+1));

        ImageView background= (ImageView)findViewById(R.id.imageActivity);
        RxViewPager.pageSelections(pager).subscribe((onNext)->{
            switch (onNext){
                case 0:
                    background.setImageResource(R.drawable.instrucciones_1);
                    break;
                case 1:
                    background.setImageResource(R.drawable.instrucciones_2);
                    break;
                case 2:
                    background.setImageResource(R.drawable.instrucciones_3);
                    break;
                case 3:
                    background.setImageResource(R.drawable.instrucciones_4);
                    break;
                case 4:
                    background.setImageResource(R.drawable.instrucciones_5);
                    button.setText("SIGUIENTE");
                    RxView.clicks(button).subscribe(aVoid -> pager.setCurrentItem(pager.getCurrentItem()+1));
                    break;
                case 5:
                    background.setImageResource(R.drawable.instrucciones_6);
                    button.setText("COMENZAR");
                    subscription = RxView.clicks(button).subscribe(aVoid -> {
                        mTracker.send(new HitBuilders.EventBuilder().setCategory("button").setAction("touch").setLabel("onboarding-start").build());
                        callInitMakeTeamOfMainActivity(true);
                    });
                    break;
            }
        });

    }

    public  static class MessagePagerAdapter extends FragmentPagerAdapter {
        private InstruccionesMessages instrucciones = InstruccionesMessages.getInstances();

        public MessagePagerAdapter(FragmentManager fm){
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return MessageFragment.newInstance(instrucciones.getMessage(position));
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return "      ";
        }

        @Override
        public int getCount() {
            return instrucciones.getLength();
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        try {
            subscription.unsubscribe();
        }catch(NullPointerException ne){}
    }
}
