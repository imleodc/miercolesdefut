package com.miercolesdefut.view;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.miercolesdefut.R;
import com.trello.rxlifecycle.components.support.RxFragment;

/**
 * Created by lauro on 09/01/2017.
 */

public class BaseFragment extends RxFragment {

    private AppCompatActivity activity;
    private FragmentManager manager;
    private ActionBar actionBar;
    String title;
    boolean showBar = true;
    private int imageBackground = -1;
    private boolean showImageLogoBottom = false;

    protected boolean isConected(){
        ConnectivityManager conMgr = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo i = conMgr.getActiveNetworkInfo();
        if (i == null)
            return false;
        if (!i.isConnected())
            return false;
        if (!i.isAvailable())
            return false;
        return true;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (AppCompatActivity)getActivity();
        manager = activity.getSupportFragmentManager();
        actionBar = activity.getSupportActionBar();
        if(imageBackground != -1){
            changeImageBackground(imageBackground);
        }
    }

    protected void showActionBar(){
        showActionBar(null);
    }
    protected void showActionBar(String title){
        this.title = title;
        if(actionBar == null){
            return;
        }
        actionBar.show();
        ((BaseActivity)activity).updateTitleBar(title);
        actionBar.setBackgroundDrawable(ContextCompat.getDrawable(getActivity(), R.color.color_active_bar));
        if(title == null){
            return;
        }
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if(title != null){
            showActionBar(title);
        }
        if(!showBar){
            hideActionBar();
        }
        if(imageBackground != -1){
            changeImageBackground(imageBackground);
        }
        setVisibleImageLogoBottom(isShowImageLogoBottom());
    }

    protected void hideActionBar(){
        showBar = false;
        actionBar.hide();
    }

    protected void changeImageBackground(final int imageResource){
        ImageView image = (ImageView) getActivity().findViewById(R.id.imageBackground);
        if(image != null) {
            image.setImageResource(imageResource);
        }
    }

    protected void setVisibleImageLogoBottom(boolean visible){
        ImageView image = (ImageView) getActivity().findViewById(R.id.imageLogoBottom);
        if(image != null){
            if(visible){
                image.setVisibility(View.VISIBLE);
            } else {
                image.setVisibility(View.INVISIBLE);
            }
        }
    }

    public int getImageBackground() {
        return imageBackground;
    }

    public void setImageBackground(int imageBackground) {
        this.imageBackground = imageBackground;
    }

    public boolean isShowImageLogoBottom() {
        return showImageLogoBottom;
    }

    public void setShowImageLogoBottom(boolean showImageLogoBottom) {
        this.showImageLogoBottom = showImageLogoBottom;
    }
}
