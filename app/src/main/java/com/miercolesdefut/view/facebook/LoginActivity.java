package com.miercolesdefut.view.facebook;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Button;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.miercolesdefut.R;
import com.miercolesdefut.logic.configurations.FacebookUtils;
import com.miercolesdefut.logic.model.facebook.ServiceFacebook;
import com.miercolesdefut.logic.model.facebook.ServiceFacebookImp;
import com.miercolesdefut.logic.model.rxbux.App;
import com.miercolesdefut.view.BaseActivity;
import com.miercolesdefut.view.initial.BirthdayActivity;
import com.miercolesdefut.view.initial.RegistrationActivity;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class LoginActivity extends BaseActivity {
    private CallbackManager callbackManager;
    private ProfileTracker profileTracker = null;
    ServiceFacebook serviceFacebook = new ServiceFacebookImp();

    private final String INIT_SCREEN = "facebook/login";

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Tracker mTracker = ((App) getApplication()).getDefaultTracker();
        mTracker.setScreenName(INIT_SCREEN);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        serviceFacebook.initializeFacebook(getApplicationContext());
        callbackManager = serviceFacebook.getCallBackManager();
        setContentView(R.layout.activity_login);
        if(FacebookUtils.isLogin(this)) {
            callNextActivity();
            return;
        }
        LoginButton login = (LoginButton) findViewById(R.id.login_button);
        serviceFacebook.registerPermissions(login,Arrays.asList("public_profile", "email","user_friends","read_custom_friendlists"));
        serviceFacebook.registerCallBack(login, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                FacebookUtils.saveCredentials(getApplicationContext(), loginResult.getAccessToken());
                callNextActivity();
            }

            @Override
            public void onCancel() {
                //Log.d("login","login canceled");
            }

            @Override
            public void onError(FacebookException error) {
                loginError();
            }
        });
        /****/
        hideActionBar();
    }

    private void loginError(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Error en la conexión a internet, por favor inténtelo más tarde.");
        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which){
                //Do nothing here because we override this button later to change the close behaviour.
                //However, we still need this because on older versions of Android unless we
                //pass a handler the button doesn't get instantiated
            }
        });

        final AlertDialog dialog = builder.create();
        dialog.show();
    }

    protected void callNextActivity(){
        Map<String, String> parameters = new HashMap<>();
        String birthday = getBirthday();
        parameters.put(BirthdayActivity.BIRTHDAY_TEXT, birthday);
        callActivy(RegistrationActivity.class, parameters);
    }

    protected String getBirthday(){
        return getIntent().getStringExtra(BirthdayActivity.BIRTHDAY_TEXT);
    }

    public void hideActionBar(){
        getSupportActionBar().hide();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode,resultCode,data);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(FacebookUtils.isLogin(getApplicationContext())){
            callNextActivity();
        }
    }
}
