package com.miercolesdefut.view.user;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.Tracker;
import com.miercolesdefut.R;
import com.miercolesdefut.ServiceCoronaImp;
import com.miercolesdefut.logic.ServiceCorona;
import com.miercolesdefut.logic.configurations.FacebookUtils;
import com.miercolesdefut.logic.dao.Invitacion;
import com.miercolesdefut.logic.model.rxbux.App;
import com.miercolesdefut.view.BaseActivity;
import com.miercolesdefut.view.BaseFragment;
import com.miercolesdefut.view.user.dummy.DummyContent.DummyItem;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;
import rx.functions.Action1;

public class FichajesFragment extends BaseFragment {
    private ServiceCorona serviceCorona = new ServiceCoronaImp();
    private OnListFragmentInteractionListener mListener;
    RecyclerView recyclerView;
    boolean isAlert = false;

    public FichajesFragment() {
        setImageBackground(R.drawable.fichajes);
    }

    public static FichajesFragment newInstance() {
        FichajesFragment fragment = new FichajesFragment();
        return fragment;
    }

    public static FichajesFragment newInstance(boolean isAlert) {
        FichajesFragment fragment = new FichajesFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean(FichajesIntermediateFragment.KEY_IS_ALERT, isAlert);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null){
            isAlert = getArguments().getBoolean(FichajesIntermediateFragment.KEY_IS_ALERT, false);
        }
        if(!isAlert) {
            showActionBar(getResources().getString(R.string.fichajes));
        }

        if(!isConected()){
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage("Error en la conexión a internet, por favor inténtelo más tarde.");
            builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which){
                    //Do nothing here because we override this button later to change the close behaviour.
                    //However, we still need this because on older versions of Android unless we
                    //pass a handler the button doesn't get instantiated
                }
            });

            final AlertDialog dialog = builder.create();
            dialog.show();
            dialog.setCancelable(false);

            Button button = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
            button.setOnClickListener(v -> {
                if(isConected()) {
                    dialog.dismiss();
                    init();
                }else{
                    button.setText("Reintentando");
                    Handler handler = new Handler();
                    handler.postDelayed(() -> {
                        button.setText("Aceptar");
                        if(isConected()) {
                            dialog.dismiss();
                            init();
                        }
                    }, 1000);
                }
            });

            return;
        }

        init();
    }

    private void init(){
        final ProgressDialog progressDialog = BaseActivity.createProgressDialog(getContext());
        progressDialog.show();
        serviceCorona.getInvitaciones(FacebookUtils.getId(getContext()), new Action1<Response<List<Invitacion>>>() {
            @Override
            public void call(Response<List<Invitacion>> listResponse) {
                progressDialog.dismiss();
                if(listResponse.isSuccessful()){
                    List<Invitacion> invitaciones = listResponse.body();
                    if(invitaciones.isEmpty()){
                        TextView textView =  (TextView) getActivity().findViewById(R.id.textView20);
                        if(textView != null){
                            textView.setText(R.string.sinFichajes);
                        }
                    }
                    ((FichajeItemRecyclerViewAdapter)recyclerView.getAdapter()).addItemsToList(listResponse.body());
                } else {
                    try {
                        Toast.makeText(getContext(), listResponse.errorBody().string(), Toast.LENGTH_SHORT).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = null;
        if(isAlert){
            view = inflater.inflate(R.layout.alert_fichajeitem_list, container, false);
        }else {
            view = inflater.inflate(R.layout.fragment_fichajeitem_list, container, false);
        }
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            recyclerView = (RecyclerView) view;
            recyclerView.setLayoutManager(new LinearLayoutManager(context));

            FichajeItemRecyclerViewAdapter fichajeItemRecyclerViewAdapter = new FichajeItemRecyclerViewAdapter(new ArrayList<>(), mListener, isAlert);

            Tracker mTracker = ((App) getActivity().getApplication()).getDefaultTracker();

            fichajeItemRecyclerViewAdapter.setmTracker(mTracker);



            recyclerView.setAdapter(fichajeItemRecyclerViewAdapter);
        }
        return view;
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void dissmisAlertDialogFichajes(DummyItem item);
    }
}
