package com.miercolesdefut.view.user;

import android.app.ProgressDialog;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.miercolesdefut.R;
import com.miercolesdefut.ServiceCoronaImp;
import com.miercolesdefut.logic.ConstantsApplication;
import com.miercolesdefut.logic.ServiceCorona;
import com.miercolesdefut.logic.configurations.FacebookUtils;
import com.miercolesdefut.logic.dao.AcceptTeam;
import com.miercolesdefut.logic.dao.DeclineTeam;
import com.miercolesdefut.logic.dao.Invitacion;
import com.miercolesdefut.logic.dao.Picture;
import com.miercolesdefut.logic.model.facebook.ServiceFacebook;
import com.miercolesdefut.logic.model.facebook.ServiceFacebookImp;
import com.miercolesdefut.view.BaseActivity;
import com.miercolesdefut.view.user.FichajesFragment.OnListFragmentInteractionListener;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Response;
import rx.functions.Action1;

public class FichajeItemRecyclerViewAdapter extends RecyclerView.Adapter<FichajeItemRecyclerViewAdapter.ViewHolder> {
    ServiceFacebook serviceFacebook = new ServiceFacebookImp();
    private final List<Invitacion> mValues;
    private final OnListFragmentInteractionListener mListener;
    private final ServiceCorona serviceCorona = new ServiceCoronaImp();
    private boolean isAlert = false;

    private Tracker mTracker;

    public void setmTracker(Tracker mTracker) {
        this.mTracker = mTracker;
    }

    public FichajeItemRecyclerViewAdapter(List<Invitacion> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    public FichajeItemRecyclerViewAdapter(List<Invitacion> items, OnListFragmentInteractionListener listener, boolean isAlert) {
        this(items, listener);
        this.isAlert = isAlert;
    }



    public void addItemsToList(List<Invitacion> friends) {
        if(friends == null && friends.isEmpty()){
            return;
        }
        mValues.addAll(friends);
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        if(isAlert) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.fragment_list_fichajes_alert, parent, false);
        } else {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.fragment_fichajeitem, parent, false);
        }
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.invitacion = mValues.get(position);
        holder.nombreEquipo.setText(holder.invitacion.getName());
        holder.nombrePropietario.setText(holder.invitacion.getOwner().getFull_name());
        holder.mView.getBackground().setAlpha(255);
        holder.aceptar.setOnClickListener(v -> {
            String label = (isAlert) ? "alert-invitations-accept":"invitations-accept";

            if( mTracker != null ){
                mTracker.send(new HitBuilders.EventBuilder().setCategory("button").setAction("touch").setLabel(label).setValue(holder.getItemId()).build());
            }

            AcceptTeam aceptar = new AcceptTeam();
            aceptar.setFacebook_id(FacebookUtils.getId(v.getContext()));
            aceptar.setTeam_id(holder.invitacion.getId());
            final ProgressDialog progressDialog = BaseActivity.createProgressDialog(v.getContext());
            progressDialog.show();
            serviceCorona.aceptarInvitacion(aceptar, new Action1<Response<ResponseBody>>() {
                @Override
                public void call(Response<ResponseBody> response) {
                    progressDialog.dismiss();
                    if(response.isSuccessful()){
                        mValues.remove(holder.invitacion);
                        notifyDataSetChanged();
                        Toast.makeText(v.getContext(), v.getResources().getString(R.string.fichaje_aceptado), Toast.LENGTH_SHORT).show();
                        callDismissDialog();
                    } else {
                        try {
                            JSONObject json = new JSONObject(response.errorBody().string());
                            Toast.makeText(v.getContext().getApplicationContext(), json.getJSONArray("facebook_id").getString(0), Toast.LENGTH_SHORT).show();
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch(JSONException e){
                            e.printStackTrace();
                        }
                    }
                }
            });
        });

        holder.cancelar.setOnClickListener(v -> {
            String label = (isAlert) ? "alert-invitations-cancel":"invitations-cancel";

            if( mTracker != null ){
                mTracker.send(new HitBuilders.EventBuilder().setCategory("button").setAction("touch").setLabel(label).setValue(holder.getItemId()).build());
            }

            DeclineTeam cancelar = new DeclineTeam();
            cancelar.setFacebook_id(FacebookUtils.getId(v.getContext()));
            cancelar.setTeam_id(holder.invitacion.getId());
            final ProgressDialog progressDialog = BaseActivity.createProgressDialog(v.getContext());
            progressDialog.show();

            serviceCorona.cancelarInvitacion(cancelar, response -> {
                progressDialog.dismiss();
                if(response.isSuccessful()){
                    mValues.remove(holder.invitacion);
                    notifyDataSetChanged();
                    Toast.makeText(v.getContext(), v.getResources().getString(R.string.fichaje_cancelado), Toast.LENGTH_SHORT).show();
                    callDismissDialog();
                }
            });
        });
        if(isAlert){
            if(holder.invitacion.getUrlImage() == null){
                serviceFacebook.completeFriendPicture(FacebookUtils.restoreCredentials(holder.mView.getContext()),holder.invitacion.getOwner().getFacebook_id(),holder.pictures);
            }
        }
    }

    private void callDismissDialog(){
        if(!isAlert){
            return;
        }
        if(mValues.isEmpty()){
            Fragment dialogFichajes = ConstantsApplication.getInstances().getFragmentDialog();
            if(dialogFichajes != null){
                ConstantsApplication.getInstances().setFragmentDialog(null);
                ((DialogFragment)dialogFichajes).dismiss();
            }
        }
    }



    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView nombreEquipo;
        public final TextView nombrePropietario;
        public final ImageButton aceptar;
        public final ImageButton cancelar;
        public Invitacion invitacion;
        public ImageView fichajeImage;
        public Picture pictures = new Picture() {
            @Override
            public void setPicture(String url) {
                //invitacion.setUrlImage(url);
                Picasso.with(mView.getContext()).load(url).into(fichajeImage);
            }

            @Override
            public String getPicture() {
                return null;
            }
        };

        public ViewHolder(View view) {
            super(view);
            mView = view;
            nombreEquipo = (TextView) view.findViewById(R.id.fichajeNombreEquipo);
            nombrePropietario = (TextView) view.findViewById(R.id.fichajePropietarioEquipo);
            aceptar = (ImageButton) view.findViewById(R.id.fichajeAceptarBtn);
            cancelar = (ImageButton) view.findViewById(R.id.fichajeCancelarBtn);
            if(isAlert){
                fichajeImage = (ImageView) view.findViewById(R.id.fichajeImage);
            }
        }
    }
}
