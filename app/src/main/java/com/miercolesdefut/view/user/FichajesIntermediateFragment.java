package com.miercolesdefut.view.user;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.miercolesdefut.R;
import com.miercolesdefut.logic.model.rxbux.App;
import com.miercolesdefut.view.buissnes.alerts.FichajesDialogFragment;

public class FichajesIntermediateFragment extends Fragment {

    private boolean isAlert = false;
    public static final String KEY_IS_ALERT = "keyIsAlert";

    private String INIT_SCREEN = "invitations";

    public static FichajesIntermediateFragment newInstance() {
        FichajesIntermediateFragment fragment = new FichajesIntermediateFragment();
        return fragment;
    }

    public static FichajesIntermediateFragment newInstance(boolean isAlert) {
        FichajesIntermediateFragment fragment = new FichajesIntermediateFragment();
        Bundle args = new Bundle();
        args.putBoolean(KEY_IS_ALERT, isAlert);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fichajes_intermediate, container, false);
        if(getArguments() !=  null){
            isAlert = getArguments().getBoolean(KEY_IS_ALERT,false);
        }

        if(isAlert){
            TextView message = (TextView)view.findViewById(R.id.textView20);
            message.setVisibility(View.GONE);
            TextView fichajes = (TextView) view.findViewById(R.id.fichajesTextAlert);
            if(fichajes != null) {
                fichajes.setVisibility(View.VISIBLE);
            }
        }else{
            Tracker mTracker = ((App) getActivity().getApplication()).getDefaultTracker();
            mTracker.setScreenName(INIT_SCREEN);
            mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        }

        getFragmentManager().beginTransaction().replace(R.id.layoutFichajes, FichajesFragment.newInstance(isAlert)).commit();
        return view;
    }

    public void dismiss(){
        Fragment fragment = getParentFragment();
        if(fragment != null && fragment instanceof FichajesDialogFragment){
            ((DialogFragment)fragment).dismiss();
        }
    }
}
