package com.miercolesdefut.view.user.location;

import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.TextView;

import com.miercolesdefut.view.BaseActivity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

/**
 * Created by lauro on 16/01/2017.
 */

public class LocationService extends BaseActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {
    LocationRequest locationRequest;
    GoogleApiClient mGoogleApliClient;
    private TextView locationTextView;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        locationTextView = new TextView(this);
        locationTextView.setBackgroundColor(Color.WHITE);
        getSupportActionBar().hide();
        mGoogleApliClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApliClient.connect();
    }

    @Override
    public void onStop() {
        mGoogleApliClient.disconnect();
        super.onStop();
    }

    //LocationClient
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(1000);
        if ( Build.VERSION.SDK_INT >= 23 &&
                ContextCompat.checkSelfPermission( this, android.Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED ) {
            return  ;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApliClient,locationRequest,this);
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i("LOCATION","Suspended connection");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.i("LOCATION", "Connection failed");
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.i(LOCATION_SERVICE,"Location received"+ location.toString());
        locationTextView.setText("Location received :" + location.toString());
    }
}
