package com.miercolesdefut.view.user;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.miercolesdefut.R;
import com.miercolesdefut.logic.dao.MetaDataDAO;
import com.miercolesdefut.logic.model.Estados;

public class UserDataFragment extends Fragment {
    MetaDataDAO datosApplicacion;
    private OnFragmentInteractionListener mListener;

    public UserDataFragment() {}

    // TODO: Rename and change types and number of parameters
    public static UserDataFragment newInstance() {
        UserDataFragment fragment = new UserDataFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    private void fillEstados() {
        Spinner spinner = (Spinner) getView().findViewById(R.id.spinnerEstados);
        ArrayAdapter<Estados> estados = new ArrayAdapter<>(getContext(),android.R.layout.simple_spinner_dropdown_item,datosApplicacion.getEstados());
        spinner.setAdapter(estados);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_user_data, container, false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            System.out.println("error:attach");
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }



    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
