package com.miercolesdefut.view.buissnes.teams.detalle;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ImageSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.miercolesdefut.R;
import com.miercolesdefut.logic.ConstantsApplication;
import com.miercolesdefut.logic.dao.Matche;
import com.miercolesdefut.logic.dao.Team;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.miercolesdefut.logic.model.rxbux.App;
import com.squareup.picasso.Picasso;

public class YaLlegueFragment extends DetalleEquipoActivoFragment {

    public static final String ARG_TEAM = "argumentTeam";
    public static final String ARG_MATCH= "argumentMatch";
    private Team team;
    private Matche match;
    private OnRequestYaLlegueListener requestYaLlegue;

    private static final String INIT_SCREEN = "team/unchecked";

    public static YaLlegueFragment newInstance() {
        YaLlegueFragment fragment = new YaLlegueFragment();
        return fragment;
    }

    public static YaLlegueFragment newInstance(Team team ){
        YaLlegueFragment fragment = new YaLlegueFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_TEAM, team);
        fragment.setArguments(args);
        return fragment;
    }

    public static YaLlegueFragment newInstance(Team team , Matche matche){
        YaLlegueFragment fragment = new YaLlegueFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_TEAM, team);
        args.putSerializable(ARG_MATCH, matche);
        fragment.setArguments(args);
        return fragment;
    }


    public YaLlegueFragment(){
        setImageBackground(R.drawable.fondo);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null){
            team = (Team) getArguments().getSerializable(ARG_TEAM);
            match = (Matche) getArguments().getSerializable(ARG_MATCH);
        }

        Tracker mTracker = ((App) getActivity().getApplication()).getDefaultTracker();
        mTracker.setScreenName(INIT_SCREEN);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ya_llegue, container, false);
        if(ConstantsApplication.getInstances().isMakeChecking()){
            getFragmentManager().popBackStack();
            return view;
        }
        updatePartidoView(view);
        Button button = (Button) view.findViewById(R.id.llegueBtn);
        button.setOnClickListener((event)->{
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setMessage(getString(R.string.confirm_check))
                    .setCancelable(false)
                    .setPositiveButton("Si", (dialog, id) -> {
                        if(match == null){
                            requestYaLlegue.requestMatch(team);
                        } else {
                            requestYaLlegue.detailtViewMap(team, match);
                        }
                    })
                    .setNegativeButton("No", (dialog, id) -> dialog.cancel());
            AlertDialog alert = builder.create();
            alert.show();
        });
        showActionBar(getResources().getString(R.string.misEquipos));
        if(button != null) {
            Spannable llegueBtnLabel = new SpannableString(getResources().getString(R.string.yaLlegue));
            llegueBtnLabel.setSpan(new ImageSpan(getContext(), R.drawable.pin), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            button.setText(llegueBtnLabel);
        }
        if(team != null) {
            TextView teamName = (TextView)view.findViewById(R.id.teamName);
            teamName.setText(team.getName());
        }
        rxPermissions
                .request(Manifest.permission.ACCESS_FINE_LOCATION)
                .subscribe((granted)->{
                    if(granted){
                        configMap(view, savedInstanceState);
                        trackPosition((location)->{
                            if(mapView != null){
                                mapView.getMapAsync((mMap)->{
                                    LatLng ref = new LatLng(location.getLatitude(), location.getLongitude());
                                    CameraPosition cameraPosition = new CameraPosition.Builder().target(ref).zoom(18).build();
                                    mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                                });
                            }
                        });
                    } else {
                        Toast.makeText(getContext(), "Se requieren los permisos de GPS.", Toast.LENGTH_LONG);
                        getFragmentManager().popBackStack();
                    }
                });
        getFragmentManager().beginTransaction().add(R.id.friendsCreateTeam, DetalleListaAmgigos.newInstance(team.getMembers(), true)).commit();
        return view;
    }

    public interface OnRequestYaLlegueListener {
        void requestMatch(Team team);
        void detailtViewMap(Team team, Matche matche);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof  OnRequestYaLlegueListener){
            requestYaLlegue = (OnRequestYaLlegueListener) context;
        }
    }

    protected void updatePartidoView(View view){
        ImageView imViewA = (ImageView) view.findViewById(R.id.equipoAImagen);
        ImageView imViewB = (ImageView) view.findViewById(R.id.equipoBImagen);
        TextView equipoAN = (TextView) view.findViewById(R.id.equipoANombre);
        TextView equipoBN = (TextView) view.findViewById(R.id.equipoBNombre);
        TextView horarioPartido = (TextView) view.findViewById(R.id.horarioPartido);
        View layoutMatch = view.findViewById(R.id.layoutMatch);
        if(match == null){
            layoutMatch.setVisibility(View.GONE);
            return;
        }
        if(imViewA != null ){
            Picasso.with(getContext()).load(match.getTeam_a().getImage()).into(imViewA);
        }
        if(imViewB != null ){
            Picasso.with(getContext()).load(match.getTeam_b().getImage()).into(imViewB);
        }
        if(equipoAN != null){
            equipoAN.setText(match.getTeam_a().getName());
        }
        if(equipoBN != null){
            equipoBN.setText(match.getTeam_b().getName());
        }
        if(horarioPartido != null){
            horarioPartido.setText(match.getStart_time_format());
        }
    }

    @Override
    protected void registeredEvents() {
    }

    /*protected void configMap(View container, Bundle savedInstanceState){
        mapView = (MapView) container.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.onResume();
        MapsInitializer.initialize(getActivity().getApplicationContext());
        mapView.getMapAsync((mMap)->{
            mMap.setMyLocationEnabled(true);
            LatLng latLng= new LatLng(19.4284700, -99.1276600);
            CameraUpdate cameraPosition = CameraUpdateFactory.newLatLngZoom(latLng, 5);
            mMap.moveCamera(cameraPosition);
            mMap.animateCamera(cameraPosition);
        });

        mapView.setOnFocusChangeListener((v, hasFocus)->{
            if(hasFocus){
                mainScroll.requestDisallowInterceptTouchEvent(true);
            } else {
                mainScroll.requestDisallowInterceptTouchEvent(false);
            }
        });
    }*/


    @Override
    public void onDetach() {
        super.onDetach();
        requestYaLlegue = null;
    }
}
