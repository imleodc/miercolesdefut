package com.miercolesdefut.view.buissnes.dialogs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.miercolesdefut.R;
import com.miercolesdefut.logic.dao.LocationError;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by lauro on 09/02/2017.
 */

public class MapDialogFragment extends DialogFragment {
    public static final String REFERENCE_LOCATION = "referenceLocation";
    public static final String REFERENCE_MESSAGE = "referenceMessage";

    private LocationError.ReferencesError referenceError;
    private String messageTxt;
    public static MapDialogFragment newInstance(LocationError.ReferencesError reference) {
        Bundle args = new Bundle();
        args.putSerializable(REFERENCE_LOCATION,reference);
        MapDialogFragment fragment = new MapDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static MapDialogFragment newInstance(String message, LocationError.ReferencesError reference) {
        Bundle args = new Bundle();
        args.putSerializable(REFERENCE_LOCATION,reference);
        args.putSerializable(REFERENCE_MESSAGE,message);
        MapDialogFragment fragment = new MapDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null){
            referenceError = (LocationError.ReferencesError) getArguments().getSerializable(REFERENCE_LOCATION);
            messageTxt = (String) getArguments().getSerializable(REFERENCE_MESSAGE);
        }
    }

    MapView mapView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_dialog_map, container, false);
        mapView = (MapView) v.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.onResume();
        MapsInitializer.initialize(getActivity().getApplicationContext());
        if(referenceError != null){
            TextView message = (TextView) v.findViewById(R.id.textView23);
            message.setText(messageTxt);
            /*
            mapView.getMapAsync((mMap)->{
                mMap.setMyLocationEnabled(true);
                LatLng referencesLocation = new LatLng(referenceError.getLatitude(), referenceError.getLongitude());
                //LatLng latLng= new LatLng(19.4284700, -99.1276600);
                mMap.addMarker( new MarkerOptions().position(referencesLocation).title("amigos"));
                CameraPosition cameraPosition = new CameraPosition.Builder().target(referencesLocation).zoom(12).build();
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            });
            */
        }
        return v;
    }



    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }
}
