package com.miercolesdefut.view.buissnes.alerts;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.miercolesdefut.R;

import java.util.Map;

/**
 * Created by lauro on 20/01/2017.
 */

public class CoronaAlertDialog extends DialogFragment {

    public static final String TERMINOS_Y_CONDICIONES = "terminosYCondiciones";
    public static final String CHECK_UBICACIONES = "checkUbicacionesShow";
    public static final String ERROR_MESSAGE = "errorMessage";
    public static final String ACTION_MESSAGE= "actionMessage";
    public static final String ACTION_AUTO_CHECKING= "autoChecking";
    public static final String ACTION_AUTO_CHECKING_TEAM  = "autoCheckingTeam";
    public static final String ACTION_AUTO_CHECKING_POINTS = "autoCheckingPoints";
    public static final String ACTION_CHECKIN_LEJOS= "autoCheckingPointsLast";
    public static final String SHARE_PICTURE= "sharePictureValid";
    public static final String ERROR_NO_TEAMS= "errorNoTeams";



    public static final String MESSAGE = "message";


    public static CoronaAlertDialog newInstance(String tipo) {
        Bundle args = new Bundle();
        args.putBoolean(tipo, true);
        CoronaAlertDialog fragment = new CoronaAlertDialog();
        fragment.setArguments(args);
        return fragment;
    }

    public static CoronaAlertDialog newInstance(String tipo, String message) {
        Bundle args = new Bundle();
        args.putBoolean(tipo, true);
        args.putString(MESSAGE, message);
        CoronaAlertDialog fragment = new CoronaAlertDialog();
        fragment.setArguments(args);
        return fragment;
    }

    public static CoronaAlertDialog newInstance(String tipo, Map<String, String> parameters) {
        Bundle args = new Bundle();
        args.putBoolean(tipo, true);
        for(String key:parameters.keySet()){
            args.putString(key, parameters.get(key));
        }
        CoronaAlertDialog fragment = new CoronaAlertDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.custom_alert_layout,null);
        ImageButton close = (ImageButton) view.findViewById(R.id.coronaButtonClose);
        close.setColorFilter(getResources().getColor(R.color.color_btn));
        close.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        if(getArguments() != null) {
            if(getArguments().getBoolean(TERMINOS_Y_CONDICIONES)){
                buildTerminos(view, inflater);
            } else {
                if(getArguments().getBoolean(ERROR_MESSAGE)){
                    buildError(view, inflater, getArguments().getString(MESSAGE));
                } else {
                    if(getArguments().getBoolean(ACTION_MESSAGE)){
                        buildActionMessage(view, inflater, getArguments().getString(MESSAGE));
                    } else {
                        if(getArguments().getBoolean(ACTION_AUTO_CHECKING)){
                            buildMessageAutoChecking(view, inflater);
                        } else {
                            if(getArguments().getBoolean(ACTION_CHECKIN_LEJOS)){
                                buildMessageLejosChecking(view, inflater);
                            }else{
                                if(getArguments().getBoolean(SHARE_PICTURE)){
                                    buildMessageSharePicture(view, inflater);
                                }else{
                                    if(getArguments().getBoolean(ERROR_NO_TEAMS)){
                                        buildMessageNoTeam(view, inflater);
                                    } else {
                                        if(getArguments().getBoolean(CHECK_UBICACIONES)){
                                            buildCheckUbicaciones(view, inflater);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        /**/
        builder.setView(view);
        return builder.create();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        try {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }catch (NullPointerException ex){}

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    private void buildMessageSharePicture(View view, LayoutInflater inflate) {
        String teamName = getArguments().getString(ACTION_AUTO_CHECKING_TEAM, "");
        String points = getArguments().getString(ACTION_AUTO_CHECKING_POINTS, "0");
        //String points = "10";
        FrameLayout layout = (FrameLayout) view.findViewById(R.id.alertContentLayout);

        View autoChecking  = inflate.inflate(R.layout.checking_automatico_valido, null);

        TextView message = (TextView) autoChecking.findViewById(R.id.textView24);
        message.setText(view.getResources().getString(R.string.image_share_correctly));

        TextView messageTeamName = (TextView) autoChecking.findViewById(R.id.textView25);
        TextView messageTeamPoints = (TextView) autoChecking.findViewById(R.id.textView26);

        teamName = view.getResources().getString(R.string.message_checking_2, teamName);
        points = view.getResources().getString(R.string.message_checking_3, points);

        messageTeamName.setText(teamName);
        messageTeamPoints.setText(points);


        autoChecking.setAlpha(1);
        autoChecking.setBackgroundColor(Color.WHITE);

        layout.addView(autoChecking);

        if(points.equals("0 PUNTOS")){
            //messageTeamName.setVisibility(View.GONE);
            autoChecking.findViewById(R.id.layout_points).setVisibility(View.GONE);
        }
    }

    private void buildMessageLejosChecking(View view, LayoutInflater inflater) {
        FrameLayout layout = (FrameLayout) view.findViewById(R.id.alertContentLayout);
        View autoChecking  = inflater.inflate(R.layout.lejos_equipo, null);
        autoChecking.setBackgroundColor(Color.WHITE);
        layout.addView(autoChecking);
    }


    private void buildTerminos(View view, LayoutInflater inflater) {
        FrameLayout layout = (FrameLayout) view.findViewById(R.id.alertContentLayout);

        WebView webView = (WebView) inflater.inflate(R.layout.terminos_web, null);
        layout.addView(webView);

        String url = "https://www.miercolesdefut.com.mx/bases.html";
        webView.loadUrl(url);
    }

    private void buildCheckUbicaciones(View view, LayoutInflater inflater) {
        FrameLayout layout = (FrameLayout) view.findViewById(R.id.alertContentLayout);

        WebView webView = (WebView) inflater.inflate(R.layout.terminos_web, null);
        layout.addView(webView);

        String url = "https://miercolesdefut.com.mx/ubicaciones.html";
        webView.loadUrl(url);
    }

    private void buildError(View view, LayoutInflater inflater, String message) {
        FrameLayout layout = (FrameLayout) view.findViewById(R.id.alertContentLayout);
        TextView terminos = (TextView) inflater.inflate(R.layout.terminos_y_condiciones, null);
        terminos.setText(message);
        layout.addView(terminos);
    }

    private void buildActionMessage(View view, LayoutInflater inflater, String message) {
        FrameLayout layout = (FrameLayout) view.findViewById(R.id.alertContentLayout);
        TextView terminos = (TextView) inflater.inflate(R.layout.terminos_y_condiciones, null);
        terminos.setText(message);
        layout.addView(terminos);
    }

    private void buildMessageAutoChecking(View view, LayoutInflater inflate){
        String teamName = getArguments().getString(ACTION_AUTO_CHECKING_TEAM, "");
        //String points = getArguments().getString(ACTION_AUTO_CHECKING_POINTS, "0");
        String points = "100";
        FrameLayout layout = (FrameLayout) view.findViewById(R.id.alertContentLayout);

        View autoChecking  = inflate.inflate(R.layout.checking_automatico_valido, null);
        TextView messageTeamName = (TextView) autoChecking.findViewById(R.id.textView25);
        TextView messageTeamPoints = (TextView) autoChecking.findViewById(R.id.textView26);
        teamName = view.getResources().getString(R.string.message_checking_2, teamName);
        points = view.getResources().getString(R.string.message_checking_3, points);
        messageTeamName.setText(teamName);
        messageTeamPoints.setText(points);
        autoChecking.setAlpha(1);
        autoChecking.setBackgroundColor(Color.WHITE);
        layout.addView(autoChecking);
    }

    private void buildMessageNoTeam(View view, LayoutInflater inflate){
        FrameLayout layout = (FrameLayout) view.findViewById(R.id.alertContentLayout);

        View autoChecking  = inflate.inflate(R.layout.error_no_teams, null);

        layout.addView(autoChecking);
    }


}
