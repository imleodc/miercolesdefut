package com.miercolesdefut.view.buissnes.teams;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.miercolesdefut.R;
import com.miercolesdefut.ServiceCoronaImp;
import com.miercolesdefut.logic.ServiceCorona;
import com.miercolesdefut.logic.configurations.FacebookUtils;
import com.miercolesdefut.logic.dao.Matche;
import com.miercolesdefut.logic.model.UpdaterListData;
import com.miercolesdefut.view.BaseActivity;
import com.miercolesdefut.view.BaseFragment;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import retrofit2.Response;
import rx.functions.Action1;

public class ProxPartidosFragment extends BaseFragment {
    ServiceCorona serviceCorona = new ServiceCoronaImp();
    RecyclerView recyclerView;
    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "mainActivity";
    // TODO: Customize parameters
    private boolean mainActivity = false;

    protected boolean isConected(){
        ConnectivityManager conMgr = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo i = conMgr.getActiveNetworkInfo();
        if (i == null)
            return false;
        if (!i.isConnected())
            return false;
        if (!i.isAvailable())
            return false;
        return true;
    }
    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ProxPartidosFragment() {
        setImageBackground(R.drawable.registro);
    }

    // TODO: Customize parameter initialization
    public static ProxPartidosFragment newInstance(boolean mainActivity){
        ProxPartidosFragment fragment = new ProxPartidosFragment();
        Bundle args = new Bundle();
        args.putBoolean(ARG_COLUMN_COUNT, mainActivity);
        fragment.setArguments(args);
        return fragment;
    }
    public static ProxPartidosFragment newInstance(int columnCount) {
        ProxPartidosFragment fragment = new ProxPartidosFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mainActivity = getArguments().getBoolean(ARG_COLUMN_COUNT);
        }
        if(mainActivity){
            showActionBar(getResources().getString(R.string.proxPartidos));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_proxpartidos_list, container, false);
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            recyclerView = (RecyclerView) view;
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            recyclerView.setAdapter(new MyProxPartidosRecyclerViewAdapter(new ArrayList<Matche>()));
        }

        if(!isConected()){
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage("Error en la conexión a internet, por favor inténtelo más tarde.");
            builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which){
                    //Do nothing here because we override this button later to change the close behaviour.
                    //However, we still need this because on older versions of Android unless we
                    //pass a handler the button doesn't get instantiated
                }
            });

            final AlertDialog dialog = builder.create();
            dialog.show();
            dialog.setCancelable(false);

            Button button = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
            button.setOnClickListener(v -> {
                if(isConected()) {
                    dialog.dismiss();
                    completeActivity();
                }else{
                    button.setText("Reintentando");
                    Handler handler = new Handler();
                    handler.postDelayed(() -> {
                        button.setText("Aceptar");
                        if(isConected()) {
                            dialog.dismiss();
                            completeActivity();
                        }
                    }, 1000);
                }
            });

            return view;
        }

        completeActivity();
        return view;
    }

    private void completeActivity() {
        final ProgressDialog progressDialog = BaseActivity.createProgressDialog(getContext());
        progressDialog.show();
        serviceCorona.getPartidos(FacebookUtils.getId(getContext()),new Action1<Response<List<Matche>>>() {
            @Override
            public void call(Response<List<Matche>> listResponse) {
                progressDialog.dismiss();
                if(listResponse.isSuccessful()){
                    List<Matche> partidos = listResponse.body();
                    Iterator<Matche> itPartidos = partidos.iterator();
                    while(itPartidos.hasNext()){
                        Matche partidoT = itPartidos.next();
                        if(partidoT.partidoEnCurso()){
                            itPartidos.remove();
                        }
                    }
                    if(partidos == null || partidos.isEmpty()){
                        View proximosPartidos = getActivity().findViewById(R.id.textView19);
                        if(proximosPartidos != null) {
                            proximosPartidos.setVisibility(View.GONE);
                        }
                        if(mainActivity){
                            View proximoPArtidosMessageMain = getActivity().findViewById(R.id.messageEmptyProxPartInt);
                            View proximoPartidoFrame = getActivity().findViewById(R.id.proxPartIntFrame);
                            if(proximoPArtidosMessageMain != null){
                                proximoPArtidosMessageMain.setVisibility(View.VISIBLE);
                                proximoPartidoFrame.setVisibility(View.GONE);
                            }
                        }
                    }
                    ((UpdaterListData<List<Matche>>)recyclerView.getAdapter()).addItemsToList(partidos);
                }
            }
        });
        setVisibleImageLogoBottom(false);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
