package com.miercolesdefut.view.buissnes.alerts;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.miercolesdefut.R;
import com.miercolesdefut.logic.ConstantsApplication;
import com.miercolesdefut.view.user.FichajesIntermediateFragment;

/**
 * Created by lauro on 11/02/2017.
 */

public class FichajesDialogFragment extends DialogFragment {


    public static final String FICHAJES_DIALOG_CALL = "fichajesDialogFragmentCall";

    public static FichajesDialogFragment newInstance() {
        FichajesDialogFragment fragment = new FichajesDialogFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.custom_alert_layout, container, false);

        try {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }catch (NullPointerException ex){}

        ImageButton close = (ImageButton) view.findViewById(R.id.coronaButtonClose);
        close.setColorFilter(getResources().getColor(R.color.color_btn));
        close.setOnClickListener((event)->dismiss());
        if(savedInstanceState == null){
            getChildFragmentManager().beginTransaction().replace(R.id.alertContentLayout, FichajesIntermediateFragment.newInstance(true),FICHAJES_DIALOG_CALL).commit();
        }
        ConstantsApplication.getInstances().setFragmentDialog(this);
        //onDismiss();
        return view;
    }
}
