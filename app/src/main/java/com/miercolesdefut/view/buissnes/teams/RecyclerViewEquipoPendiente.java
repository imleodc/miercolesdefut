package com.miercolesdefut.view.buissnes.teams;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.miercolesdefut.R;
import com.miercolesdefut.ServiceCoronaImp;
import com.miercolesdefut.logic.ServiceCorona;
import com.miercolesdefut.logic.configurations.FacebookUtils;
import com.miercolesdefut.logic.dao.FriendDAO;
import com.miercolesdefut.logic.dao.Picture;
import com.miercolesdefut.logic.dao.Team;
import com.miercolesdefut.logic.model.UpdaterListData;
import com.miercolesdefut.logic.model.facebook.ServiceFacebook;
import com.miercolesdefut.logic.model.facebook.ServiceFacebookImp;
import com.miercolesdefut.view.buissnes.teams.detalle.amigos.AmigosCerrarRecyclerView;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by lauro on 19/02/2017.
 */

public class RecyclerViewEquipoPendiente extends RecyclerView.Adapter<RecyclerViewEquipoPendiente.ViewHolder> implements UpdaterListData<List<FriendDAO>>{

    private final List<FriendDAO> mValues;
    private Team equipo;
    ServiceFacebook serviceFacebook = new ServiceFacebookImp();
    ServiceCorona serviceCorona = new ServiceCoronaImp();

    public RecyclerViewEquipoPendiente(List<FriendDAO> items) {
        this(items, null);
    }

    public RecyclerViewEquipoPendiente(List<FriendDAO> items, Team team){
        this.mValues = items;
        if(team != null){
            this.equipo = team;
        } else {
            this.equipo = new Team();
        }
    }

    @Override
    public void addItemsToList(List<FriendDAO> friends) {
        if(friends == null) {
            return;
        }

        mValues.addAll(friends);
        notifyItemRangeInserted(0,friends.size());
    }

    @Override
    public void clearList() {
        mValues.clear();
    }

    @Override
    public RecyclerViewEquipoPendiente.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_createteamfriends, parent, false);
        return new RecyclerViewEquipoPendiente.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final RecyclerViewEquipoPendiente.ViewHolder holder, final int position) {
        holder.friend = mValues.get(position);
        holder.nombreFriend.setText(mValues.get(position).getName());
        if(holder.friend.getPicture() == null) {
            serviceFacebook.completeFriendPicture(FacebookUtils.restoreCredentials(holder.mView.getContext()), holder.friend.getId(),new Picture(){
                public void setPicture(String url){
                    holder.friend.setPicture(url);
                    Picasso.with(holder.mView.getContext()).load(holder.friend.getPicture()).into(holder.imageFriend);
                }
                public String getPicture(){
                    return null;
                }
            });
        } else {
            holder.updatePicture();
        }
        holder.invitationBtn.setVisibility(View.GONE);
        //holder.cancelInvitationBtn.setVisibility(View.GONE);
        setRowTransparent(holder);

    }

    public void setRowTransparent(RecyclerViewEquipoPendiente.ViewHolder holder){
        holder.imageFriend.setAlpha(AmigosCerrarRecyclerView.TRANSPARENCI_ROW);
        holder.mView.getBackground().setAlpha(AmigosCerrarRecyclerView.TRANSPARENCI_ROW);
        holder.imageFriend.setBackgroundColor(Color.TRANSPARENT);
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView nombreFriend;
        public final ImageView imageFriend;
        //public final ImageButton cancelInvitationBtn;
        public final Button invitationBtn;
        public FriendDAO friend;


        public ViewHolder(View view) {
            super(view);
            mView = view;
            nombreFriend = (TextView) view.findViewById(R.id.fichajeNombreEquipo);
            imageFriend = (ImageView) view.findViewById(R.id.itemImageFriend);
            //cancelInvitationBtn = (ImageButton) view.findViewById(R.id.cancelBtn);
            invitationBtn = (Button) view.findViewById(R.id.invitarBtn);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + nombreFriend.getText() + "'";
        }

        public void updatePicture(){
            Picasso.with(mView.getContext()).load(friend.getPicture()).into(imageFriend);
        }
    }
}
