package com.miercolesdefut.view.buissnes.teams.detalle.amigos;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.miercolesdefut.R;
import com.miercolesdefut.ServiceCoronaImp;
import com.miercolesdefut.logic.ServiceCorona;
import com.miercolesdefut.logic.configurations.FacebookUtils;
import com.miercolesdefut.logic.dao.FriendDAO;
import com.miercolesdefut.logic.dao.Invited;
import com.miercolesdefut.logic.dao.Team;
import com.miercolesdefut.logic.dao.TeamStatusMembers;
import com.miercolesdefut.logic.model.UpdaterListData;
import com.miercolesdefut.logic.model.facebook.ServiceFacebook;
import com.miercolesdefut.logic.model.facebook.ServiceFacebookImp;
import com.miercolesdefut.logic.model.rxbux.App;
import com.miercolesdefut.view.BaseActivity;
import com.miercolesdefut.view.BaseFragment;
import com.miercolesdefut.view.buissnes.teams.RegistrarEquipo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by lauro on 16/02/2017.
 */

public class AmigosCerrarFragment extends BaseFragment {

    protected ServiceFacebook serviceFacebook = new ServiceFacebookImp();
    private AmigosCerrarFragment.OnListFragmentInteractionListenerInvitados mListener;
    protected RecyclerView recyclerView;
    private Team equipo;
    private ServiceCorona serviceCorona = new ServiceCoronaImp();

    private final String INIT_SCREEN = "team/pending";

    public AmigosCerrarFragment() {
    }

    public static AmigosCerrarFragment newInstance() {
        AmigosCerrarFragment fragment = new AmigosCerrarFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public static AmigosCerrarFragment newInstance(Team equipo) {
        Bundle args = new Bundle();
        AmigosCerrarFragment fragment = new AmigosCerrarFragment();
        args.putSerializable(RegistrarEquipo.KEY_TEAM_COMPLETE, equipo);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();

        Tracker mTracker = ((App) getActivity().getApplication()).getDefaultTracker();
        mTracker.setScreenName(INIT_SCREEN);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        if(getArguments() != null){
            this.equipo = (Team)getArguments().getSerializable(RegistrarEquipo.KEY_TEAM_COMPLETE);
        }
    }

    protected void init(){
        final ProgressDialog progressDialog = BaseActivity.createProgressDialog(getContext());
        progressDialog.show();
        serviceFacebook.callFacebookFriends(getActivity().getApplicationContext(), (friendDAOs)->{
            Invited invited = new Invited();
            invited.setFacebookId(FacebookUtils.getId(getContext()));

            for(FriendDAO friend:friendDAOs){
                invited.getFriensIds().add(friend.getId());
            }
            serviceCorona.canBeInvited(invited, (response)->{
                if(response.isSuccessful()){
                    Map<String, Boolean> responseInvited = response.body();
                    Iterator<FriendDAO> friends = friendDAOs.iterator();
                    boolean isOwnerTeam = equipo.isOnwerTeam(FacebookUtils.getId(getContext()));

                    while (friends.hasNext()){
                        FriendDAO friendIt = friends.next();
                        if(responseInvited != null){
                            if(!responseInvited.get(friendIt.getId())){
                                friends.remove();
                            }
                        }
                    }
                    /***/
                    if(!equipo.isNewTeam()) {
                        progressDialog.dismiss();
                        serviceCorona.invitacionesPendientesEquipo(equipo.getId(), (responsePending) -> {
                            if (response.isSuccessful()) {
                                TeamStatusMembers members = responsePending.body();
                                equipo.getInvitationSended().addAll(members.getPending());
                                equipo.getInvitationAccepted().addAll(members.getAccepted());
                                equipo.setEspacio(members.getRoom());

                                List<FriendDAO> accepted = new ArrayList<>();
                                List<FriendDAO> pending = new ArrayList<>();
                                List<FriendDAO> available = new ArrayList<>();

                                for(FriendDAO friend: friendDAOs){
                                    if(equipo.getInvitationAccepted().contains(friend.getId())){
                                        accepted.add(friend);
                                    }else if(equipo.getInvitationSended().contains(friend.getId())){
                                        pending.add(friend);
                                    }else{
                                        available.add(friend);
                                    }
                                }

                                Collections.sort(accepted, (o1, o2) -> o1.getName().compareTo(o2.getName()));
                                Collections.sort(pending, (o1, o2) -> o1.getName().compareTo(o2.getName()));
                                Collections.sort(available, (o1, o2) -> o1.getName().compareTo(o2.getName()));


                                List<FriendDAO> completeList = new ArrayList<>();
                                completeList.addAll(accepted);
                                completeList.addAll(pending);
                                completeList.addAll(available);

                                ((UpdaterListData<List<FriendDAO>>) recyclerView.getAdapter()).addItemsToList(completeList);
                            }
                        });
                    } else {
                        progressDialog.dismiss();
                        ((UpdaterListData<List<FriendDAO>>) recyclerView.getAdapter()).addItemsToList(friendDAOs);
                    }
                }else{
                    progressDialog.dismiss();
                }
            });

        });
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_createteamfriends_list, container, false);
        if (view instanceof RecyclerView) {
            initRecyclerView(view);
        }
        return view;
    }

    protected void initRecyclerView(View view){
        Context context = view.getContext();
        recyclerView = (RecyclerView) view;
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(new AmigosCerrarRecyclerView(new ArrayList<FriendDAO>(), mListener, equipo));
        //recyclerView.setAdapter(new MyCreateTeamFriendsRecyclerViewAdapter(new ArrayList<FriendDAO>(), mListener, equipo));
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof AmigosCerrarFragment.OnListFragmentInteractionListenerInvitados) {
            mListener = (AmigosCerrarFragment.OnListFragmentInteractionListenerInvitados) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnListFragmentInteractionListenerInvitados {
        void update();
        boolean addCerrarFragment(FriendDAO friend);
    }
}
