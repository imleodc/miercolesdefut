package com.miercolesdefut.view.buissnes.teams;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.miercolesdefut.R;
import com.miercolesdefut.logic.configurations.FacebookUtils;
import com.miercolesdefut.logic.dao.CheckedTeams;
import com.miercolesdefut.logic.dao.DetailMatchPerfil;
import com.miercolesdefut.logic.dao.Matche;
import com.miercolesdefut.logic.dao.Perfil;
import com.miercolesdefut.logic.dao.Team;
import com.miercolesdefut.logic.model.UpdaterListData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lauro on 15/02/2017.
 */

public class MisEquiposFromMatchesRecyclerViewAdapter extends RecyclerView.Adapter<MisEquiposFromMatchesRecyclerViewAdapter.ViewHolder> implements UpdaterListData<List<CheckedTeams>> {
    private List<CheckedTeams> equipos;
    private MisEquiposFragment.OnListFragmentInteractionListener mListener;
    private List<Matche> currents;
    private boolean playing = false;
    private Matche partido;
    private Perfil perfil;
    private List<Team> teamsCheckeds;


    public MisEquiposFromMatchesRecyclerViewAdapter(List<Matche> partidos,MisEquiposFragment.OnListFragmentInteractionListener listener){
        this(partidos, listener, null, null);
    }

    public MisEquiposFromMatchesRecyclerViewAdapter(List<Matche> partidos,MisEquiposFragment.OnListFragmentInteractionListener listener, Matche partido, Perfil perfil){
        if(partidos == null){
            return;
        }
        currents = new ArrayList<>();
        for(Matche partidoFor:partidos){
            if(partidoFor.isActive()){
                currents.add(partidoFor);
                playing = true;
            }
            if(partidoFor.getUser_teams() != null && !partidoFor.getUser_teams().isEmpty()) {
                equipos = partidoFor.getUser_teams();
            }
        }
        this.mListener = listener;
        this.partido = partido;
        this.perfil = perfil;
    }


    /*public MyMisEquiposRecyclerViewAdapter(List<CheckedTeams> items, MisEquiposFragment.OnListFragmentInteractionListener listener) {
        equipos = items;
        mListener = listener;
    }

    public MyMisEquiposRecyclerViewAdapter(List<CheckedTeams> items, MisEquiposFragment.OnListFragmentInteractionListener listner, List<Matche> currents){
        this.equipos = items;
        this.mListener = listner;
        this.currents = currents;
        if(!currents.isEmpty()){
            playing = true;
        }
    }
    public MyMisEquiposRecyclerViewAdapter(List<CheckedTeams> items, MisEquiposFragment.OnListFragmentInteractionListener listner, List<Matche> currents, Matche partido){
        this(items, listner, currents);
        this.partido = partido;
    }

    public MyMisEquiposRecyclerViewAdapter(List<CheckedTeams> items, MisEquiposFragment.OnListFragmentInteractionListener listner, List<Matche> currents, Matche partido, Perfil perfil){
        this(items, listner, currents, partido);
        this.perfil = perfil;
        if(perfil != null){
            teamsCheckeds = getCheckedTeams(perfil);
        }
    }*/


    @Override
    public MisEquiposFromMatchesRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_misequipos, parent, false);
        return new MisEquiposFromMatchesRecyclerViewAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MisEquiposFromMatchesRecyclerViewAdapter.ViewHolder holder, int position) {
        holder.equipo = equipos.get(position);
        holder.nombreEquipo.setText(holder.equipo.getTeam().getName());
        if(holder.equipo.getTeam().isPendiente()){
            configViewHolderPendiente(holder);
        } else {
            if(holder.equipo.getTeam().isCompletado()){
                configViewHolderCompletado(holder);
            } else {
                if(holder.equipo.hasCheckedMembers()){
                    configViewHolderJuntos(holder);
                } else {
                    configViewHolderCompleto(holder);
                }
            }
        }
        holder.mView.setOnClickListener((v)->{
            if (null != mListener) {
                if(perfil == null){
                    mListener.onListFragmentInteraction(holder.equipo.getTeam(),partido,currents);
                } else {
                    Matche partidoL = getPartidoByTeam(holder.equipo.getTeam());
                    mListener.onListFragmentInteraction(holder.equipo.getTeam(), partidoL, currents);
                }
            }
        });
    }

    private Matche getPartidoByTeam(Team team){
        if(perfil == null || perfil.getMatches() == null || perfil.getMatches().isEmpty()){
            return null;
        }

        for(DetailMatchPerfil matchPerfil: perfil.getMatches()){
            for(CheckedTeams teamf:matchPerfil.getTeams()){
                if(teamf.getTeam().getId() == team.getId()){
                    return matchPerfil.getMatch();
                }
            }
        }
        return null;
    }



    private boolean hayPartido() {
        return playing;
    }

    protected void configViewHolderCompleto(MisEquiposFromMatchesRecyclerViewAdapter.ViewHolder holder) {
        configViewHolder(holder, holder.mView.getResources().getColor(R.color.color_text_fichajes), holder.mView.getResources().getColor(R.color.color_background_title), -1, "");
    }

    protected void configViewHolderJuntos(MisEquiposFromMatchesRecyclerViewAdapter.ViewHolder holder) {
        configViewHolder(holder, holder.mView.getResources().getColor(R.color.color_text_fichajes), holder.mView.getResources().getColor(R.color.color_background_title), R.drawable.location_highlighted_mini, holder.mView.getResources().getString(R.string.ahoraJuntos));
    }

    protected  void configViewHolderCompletado(MisEquiposFromMatchesRecyclerViewAdapter.ViewHolder holder){
        configViewHolder(holder, holder.mView.getResources().getColor(R.color.pendiente),-1, R.drawable.pendiente_cerrar, holder.mView.getResources().getString(R.string.pendientePorCerrar));
    }


    protected void configViewHolderPendiente(MisEquiposFromMatchesRecyclerViewAdapter.ViewHolder holder){
        configViewHolder(holder, holder.mView.getResources().getColor(R.color.pendiente),-1, R.drawable.pendiente, holder.mView.getResources().getString(R.string.pendiente));
    }


    protected void configViewHolder(MisEquiposFromMatchesRecyclerViewAdapter.ViewHolder holder, int leftColor, int fillColor, int leftIcono, String status){
        holder.itemImageFriend.setBackgroundColor(leftColor);
        if(fillColor != -1) {
            holder.mView.setBackgroundColor(fillColor);
        }
        holder.ahoraJuntos.setText(status);
        holder.ahoraJuntos.setTextColor(leftColor);
        if(leftIcono != -1){
            holder.ahoraJuntos.setCompoundDrawablesWithIntrinsicBounds(leftIcono,0,0,0);
        }
        holder.markCapitan.setTextColor(fillColor);
        if(holder.equipo.getTeam().isOnwerTeam(FacebookUtils.getId(holder.mView.getContext()))){
            holder.markCapitan.setVisibility(View.VISIBLE);
        } else {
            holder.markCapitan.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        if(equipos == null){
            return 0;
        }
        return equipos.size();
    }

    @Override
    public void addItemsToList(List<CheckedTeams> data) {
        equipos.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public void clearList() {
        equipos.clear();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView nombreEquipo;
        public final ImageView itemImageFriend;
        public final TextView ahoraJuntos;
        public final ImageView arrow;
        public final TextView markCapitan;
        public CheckedTeams equipo;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            nombreEquipo = (TextView) view.findViewById(R.id.fichajeNombreEquipo);
            itemImageFriend = (ImageView) view.findViewById(R.id.itemImageFriend);
            ahoraJuntos = (TextView) view.findViewById(R.id.fichajeName);
            arrow = (ImageView) view.findViewById(R.id.imageView8);
            markCapitan = (TextView) view.findViewById(R.id.markCapitan);
        }
    }
}
