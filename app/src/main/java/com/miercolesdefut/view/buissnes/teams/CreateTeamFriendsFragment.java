package com.miercolesdefut.view.buissnes.teams;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.android.gms.analytics.Tracker;
import com.miercolesdefut.R;
import com.miercolesdefut.ServiceCoronaImp;
import com.miercolesdefut.logic.ServiceCorona;
import com.miercolesdefut.logic.configurations.FacebookUtils;
import com.miercolesdefut.logic.dao.FriendDAO;
import com.miercolesdefut.logic.dao.Invited;
import com.miercolesdefut.logic.dao.Team;
import com.miercolesdefut.logic.dao.TeamStatusMembers;
import com.miercolesdefut.logic.model.UpdaterListData;
import com.miercolesdefut.logic.model.facebook.ServiceFacebook;
import com.miercolesdefut.logic.model.facebook.ServiceFacebookImp;
import com.miercolesdefut.logic.model.rxbux.App;
import com.miercolesdefut.view.BaseActivity;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class CreateTeamFriendsFragment extends Fragment {
    protected ServiceFacebook serviceFacebook = new ServiceFacebookImp();
    private OnListFragmentInteractionListenerInvitados mListener;
    protected RecyclerView recyclerView;
    private Team equipo;
    private ServiceCorona serviceCorona = new ServiceCoronaImp();

    public CreateTeamFriendsFragment() {
    }

    public static CreateTeamFriendsFragment newInstance() {
        CreateTeamFriendsFragment fragment = new CreateTeamFriendsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public static CreateTeamFriendsFragment newInstance(Team equipo) {
        Bundle args = new Bundle();
        CreateTeamFriendsFragment fragment = new CreateTeamFriendsFragment();
        args.putSerializable(RegistrarEquipo.KEY_TEAM_COMPLETE, equipo);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getArguments() != null){
            this.equipo = (Team)getArguments().getSerializable(RegistrarEquipo.KEY_TEAM_COMPLETE);
        }

        if(!isConected()){
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage("Error en la conexión a internet, por favor inténtelo más tarde.");
            builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which){
                    //Do nothing here because we override this button later to change the close behaviour.
                    //However, we still need this because on older versions of Android unless we
                    //pass a handler the button doesn't get instantiated
                }
            });

            final AlertDialog dialog = builder.create();
            dialog.show();
            dialog.setCancelable(false);

            Button button = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
            button.setOnClickListener(v -> {
                if(isConected()) {
                    dialog.dismiss();
                    init();
                }else{
                    button.setText("Reintentando");
                    Handler handler = new Handler();
                    handler.postDelayed(() -> {
                        button.setText("Aceptar");
                        if(isConected()) {
                            dialog.dismiss();
                            init();
                        }
                    }, 1000);
                }
            });

            return;
        }

        init();

    }

    protected boolean isConected(){
        ConnectivityManager conMgr = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo i = conMgr.getActiveNetworkInfo();
        if (i == null)
            return false;
        if (!i.isConnected())
            return false;
        if (!i.isAvailable())
            return false;
        return true;
    }

    protected void init(){
        final ProgressDialog progressDialog = BaseActivity.createProgressDialog(getContext());
        progressDialog.show();
        serviceFacebook.callFacebookFriends(getActivity().getApplicationContext(), (friendDAOs)->{
            Invited invited = new Invited();
            invited.setFacebookId(FacebookUtils.getId(getContext()));

            for(FriendDAO friend:friendDAOs){
                invited.getFriensIds().add(friend.getId());
            }
            serviceCorona.canBeInvited(invited, (response)->{
                if(response.isSuccessful()){
                    Map<String, Boolean> responseInvited = response.body();
                    Iterator<FriendDAO> friends = friendDAOs.iterator();
                    boolean isOwnerTeam = equipo.isOnwerTeam(FacebookUtils.getId(getContext()));
                    while (friends.hasNext()){
                        FriendDAO friendIt = friends.next();
                        if(responseInvited != null){
                            if(!responseInvited.get(friendIt.getId())){
                                friends.remove();
                            }
                        }
                    }
                    /***/
                    if(!equipo.isNewTeam()) {
                        serviceCorona.invitacionesPendientesEquipo(equipo.getId(), (responsePending) -> {
                            progressDialog.dismiss();
                            if (response.isSuccessful()) {
                                TeamStatusMembers members = responsePending.body();
                                equipo.getInvitationSended().addAll(members.getPending());
                                equipo.getInvitationAccepted().addAll(members.getAccepted());
                                if(!isOwnerTeam){
                                    Iterator<FriendDAO> friendsRemove = friendDAOs.iterator();
                                    while(friendsRemove.hasNext()){
                                        FriendDAO friendIt = friendsRemove.next();
                                        if(!equipo.getInvitationAccepted().contains(friendIt.getId())){
                                            friendsRemove.remove();
                                        }
                                    }
                                }
                            }
                            ((UpdaterListData<List<FriendDAO>>) recyclerView.getAdapter()).addItemsToList(friendDAOs);
                        });
                    } else {
                        progressDialog.dismiss();
                        ((UpdaterListData<List<FriendDAO>>) recyclerView.getAdapter()).addItemsToList(friendDAOs);
                    }
                }else{
                    progressDialog.dismiss();
                }
            });

        });
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_createteamfriends_list, container, false);
        if (view instanceof RecyclerView) {
            initRecyclerView(view);
        }
        return view;
    }

    protected void initRecyclerView(View view){
        Context context = view.getContext();
        recyclerView = (RecyclerView) view;
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        if(!equipo.isOnwerTeam(FacebookUtils.getId(getContext())) && !equipo.isNewTeam() && !equipo.is_valid()){
            recyclerView.setAdapter(new RecyclerViewEquipoPendiente(new ArrayList<>()));
            return;
        }
        if(equipo == null){
            MyCreateTeamFriendsRecyclerViewAdapter myCreateTeamFriendsRecyclerViewAdapter = new MyCreateTeamFriendsRecyclerViewAdapter(new ArrayList<>(), mListener);
            myCreateTeamFriendsRecyclerViewAdapter.setmTracker(((App)getActivity().getApplication()).getDefaultTracker());
            recyclerView.setAdapter(myCreateTeamFriendsRecyclerViewAdapter);

        } else {
            recyclerView.setAdapter(new MyCreateTeamFriendsRecyclerViewAdapter(new ArrayList<>(), mListener, equipo));
        }
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListenerInvitados) {
            mListener = (OnListFragmentInteractionListenerInvitados) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnListFragmentInteractionListenerInvitados {
        boolean add(FriendDAO friend);
        boolean remove(FriendDAO friend);
        boolean containt(FriendDAO friend);
    }
}
