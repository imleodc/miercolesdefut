package com.miercolesdefut.view.buissnes.historial;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.miercolesdefut.R;
import com.miercolesdefut.logic.dao.HistoriUser;
import com.miercolesdefut.logic.dao.Matche;
import com.miercolesdefut.logic.model.UpdaterListData;
import com.miercolesdefut.view.buissnes.historial.HistorialFragmentList.OnListFragmentInteractionListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class HistorialRecyclerViewAdapter extends RecyclerView.Adapter<HistorialRecyclerViewAdapter.ViewHolder> implements UpdaterListData<HistoriUser> {

    private final List<Matche> mValues;
    HistoriUser historiUser;
    private final OnListFragmentInteractionListener mListener;

    public HistorialRecyclerViewAdapter(HistoriUser userHistori, OnListFragmentInteractionListener listener) {
        mValues = new ArrayList<>();
        historiUser = userHistori;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_historial_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.partido = mValues.get(position);
        holder.dayMatch.setText(holder.partido.getStart_date_format());
        holder.playerAName.setText(holder.partido.getTeam_a().getName());
        holder.playerBName.setText(holder.partido.getTeam_b().getName());
        Picasso.with(holder.mView.getContext()).load(holder.partido.getTeam_a().getImage()).into(holder.playerAImage);
        Picasso.with(holder.mView.getContext()).load(holder.partido.getTeam_b().getImage()).into(holder.playerBImage);
        if(holder.partido.getPoints() != null) {
            StringBuilder strBuilder = new StringBuilder().append(holder.partido.getPoints().getTotal()).append(" ").append(holder.mView.getResources().getString(R.string.pts));
            holder.puntos.setText(strBuilder.toString());
            if(holder.partido.getPoints().getShare() != null && holder.partido.getPoints().getShare().length()>0){
                holder.puntosShare.setText(holder.mView.getResources().getString(R.string.format_pts, holder.partido.getPoints().getShare()));
            }
            if(holder.partido.getPoints().getFull_team() != null && holder.partido.getPoints().getFull_team().length()>0 && !holder.partido.getPoints().getFull_team().equals("0")){
                holder.puntosEquipoCompleto.setText("+"+holder.partido.getPoints().getFull_team());
                holder.puntosEquipoCompleto.setTextColor(holder.mView.getContext().getResources().getColor(R.color.completado));
                holder.puntosEquipoCompleto.setVisibility(View.VISIBLE);
                holder.iconoEquipo.setColorFilter(holder.mView.getContext().getResources().getColor(R.color.completado));
            } else {
                holder.puntosEquipoCompleto.setVisibility(View.GONE);
                holder.iconoEquipo.setColorFilter(Color.WHITE);
            }
            if(holder.partido.getPoints().getLocation() != null && holder.partido.getPoints().getLocation().points != 0){
                holder.layoutLocation.setVisibility(View.VISIBLE);
                holder.nameLocationH.setText(holder.partido.getPoints().getLocation().name);
                holder.scoreLocationH.setText(holder.mView.getResources().getString(R.string.format_pts, String.valueOf(holder.partido.getPoints().getLocation().points) ));
                holder.scoreLayoutUbication.setVisibility(View.VISIBLE);
            } else {
                holder.nameLocationH.setText(R.string.sinUbicacion);
                holder.scoreLocationH.setText(holder.mView.getResources().getString(R.string.format_pts, String.valueOf(0) ));
            }
        }
        holder.imageCamera.setColorFilter(Color.WHITE);
        holder.members.setText(new StringBuilder().append(holder.partido.getMembers().getChecked()).append(" de ").append(holder.partido.getMembers().getTotal()));
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    @Override
    public void addItemsToList(HistoriUser data) {
        historiUser = data;
        if(historiUser.getMatches() == null || historiUser.getMatches().isEmpty()){
            return;
        }
        mValues.addAll(historiUser.getMatches());
        notifyItemRangeInserted(0, mValues.size());
    }

    @Override
    public void clearList() {
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView dayMatch;
        public final TextView playerAName;
        public final TextView playerBName;
        public final ImageView playerAImage;
        public final ImageView playerBImage;
        public final TextView puntos;
        public final TextView members;
        public final TextView puntosShare;
        public final ImageView imageCamera;
        public final TextView puntosEquipoCompleto;
        public final ImageView iconoEquipo;
        public final View layoutLocation;
        public final TextView nameLocationH;
        public final TextView scoreLocationH;
        public final View scoreLayoutUbication;
        public Matche partido;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            dayMatch = (TextView) view.findViewById(R.id.dayMatch);
            playerAName = (TextView) view.findViewById(R.id.hPlayerAName);
            playerBName = (TextView) view.findViewById(R.id.hPlayerBName);
            playerAImage = (ImageView) view.findViewById(R.id.hPlayerAImage);
            playerBImage = (ImageView) view.findViewById(R.id.hPlayerBImage);
            puntos = (TextView) view.findViewById(R.id.score);
            members = (TextView)view.findViewById(R.id.hMembers);
            puntosShare = (TextView)view.findViewById(R.id.puntosFotoH);
            imageCamera = (ImageView) view.findViewById(R.id.imageCameraH);
            puntosEquipoCompleto = (TextView) view.findViewById(R.id.equipoCompletoH);
            iconoEquipo = (ImageView) view.findViewById(R.id.iconoEquipoH);
            layoutLocation = view.findViewById(R.id.locationLayoutH);
            nameLocationH = (TextView) view.findViewById(R.id.nameLocation);
            scoreLocationH = (TextView) view.findViewById(R.id.scoreLocation);
            scoreLayoutUbication = view.findViewById(R.id.scoreLayoutUbication);
        }
    }
}
