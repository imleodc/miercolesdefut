package com.miercolesdefut.view.buissnes.matchs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.miercolesdefut.R;
import com.miercolesdefut.logic.dao.Team;
import com.miercolesdefut.logic.model.rxbux.App;

public class ActivesMatchesIntermediateFragment extends Fragment {

    private final String INIT_SCREEN = "selectMatch";

    public static ActivesMatchesIntermediateFragment newInstance() {
        ActivesMatchesIntermediateFragment fragment = new ActivesMatchesIntermediateFragment();
        return fragment;
    }
    public static ActivesMatchesIntermediateFragment newInstance(Team team ) {
        ActivesMatchesIntermediateFragment fragment = new ActivesMatchesIntermediateFragment();
        Bundle args = new Bundle();
        args.putSerializable(PartidosActivosFragment.KEY_TEAM, team);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Tracker mTracker = ((App) getActivity().getApplication()).getDefaultTracker();
        mTracker.setScreenName(INIT_SCREEN);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_actives_matches_intermediate, container, false);
        Team team = null;
        if(getArguments() != null ){
            team = (Team) getArguments().getSerializable(PartidosActivosFragment.KEY_TEAM);
        }
        getFragmentManager().beginTransaction().replace(R.id.layoutActivesMatchs, PartidosActivosFragment.newInstance(team)).commit();
        return view;
    }
}
