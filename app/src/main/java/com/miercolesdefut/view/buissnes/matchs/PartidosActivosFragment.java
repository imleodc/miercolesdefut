package com.miercolesdefut.view.buissnes.matchs;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.android.gms.analytics.Tracker;
import com.miercolesdefut.R;
import com.miercolesdefut.ServiceCoronaImp;
import com.miercolesdefut.logic.ServiceCorona;
import com.miercolesdefut.logic.configurations.FacebookUtils;
import com.miercolesdefut.logic.dao.CheckedTeams;
import com.miercolesdefut.logic.dao.Matche;
import com.miercolesdefut.logic.dao.Team;
import com.miercolesdefut.logic.model.BlockListData;
import com.miercolesdefut.logic.model.UpdaterListData;
import com.miercolesdefut.logic.model.rxbux.App;
import com.miercolesdefut.view.BaseFragment;
import com.trello.rxlifecycle.android.FragmentEvent;

import java.util.ArrayList;
import java.util.List;

public class PartidosActivosFragment extends BaseFragment {
    private ServiceCorona serviceCorona = new ServiceCoronaImp();
    private OnListFragmentInteractionListener mListener;
    RecyclerView recyclerView;
    public static final String KEY_TEAM = "keyTeam";
    public static final String KEY_AUTO_CHECKING = "keytofragmentyallegue";
    public static final String KEY_MAIN_SCRREN = "itsfrommainScreen";
    private static final String TAG = "PartidosActivosFragment";
    public boolean autoChecking = false;
    public boolean mainScreen = false;
    private Button crearEquipo;

    private PartidosActivosRecyclerViewAdapter viewAdapterAux;

    private CheckedTeams teamChecked;
    private Matche matche;

    private boolean checkedMatch = false;

    public PartidosActivosFragment() {
    }

    public static PartidosActivosFragment newInstance() {
        PartidosActivosFragment fragment = new PartidosActivosFragment();
        return fragment;
    }

    public static PartidosActivosFragment newInstance(boolean mainScreen) {
        PartidosActivosFragment fragment = new PartidosActivosFragment();
        Bundle args = new Bundle();
        args.putBoolean(KEY_MAIN_SCRREN, mainScreen);
        fragment.setArguments(args);
        return fragment;
    }

    public static PartidosActivosFragment newInstance(Team team) {
        PartidosActivosFragment fragment = new PartidosActivosFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(KEY_TEAM, team);
        fragment.setArguments(bundle);
        return fragment;
    }

    public static PartidosActivosFragment newInstance(Team team, boolean autoChecking) {
        PartidosActivosFragment fragment = new PartidosActivosFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(KEY_TEAM, team);
        bundle.putBoolean(KEY_AUTO_CHECKING, autoChecking);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(this.getArguments() != null){
            mainScreen = getArguments().getBoolean(KEY_MAIN_SCRREN, false);
        }
    }

    public void setCheckedMatch(boolean checkedMatch) {
        this.checkedMatch = checkedMatch;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_partidosactivos_list, container, false);
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            recyclerView = (RecyclerView) view;
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            Team equipo = null;
            if(getArguments() != null){
                equipo = (Team) getArguments().getSerializable(KEY_TEAM);
                autoChecking = getArguments().getBoolean(KEY_AUTO_CHECKING,false);
            }
            if(equipo != null){
                viewAdapterAux = new PartidosActivosRecyclerViewAdapter(new ArrayList<>(), mListener,equipo, autoChecking);
            } else {
                if(mainScreen){
                    // checkin
                    viewAdapterAux = new PartidosActivosRecyclerViewAdapter(new ArrayList<>(), mListener, true);
                    viewAdapterAux.setmTracker(((App)getActivity().getApplication()).getDefaultTracker());
                } else {
                    viewAdapterAux = new PartidosActivosRecyclerViewAdapter(new ArrayList<>(), mListener);
                }
            }

            viewAdapterAux.setFragmentManager(getFragmentManager());
            recyclerView.setAdapter(viewAdapterAux);
        }
        crearEquipo = (Button) getActivity().findViewById(R.id.crearEquipoBtn);
        //ProgressDialog progressDialog = BaseActivity.createProgressDialog(getContext());
        //progressDialog.show();
        return view;
    }

    private void hideCrearEquipo(){
        if(crearEquipo == null) {
            return;
        }
        crearEquipo.setVisibility(View.GONE);
    }


    private boolean hasValidTeams(List<Team> equipos) {
        if(equipos == null || equipos.isEmpty()){
            return false;
        }

        for(Team team: equipos){
            if(team.isValidForMatches()){
                return true;
            }
        }
        return false;
    }

    private boolean hasChecking(List<Matche> partidos) {
        for(Matche match: partidos){
            List<CheckedTeams> equipos = match.getUser_teams();
            if( equipos != null) {
                for(CheckedTeams check:equipos){
                    if(check.isUser_has_checked()){
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private CheckedTeams getCheckedTeam(List<Matche> partidos) {
        for(Matche match: partidos){
            List<CheckedTeams> equipos = match.getUser_teams();
            if( equipos != null) {
                for(CheckedTeams check:equipos){
                    if(check.isUser_has_checked()){
                        return check;
                    }
                }
            }
        }
        return null;
    }

    private Matche getCheckedMatche(List<Matche> partidos){
        for(Matche match: partidos){
            List<CheckedTeams> equipos = match.getUser_teams();
            if(equipos != null){
                for(CheckedTeams check: equipos){
                    if(check.isUser_has_checked()){
                        return match;
                    }
                }
            }
        }
        return null;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        App.get().busMatch().toObservable()
                .compose(this.bindUntilEvent(FragmentEvent.PAUSE))//bindUntilEvent(lifecycle(),FragmentEvent.DESTROY_VIEW))//bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .map((partidosR)->{
                    List<Matche> partidosActivos = new ArrayList<>();
                    List<Matche> partidos = (List<Matche>) partidosR;
                    if(partidos == null){
                        return partidos;
                    }
                    for(Matche partido :partidos){
                        if(partido.is_active() == 1){
                            partidosActivos.add(partido);
                        }
                    }

                    if(partidosActivos == null || partidosActivos.isEmpty()){
                        View ahoraJuegaText = getActivity().findViewById(R.id.textView18);
                        if(ahoraJuegaText != null) {
                            ahoraJuegaText.setVisibility(View.GONE);
                        }
                    } else {
                        View ahoraJuegaText = getActivity().findViewById(R.id.textView18);
                        if(ahoraJuegaText != null) {
                            ahoraJuegaText.setVisibility(View.VISIBLE);
                        }
                    }
                    return partidosActivos;
                }).subscribe((partidosActivos)->{
            serviceCorona.getEquipos(FacebookUtils.getId(getContext()), (response)->{
                if(response.isSuccessful()){
                    List<Team> equipos = response.body();
                    if((equipos == null || equipos.isEmpty()) || hasChecking(partidosActivos) || !hasValidTeams(equipos)){
                        ((BlockListData)recyclerView.getAdapter()).setBlockListData(true);
                        if(equipos == null || equipos.isEmpty()|| !hasValidTeams(equipos)){
                            View sinEquipos = getActivity().findViewById(R.id.alertaSinEquipo);
                            if(sinEquipos!= null) {
                                sinEquipos.setVisibility(View.VISIBLE);
                            }
                        } else {
                            if(getActivity() == null){
                                return;
                            }
                            View sinEquipos = getActivity().findViewById(R.id.alertaSinEquipo);
                            if(sinEquipos != null){
                                sinEquipos.setVisibility(View.GONE);
                            }
                        }
                    } else {
                        ((BlockListData)recyclerView.getAdapter()).setBlockListData(false);
                        View sinEquipos = getActivity().findViewById(R.id.alertaSinEquipo);
                        if(sinEquipos != null){
                            sinEquipos.setVisibility(View.GONE);
                        }
                    }
                    if(!hasValidTeams(equipos)){
                        //((PartidosActivosRecyclerViewAdapter)recyclerView.getAdapter()).setShowChecking(false);
                        //getActivity().findViewById(R.id.)
                        //viewAdapterAux.
                    } else {
                        ((PartidosActivosRecyclerViewAdapter)recyclerView.getAdapter()).setShowChecking(true);
                    }

                    if(hasChecking(partidosActivos)){
                        viewAdapterAux.setChecked(true);
                        // si ya hizo un checkin
                        teamChecked = getCheckedTeam(partidosActivos);
                        // equipo en el que hizo checkin
                        matche = getCheckedMatche(partidosActivos);
                        // partido del checkin
                        if(teamChecked != null){
                            viewAdapterAux.setTeamName(teamChecked.getTeam().getName());
                        }
                        if( matche != null){
                            viewAdapterAux.setCheckedMatch(matche);
                            viewAdapterAux.setCheckedTeam(teamChecked);
                        }
                    }
                    (((UpdaterListData)recyclerView.getAdapter())).clearList();
                    ((UpdaterListData<List<Matche>>)recyclerView.getAdapter()).addItemsToList(partidosActivos);
                }
            });
        });
        App.get().updateWithCache();
    }

    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(Matche partido, Team team);
        void toDetalleEquipoActivo(Matche partido, Team team);
        void onListFragmentMainScreen(Matche partido, Team team);
    }
}
