package com.miercolesdefut.view.buissnes.teams;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.miercolesdefut.R;
import com.miercolesdefut.ServiceCoronaImp;
import com.miercolesdefut.logic.ServiceCorona;
import com.miercolesdefut.logic.configurations.FacebookUtils;
import com.miercolesdefut.logic.dao.FriendDAO;
import com.miercolesdefut.logic.dao.InvitarAEquipo;
import com.miercolesdefut.logic.dao.Team;
import com.miercolesdefut.logic.dao.android.FriendDAOFB;
import com.miercolesdefut.logic.model.UpdaterListData;
import com.miercolesdefut.view.buissnes.teams.CreateTeamFriendsFragment.OnListFragmentInteractionListenerInvitados;
import com.miercolesdefut.view.buissnes.teams.detalle.amigos.AmigosCerrarRecyclerView;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MyCreateTeamFriendsRecyclerViewAdapter extends RecyclerView.Adapter<MyCreateTeamFriendsRecyclerViewAdapter.ViewHolder> implements UpdaterListData<List<FriendDAO>>,View.OnClickListener{

    private final List<FriendDAO> mValues;
    private final OnListFragmentInteractionListenerInvitados listenerInvitados;
    private Team equipo;
    ServiceCorona serviceCorona = new ServiceCoronaImp();

    private Tracker mTracker;

    public void setmTracker(Tracker mTracker) {
        this.mTracker = mTracker;
    }

    public MyCreateTeamFriendsRecyclerViewAdapter(List<FriendDAO> items, OnListFragmentInteractionListenerInvitados listener) {
        this(items, listener, null);
    }

    public MyCreateTeamFriendsRecyclerViewAdapter(List<FriendDAO> items, OnListFragmentInteractionListenerInvitados listener, Team team){
        this.mValues = items;
        this.listenerInvitados = listener;
        if(team != null){
            this.equipo = team;
        } else {
            this.equipo = new Team();
        }
    }

    @Override
    public void addItemsToList(List<FriendDAO> friends) {
        if(friends == null) {
            return;
        }
        mValues.addAll(friends);
        notifyItemRangeInserted(0,friends.size());
    }

    @Override
    public void clearList() {
        mValues.clear();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_createteamfriends, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.friend = mValues.get(position);
        holder.cancelInvitationBtn.setVisibility(View.GONE);

        synchronized (holder.friend){
            if(holder.friend.getPicture() == null) {
                ((FriendDAOFB)holder.friend).setHolder(holder);
            }
        }
        holder.nombreFriend.setText(mValues.get(position).getName());
        holder.updatePicture();
        if(equipo != null && !equipo.isOnwerTeam(FacebookUtils.getId(holder.mView.getContext()))){
            holder.invitationBtn.setVisibility(View.GONE);
            if(equipo.getInvitationAccepted().contains(holder.friend.getId())){
                setRowNormal(holder);
            } else {
                if(equipo.getInvitationSended().contains(holder.friend.getId())){
                    setRowTransparent(holder);
                } else {
                    setRowTransparent(holder);
                }
            }
            return;
        }
        holder.refresh();



        holder.invitationBtn.setOnClickListener((event)->{
            if(equipo.getInvitationSended().contains(holder.friend.getId())){
               return;
            }

            if( mTracker != null ){
                mTracker.send(new HitBuilders.EventBuilder().setCategory("button").setAction("touch").setLabel("create-add").setValue(Long.parseLong(holder.friend.getId())).build());
            }

            if(listenerInvitados.add(holder.friend)){
                if(!equipo.isNewTeam()){
                    InvitarAEquipo invitarAEquipo = new InvitarAEquipo();
                    invitarAEquipo.setFacebook_id(FacebookUtils.getId(holder.mView.getContext()));
                    invitarAEquipo.setTeam_id(equipo.getId());
                    invitarAEquipo.setInvited(holder.friend.getId());
                    serviceCorona.invitarAEquipo(invitarAEquipo, (responseInv)->{
                        if(responseInv.isSuccessful()){
                            equipo.getInvitationSended().add(holder.friend.getId());
                            notifyItemChanged(mValues.indexOf(holder.friend));
                        }
                    });
                }
                notifyItemChanged(mValues.indexOf(holder.friend));
            } else{
                if(listenerInvitados.containt(holder.friend)){
                    if(listenerInvitados.remove(holder.friend)){
                        notifyItemChanged(mValues.indexOf(holder.friend));
                    }
                }
            }
        });
        /*
        holder.cancelInvitationBtn.setOnClickListener((event)->{
            int index = mValues.indexOf(holder.friend);
            if(mValues.remove(holder.friend)){
                notifyItemRemoved(index);
            }
        });
        */
    }

    public void setRowTransparent(ViewHolder holder){
        holder.imageFriend.setAlpha(AmigosCerrarRecyclerView.TRANSPARENCI_ROW);
        holder.mView.getBackground().setAlpha(AmigosCerrarRecyclerView.TRANSPARENCI_ROW);
        holder.imageFriend.setBackgroundColor(Color.TRANSPARENT);
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    @Override
    public void onClick(View v) {
        v.setBackgroundColor(v.getResources().getColor(R.color.item_lista));
        //Log.i("buttonValue", String.valueOf(v.getId()));
    }

    public void setRowNormal(ViewHolder holder) {
        holder.imageFriend.setAlpha(255);
        holder.mView.setBackgroundColor(Color.WHITE);
        //holder.imageFriend.setBackgroundColor(Color.TRANSPARENT);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView nombreFriend;
        public final ImageView imageFriend;
        public final ImageButton cancelInvitationBtn;
        public final Button invitationBtn;
        public FriendDAO friend;


        public ViewHolder(View view) {
            super(view);
            mView = view;
            nombreFriend = (TextView) view.findViewById(R.id.fichajeNombreEquipo);
            imageFriend = (ImageView) view.findViewById(R.id.itemImageFriend);
            cancelInvitationBtn = (ImageButton) view.findViewById(R.id.cancelBtn);
            invitationBtn = (Button) view.findViewById(R.id.invitarBtn);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + nombreFriend.getText() + "'";
        }

        public void updatePicture(){
            Picasso.with(mView.getContext()).load(friend.getPicture()).into(imageFriend);
        }

        public void refresh(){
            if(listenerInvitados.containt(friend) || equipo.getInvitationSended().contains(friend.getId())){
                invitationBtn.setText(mView.getResources().getString(R.string.invitado));
                invitationBtn.setBackgroundColor(mView.getResources().getColor(R.color.item_lista));
                mView.setBackgroundColor(Color.WHITE);
            } else {
                if(equipo.getInvitationSended().contains(friend.getId())){
                    //cancelInvitationBtn.setVisibility(View.GONE);
                    invitationBtn.setVisibility(View.GONE);
                } else {
                    if(equipo.getInvitationAccepted().contains(friend.getId())){
                        //cancelInvitationBtn.setVisibility(View.GONE);
                        invitationBtn.setVisibility(View.GONE);
                    } else {
                        invitationBtn.setText(mView.getResources().getString(R.string.invitar));
                        invitationBtn.setBackgroundColor(mView.getResources().getColor(R.color.color_btn));
                        //cancelInvitationBtn.setVisibility(View.VISIBLE);
                        mView.setBackgroundColor(Color.WHITE);
                    }
                }
            }
        }
    }
}
