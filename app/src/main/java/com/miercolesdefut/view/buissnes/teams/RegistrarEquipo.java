package com.miercolesdefut.view.buissnes.teams;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.ImageSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.applinks.AppLinkData;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.miercolesdefut.R;
import com.miercolesdefut.ServiceCoronaImp;
import com.miercolesdefut.Utils;
import com.miercolesdefut.logic.ServiceCorona;
import com.miercolesdefut.logic.configurations.FacebookUtils;
import com.miercolesdefut.logic.dao.AcceptTeam;
import com.miercolesdefut.logic.dao.FriendDAO;
import com.miercolesdefut.logic.dao.StandarError;
import com.miercolesdefut.logic.dao.Team;
import com.miercolesdefut.logic.model.rxbux.App;
import com.miercolesdefut.view.BaseActivity;
import com.miercolesdefut.view.BaseFragment;
import com.facebook.FacebookSdk;
import com.facebook.share.model.AppInviteContent;
import com.facebook.share.widget.AppInviteDialog;
import com.jakewharton.rxbinding.view.RxView;
import com.miercolesdefut.view.buissnes.teams.detalle.CerrarEquipoFragment;
import com.raizlabs.universalfontcomponents.widget.UniversalFontEditText;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import bolts.AppLinks;
import rx.Subscription;

public class RegistrarEquipo extends BaseFragment implements CreateTeamFriendsFragment.OnListFragmentInteractionListenerInvitados {
    public static final String KEY_TEAM_COMPLETE = "keyTeamCompleteData";
    public static final String KEY_TEAM_MEMBERS_STATUS = "keyTeamStatusMembersData";
    private OnFragmentInteractionListener mListener;
    private ServiceCorona serviceCorona;
    private EditText nombreEquipo;
    private TextInputLayout nombreEquipoLayout;
    List<String> idInvitados = new ArrayList<>();
    Subscription subscription;
    Subscription subscriptionInvitar;
    private boolean blockEvent = true;

    private static final String INIT_SCREEN = "create";

    private Team completar = null;
    public RegistrarEquipo() {
        serviceCorona = new ServiceCoronaImp();
        setImageBackground(R.drawable.registro);
    }

    public static RegistrarEquipo newInstance() {
        RegistrarEquipo fragment = new RegistrarEquipo();
        return fragment;
    }

    public static RegistrarEquipo newInstance(Team team){
        RegistrarEquipo fragment = new RegistrarEquipo();
        Bundle args = new Bundle();
        args.putSerializable(KEY_TEAM_COMPLETE, team);
        fragment.setArguments(args);
        return fragment;
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Tracker mTracker = ((App) getActivity().getApplication()).getDefaultTracker();
        mTracker.setScreenName(INIT_SCREEN);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        showActionBar(getResources().getString(R.string.btn_make_team));
        if(getArguments() != null){
            completar = (Team) getArguments().getSerializable(KEY_TEAM_COMPLETE);
        } else {
            completar = new Team();
            completar.setNewTeam(true);
        }

        FacebookSdk.sdkInitialize(getContext());
        Uri targetUrl =
                AppLinks.getTargetUrlFromInboundIntent(getContext(), getActivity().getIntent());
        AppLinkData.fetchDeferredAppLinkData(
                getActivity(),
                new AppLinkData.CompletionHandler() {
                    @Override
                    public void onDeferredAppLinkDataFetched(AppLinkData appLinkData) {
                    }
                });
    }


    public void applyOpacity(View view, float opacity){
        LinearLayout layoutTeamName = (LinearLayout) view.findViewById(R.id.layoutTeamName);
        layoutTeamName.setAlpha(opacity);

        FrameLayout friendsCreateTeam = (FrameLayout) view.findViewById(R.id.friendsCreateTeam);
        friendsCreateTeam.setAlpha(opacity);

        LinearLayout layout_invite = (LinearLayout) view.findViewById(R.id.layout_invite);
        layout_invite.setAlpha(opacity);

        Button crearEquipo = (Button) view.findViewById(R.id.crearEquipoBtn);
        crearEquipo.setAlpha(opacity);

        boolean aux = opacity==1;
        crearEquipo.setEnabled(aux);
        enableDisableView(friendsCreateTeam, aux);


    }

    public static void enableDisableView(View view, boolean enabled) {
        view.setEnabled(enabled);
        if ( view instanceof ViewGroup ) {
            ViewGroup group = (ViewGroup)view;

            for ( int idx = 0 ; idx < group.getChildCount() ; idx++ ) {
                enableDisableView(group.getChildAt(idx), enabled);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = selectInflateLayout(inflater, container);

        getFragmentManager().beginTransaction().add(R.id.friendsCreateTeam, FragmentIntermedio.newInstance(completar)).commit();
        if(!completar.isNewTeam() && !completar.isOnwerTeam(FacebookUtils.getId(getContext()))){
            updateTeamNameNoOwner(view);
            return view;
        }

        initializeTextView(view);
        configInvitarBtn(view);
        if(completar.isNewTeam()){
            configButtonCreate(view);

            UniversalFontEditText editText = (UniversalFontEditText) view.findViewById(R.id.nuevoEquipoName);
            editText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if( s.length() > 2 ){
                        applyOpacity(view, 1);
                        blockEvent = false;
                    }else{
                        applyOpacity(view, 0.3F);
                        blockEvent = true;
                    }

                }
            });

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    applyOpacity(view, 0.3F);
                }
            }, 1000);

            applyOpacity(view, 0.3F);
        } else {
            nombreEquipo.setText(completar.getName());
            nombreEquipo.setEnabled(false);
            configButtonCerrar(view);
        }

        return view;
    }

    private void initializeTextView(View view){
        nombreEquipo = (EditText) view.findViewById(R.id.nuevoEquipoName);
        nombreEquipoLayout = (TextInputLayout)view.findViewById(R.id.nombreEquipoLayout);
    }


    private void updateTeamNameNoOwner(View view){
        TextView nombreEquipoText = (TextView)view.findViewById(R.id.nuevoEquipoName);
        nombreEquipoText.setText(completar.getName());
        TextView message = (TextView)view.findViewById(R.id.messageToMember);
        message.setText(getResources().getString(R.string.messageMember, completar.getOwner().getFull_name()));
    }

    private View selectInflateLayout(LayoutInflater inflate, ViewGroup container){
        if(completar.isOnwerTeam(FacebookUtils.getId(getContext()))){
            return inflate.inflate(R.layout.fragment_registrar_equipo, container, false);
        } else {
            return inflate.inflate(R.layout.fragment_muestra_equipo, container, false);
        }
    }

    private void configButtonCreate(View view){
        Button crearEquipo = (Button) view.findViewById(R.id.crearEquipoBtn);
        crearEquipo.setVisibility(View.VISIBLE);

        subscription = RxView.clicks(crearEquipo).filter((vacio)->{
            return valida();
        }).subscribe((vacio)->{

            if(!isConected()){
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage("Error en la conexión a internet, por favor inténtelo más tarde.");
                builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which){
                        //Do nothing here because we override this button later to change the close behaviour.
                        //However, we still need this because on older versions of Android unless we
                        //pass a handler the button doesn't get instantiated
                    }
                });

                final AlertDialog dialog = builder.create();
                dialog.show();

                return;
            }

            ((App)getActivity().getApplication()).getDefaultTracker().send(new HitBuilders.EventBuilder().setCategory("button").setAction("touch").setLabel("create-add").build());
            completar.setFacebook_id(FacebookUtils.getId(getContext()));
            completar.setName(nombreEquipo.getText().toString());
            final ProgressDialog progressDialog = BaseActivity.createProgressDialog(getContext());
            progressDialog.show();
            serviceCorona.registraEquipo(completar, (response)->{
                progressDialog.dismiss();
                if(response.isSuccessful()){
                    getFragmentManager().beginTransaction().addToBackStack(null).replace(R.id.content_frame, MisEquiposIntermediFragment.newInstance()).commit();
                } else {
                    StandarError error = Utils.converterError.call(response.errorBody(),((ServiceCoronaImp)serviceCorona).retrofit);
                    Toast.makeText(getContext(), error.toString(),Toast.LENGTH_LONG).show();
                }
            });
        });
    }

    private void configButtonCerrar(View view){
        Button cerrarEquipo = (Button) view.findViewById(R.id.cerrarEquipoBtn);
        cerrarEquipo.setVisibility(View.VISIBLE);
        if(!completar.isCompletado()){
            cerrarEquipo.setEnabled(false);
            cerrarEquipo.setBackgroundColor(Color.LTGRAY);
            TextView textView = (TextView) view.findViewById(R.id.textView16);
            textView.setText(R.string.messageCerrarEquipo);
        }
        subscription = RxView.clicks(cerrarEquipo)
                .filter((vacio)->completar.isCompletado())
                .subscribe((vacio)->{

                    if(!isConected()){
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setMessage("Error en la conexión a internet, por favor inténtelo más tarde.");
                        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which){
                                //Do nothing here because we override this button later to change the close behaviour.
                                //However, we still need this because on older versions of Android unless we
                                //pass a handler the button doesn't get instantiated
                            }
                        });

                        final AlertDialog dialog = builder.create();
                        dialog.show();

                        return;
                    }

                    final ProgressDialog progressDialog = BaseActivity.createProgressDialog(getContext());
                    progressDialog.show();
                    AcceptTeam equipo = new AcceptTeam();
                    equipo.setFacebook_id(FacebookUtils.getId(getContext()));
                    equipo.setTeam_id(completar.getId());
                    serviceCorona.cerrarEquipo(equipo, (response)->{
                        progressDialog.dismiss();
                        if(response.isSuccessful()){
                            getFragmentManager().beginTransaction().addToBackStack(null).replace(R.id.content_frame, MisEquiposIntermediFragment.newInstance()).commit();
                            Toast.makeText(getContext(), "Se ha cerrado correctamente el equipo", Toast.LENGTH_LONG).show();
                        } else {
                            StandarError error = Utils.converterError.call(response.errorBody(), ((ServiceCoronaImp)serviceCorona).retrofit);
                            Toast.makeText(getContext(), error.toString(), Toast.LENGTH_LONG).show();
                        }
                    });
                });
    }

    //private void loadFrament

    private Boolean valida() {
        boolean valido = true;
        if(nombreEquipo.getText().length() == 0){
            nombreEquipoLayout.setErrorEnabled(true);
            nombreEquipoLayout.setError(getResources().getString(R.string.error_nombre_equipo));
            valido = false;
        } else {
            nombreEquipoLayout.setErrorEnabled(false);
        }
        return valido;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        if(subscription != null){
            subscription.unsubscribe();
        }
        if(subscriptionInvitar != null){
            subscriptionInvitar.unsubscribe();
        }
    }

    @Override
    public boolean add(FriendDAO friend) {
        if(blockEvent){
            return false;
        }
        boolean complete = completar.add(friend);
        if(complete){
            updateSizeView();
        } else {
            updateSizeViewError();
        }
        return complete;
    }

    private void updateSizeView() {
        TextView textView = (TextView) getActivity().findViewById(R.id.sizeRecyclerView);
        if(textView != null && completar.getIds() != null){
            textView.setText(completar.getIds().size()+"/6");
            textView.setTextColor(getResources().getColor(R.color.item_lista));
        }
    }

    private void updateSizeViewError(){
        TextView textView = (TextView) getActivity().findViewById(R.id.sizeRecyclerView);
        if(textView != null && completar.getIds() != null){
            textView.setText(completar.getIds().size()+"/6");
        }
        textView.setTextColor(getContext().getResources().getColor(R.color.pendiente));
    }

    @Override
    public boolean remove(FriendDAO friend) {
        boolean complete = completar.remove(friend);
        updateSizeView();
        return complete;
    }

    @Override
    public boolean containt(FriendDAO friend) {
        return completar.containt(friend);
    }


    protected void configInvitarBtn(View view){
        Button invitar = (Button) view.findViewById(R.id.invitar);
        Spannable buttonLabel = new SpannableString("i   "+getResources().getString(R.string.fb_invite));
        buttonLabel.setSpan(new ImageSpan(getContext(), R.drawable.fb, ImageSpan.ALIGN_BASELINE), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        invitar.setText(buttonLabel);
        String isOwnerAux = FacebookUtils.getId(getContext());
        if(completar != null && !completar.isOnwerTeam(isOwnerAux)){
            invitar.setVisibility(View.GONE);
            return;
        }
        subscriptionInvitar = RxView.clicks(invitar).filter((event)->!blockEvent).subscribe((vacio)->{
            ((App)getActivity().getApplication()).getDefaultTracker().send(new HitBuilders.EventBuilder().setCategory("button").setAction("touch").setLabel("create-invite").setValue(Long.parseLong(isOwnerAux)).build());
            FacebookSdk.sdkInitialize(getContext());

            if(AppInviteDialog.canShow()){
                AppInviteContent.Builder content = new AppInviteContent.Builder();
                content.setApplinkUrl(getContext().getResources().getString(R.string.facebook_applink));
                content.setPreviewImageUrl("https://www.miercolesdefut.com.mx/site-content/f_invite.png");
                AppInviteContent appInviteContent = content.build();

                AppInviteDialog appInviteDialog = new AppInviteDialog(RegistrarEquipo.this);
                //appInviteDialog.registerCallback(callbackManager, facebookCallback);
                appInviteDialog.show(RegistrarEquipo.this, appInviteContent);


                /*
                AppInviteContent content = new AppInviteContent.Builder()
                        .setApplinkUrl(getContext().getResources().getString(R.string.facebook_applink))
                        .setPreviewImageUrl("https://www.miercolesdefut.com.mx/site-content/f_invite.png")
                        .build();

                AppInviteDialog.show(getActivity(), content);
                */
            }
        });
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
