package com.miercolesdefut.view.buissnes.matchs;

import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.miercolesdefut.R;
import com.miercolesdefut.ServiceCoronaImp;
import com.miercolesdefut.logic.ServiceCorona;
import com.miercolesdefut.logic.configurations.FacebookUtils;
import com.miercolesdefut.logic.dao.CheckedTeams;
import com.miercolesdefut.logic.dao.Matche;
import com.miercolesdefut.logic.dao.Team;
import com.miercolesdefut.logic.model.BlockListData;
import com.miercolesdefut.logic.model.UpdaterListData;
import com.miercolesdefut.logic.model.rxbux.App;
import com.miercolesdefut.view.buissnes.alerts.CoronaAlertDialog;
import com.miercolesdefut.view.buissnes.matchs.PartidosActivosFragment.OnListFragmentInteractionListener;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;

public class PartidosActivosRecyclerViewAdapter extends RecyclerView.Adapter<PartidosActivosRecyclerViewAdapter.ViewHolder> implements UpdaterListData<List<Matche>>, BlockListData{
    private ServiceCorona serviceCorona = new ServiceCoronaImp();
    private final List<Matche> mValues;
    private final OnListFragmentInteractionListener mListener;
    private Team equipo;

    private boolean toFragmentYallegue = false;
    private boolean blockEvents = false;
    private boolean isMainScreen = false;
    private boolean showChecking = true;

    private boolean checked = false;
    private Matche checkedMatch;
    private String teamName;

    private Tracker mTracker;

    public void setmTracker(Tracker mTracker) {
        this.mTracker = mTracker;
    }

    String fechaPivote = null;

    private CheckedTeams teamChecked;

    private FragmentManager fragmentManager;

    public void setFragmentManager(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
    }

    public PartidosActivosRecyclerViewAdapter(List<Matche> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }
    public PartidosActivosRecyclerViewAdapter(List<Matche> items, OnListFragmentInteractionListener listener, Team team) {
        this(items, listener);
        this.equipo = team;
    }

    public PartidosActivosRecyclerViewAdapter(List<Matche> items, OnListFragmentInteractionListener listener, boolean mainScreen) {
        this(items, listener);
        isMainScreen = mainScreen;
    }

    public PartidosActivosRecyclerViewAdapter(List<Matche> items, OnListFragmentInteractionListener listener, boolean mainScreen, boolean showChecking) {
        this(items, listener, mainScreen);
        this.setShowChecking(showChecking);
    }


    public PartidosActivosRecyclerViewAdapter(List<Matche> items, OnListFragmentInteractionListener listener, Team team, boolean toFragmentYaLLegue) {
        this(items, listener);
        this.equipo = team;
        this.toFragmentYallegue = toFragmentYaLLegue;
    }

    public void setCheckedMatch(Matche checkedMatch) {
        this.checkedMatch = checkedMatch;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_partidosactivos, parent, false);
        return new ViewHolder(view);
    }

    public void setCheckedTeam(CheckedTeams checkedTeams) {
        this.teamChecked = checkedTeams;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.partido = mValues.get(position);
        holder.equipoANombre.setText(holder.partido.getTeam_a().getName());
        holder.equipoBNombre.setText(holder.partido.getTeam_b().getName());
        Picasso.with(holder.mView.getContext()).load(holder.partido.getTeam_a().getImage()).into(holder.equipoAImage);
        Picasso.with(holder.mView.getContext()).load(holder.partido.getTeam_b().getImage()).into(holder.equipoBImage);
        holder.horarioPartido.setText(holder.partido.getStart_time_format());

        holder.mView.setOnClickListener((view)->{
            if(mTracker!=null) mTracker.send(new HitBuilders.EventBuilder().setCategory("button").setAction("touch").setLabel("home-checkin").setValue(holder.partido.getId()).build());



            if(blockEvents){
                CoronaAlertDialog coronaAlert = CoronaAlertDialog.newInstance(CoronaAlertDialog.ERROR_NO_TEAMS, new HashMap<>());
                coronaAlert.show(fragmentManager, null);
                return;
            }
            if (null != mListener) {
                if(isMainScreen){
                    mListener.onListFragmentMainScreen(holder.partido, equipo);
                    return;
                }
                if(toFragmentYallegue){
                    mListener.toDetalleEquipoActivo(holder.partido, equipo);
                } else {
                    mListener.onListFragmentInteraction(holder.partido, equipo);
                }
            }
        });

        if(fechaPivote == null || !fechaPivote.equals(holder.partido.getStart_date_format())){
            fechaPivote = holder.partido.getStart_date_format();
            holder.layoutFechaPartido.setVisibility(View.VISIBLE);
            holder.fechaPartido.setText(holder.partido.getStart_date_format().toUpperCase());
        }

        if(isShowChecking()){
            if(checked){
                holder.layoutChecking.setVisibility(View.GONE);

                if( holder.partido.getId()== checkedMatch.getId() ){
                    holder.layoutChecking.setVisibility(View.VISIBLE);

                    holder.checkinText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    String append = holder.mView.getResources().getString(R.string.estasCon);
                    holder.checkinText.setText(append.replace("%1$S", teamName.toUpperCase()));
                    showDetalleEquipo(holder);
                }
            }else{
                holder.layoutChecking.setVisibility(View.VISIBLE);
            }
        } else {
            holder.layoutChecking.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    @Override
    public void addItemsToList(List<Matche> data) {
        if(data == null || data.isEmpty()){
            return;
        }
        mValues.addAll(data);
        notifyItemRangeInserted(0, data.size());
    }

    @Override
    public void clearList() {
        mValues.clear();
        notifyDataSetChanged();
    }

    @Override
    public void setBlockListData(boolean block) {
        this.blockEvents = block;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public boolean isShowChecking() {
        return showChecking;
    }

    public void setShowChecking(boolean showChecking) {
        this.showChecking = showChecking;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public boolean getChecked(){
        return checked;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView equipoANombre;
        public final TextView equipoBNombre;
        public final ImageView equipoAImage;
        public final ImageView equipoBImage;
        public final View layoutFechaPartido;
        public final TextView fechaPartido;
        public final View layoutItemRecyclerView;
        public final View layoutChecking;
        public final TextView horarioPartido;
        public Matche partido;

        public final Button btnDetalle;


        public final TextView checkinText;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            equipoANombre = (TextView) view.findViewById(R.id.equipoANombre);
            equipoBNombre = (TextView) view.findViewById(R.id.equipoBNombre);
            equipoAImage = (ImageView) view.findViewById(R.id.equipoAImagen);
            equipoBImage = (ImageView) view.findViewById(R.id.equipoBImagen);
            layoutFechaPartido = view.findViewById(R.id.layoutFechaPartidoActivo);
            fechaPartido = (TextView) view.findViewById(R.id.fechaPartido);
            layoutItemRecyclerView = view.findViewById(R.id.layoutItemRecyclerView);
            layoutChecking = view.findViewById(R.id.layoutChecking);
            horarioPartido = (TextView)view.findViewById(R.id.horarioPartido);
            checkinText = (TextView) itemView.findViewById(R.id.textView15);
            btnDetalle = (Button) view.findViewById(R.id.mainDetalleEquipoActivo);
        }
    }

    private void showDetalleEquipo(PartidosActivosRecyclerViewAdapter.ViewHolder viewHolder){
        viewHolder.layoutItemRecyclerView.setOnClickListener((event)->{
            serviceCorona.getEquipos(FacebookUtils.getId(viewHolder.mView.getContext()),response->{
                if(response.isSuccessful()){
                    List<Team> equipos = response.body();
                    Team detalleEquipo = null;
                    for(Team equipoL: equipos){
                        if(equipoL.getId() == teamChecked.getTeam().getId()){
                            detalleEquipo = equipoL;
                            break;
                        }
                    }
                    if(detalleEquipo != null){
                        mListener.toDetalleEquipoActivo(viewHolder.partido, detalleEquipo);
                    }
                }
            });

        });
    }
}
