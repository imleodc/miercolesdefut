package com.miercolesdefut.view.buissnes.teams;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.miercolesdefut.R;
import com.miercolesdefut.logic.dao.Team;
import com.miercolesdefut.logic.dao.TeamStatusMembers;
import com.miercolesdefut.view.BaseFragment;

import rx.Subscription;

/**
 * Created by lauro on 25/01/2017.
 */

public class FragmentIntermedio extends BaseFragment {
    private Team equipo;
    Subscription subscription;

    public static FragmentIntermedio newInstance() {
        Bundle args = new Bundle();
        FragmentIntermedio fragment = new FragmentIntermedio();
        fragment.setArguments(args);
        return fragment;
    }

    public static FragmentIntermedio newInstance(Team team) {
        Bundle args = new Bundle();
        FragmentIntermedio fragment = new FragmentIntermedio();
        args.putSerializable(RegistrarEquipo.KEY_TEAM_COMPLETE, team);
        fragment.setArguments(args);
        return fragment;
    }

    public static FragmentIntermedio newInstance(Team team, TeamStatusMembers statusMembers) {
        Bundle args = new Bundle();
        FragmentIntermedio fragment = new FragmentIntermedio();
        args.putSerializable(RegistrarEquipo.KEY_TEAM_COMPLETE, team);
        args.putSerializable(RegistrarEquipo.KEY_TEAM_MEMBERS_STATUS, statusMembers);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null){
            equipo = (Team) getArguments().getSerializable(RegistrarEquipo.KEY_TEAM_COMPLETE);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_intermedio_teamfriends_facebook_invitatio, container,false);
        if(equipo == null){
            getFragmentManager().beginTransaction().add(R.id.friendsListView, CreateTeamFriendsFragment.newInstance()).commit();
        } else {
            getFragmentManager().beginTransaction().add(R.id.friendsListView, CreateTeamFriendsFragment.newInstance(equipo)).commit();
        }

        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if(subscription != null){
            subscription.unsubscribe();
        }
    }
}
