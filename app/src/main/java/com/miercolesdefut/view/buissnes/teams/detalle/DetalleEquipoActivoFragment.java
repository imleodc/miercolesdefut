package com.miercolesdefut.view.buissnes.teams.detalle;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.FileProvider;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ImageSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.miercolesdefut.R;
import com.miercolesdefut.ServiceCoronaImp;
import com.miercolesdefut.Utils;
import com.miercolesdefut.logic.ConstantsApplication;
import com.miercolesdefut.logic.ServiceCorona;
import com.miercolesdefut.logic.configurations.FacebookUtils;
import com.miercolesdefut.logic.dao.AcceptTeam;
import com.miercolesdefut.logic.dao.CheckedTeams;
import com.miercolesdefut.logic.dao.DetailMatchPerfil;
import com.miercolesdefut.logic.dao.HistoriUser;
import com.miercolesdefut.logic.dao.LocationError;
import com.miercolesdefut.logic.dao.Matche;
import com.miercolesdefut.logic.dao.Perfil;
import com.miercolesdefut.logic.dao.PuntosPorCompartir;
import com.miercolesdefut.logic.dao.Reference;
import com.miercolesdefut.logic.dao.Registro;
import com.miercolesdefut.logic.dao.Team;
import com.miercolesdefut.logic.dao.gps.CheckPointResult;
import com.miercolesdefut.logic.model.rxbux.App;
import com.miercolesdefut.view.BaseActivity;
import com.miercolesdefut.view.BaseFragment;
import com.miercolesdefut.view.buissnes.alerts.CoronaAlertDialog;
import com.miercolesdefut.view.buissnes.historial.HistorialFragment;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.jakewharton.rxbinding.view.RxView;
import com.squareup.picasso.Picasso;
import com.tbruyelle.rxpermissions.RxPermissions;
import com.trello.rxlifecycle.android.FragmentEvent;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.ResponseBody;
import pl.charmas.android.reactivelocation.ReactiveLocationProvider;
import retrofit2.Response;
import rx.Observable;
import rx.Subscription;
import rx.functions.Action1;

/**
 * Created by lauro on 19/01/2017.
 */

public class DetalleEquipoActivoFragment extends BaseFragment {

    private UpdateLocation updateLocation;

    private static final int CAMERA_REQUES = 1888;
    ServiceCorona serviceCorona = new ServiceCoronaImp();

    Subscription subscription;
    Subscription subscriptionLocation;
    Subscription updateData;
    public static final String KEY_TEAM    = "teamKey";
    public static final String KEY_PARTIDO = "partidoKey";
    public static final String KEY_AUTO_CHECKING = "keyAtuoChecking";
    public static final String KEY_PERFIL_UPDATE = "keyAutoPErfilUpdate";
    public static final String PROCCESS_DETAIL_PERFIL_RECEIVER = "process.detail.perfil.receiber.background";
    public static final String TAG ="DetalleEquipoActivoFr";

    private final String INIT_SCREEN = "team/check";

    protected TextView nombre;
    protected TextView totalScore;
    protected Team equipo;
    protected Matche partido;
    protected RxPermissions rxPermissions;
    protected View ahoraJuntos;
    protected CallbackManager callBackManager ;
    private float maxDistance = 200.0f;
    protected View layoutPointsUbication;
    protected TextView pointsUbicacion;

    public DetalleEquipoActivoFragment(){
        setImageBackground(R.drawable.fondo);
    }
    protected ShareDialog shareDialog;
    protected Subscription compartir;
    protected TextView equipoActivoDetallePoints;
    protected ScrollView mainScroll;
    boolean autoChecking = false;
    protected MapView mapView;
    private int countIntentRefresh = 0;
    UpdatePerfilReceiver updatePerfilReceiver;
    Marker currentPosition;
    private Subscription intervalPosition;
    private Marker destino;
    private Circle radioChecking;
    private TextView distanciaView;
    private boolean canSharePhoto = false;
    private Button fbCompartir;
    boolean userMakeCheckin = false;
    private Subscription subscriptionBusProfile;

    public Action1<Location> onAccept = (location)->{
            Reference reference = new Reference();
            reference.setFacebook_id(FacebookUtils.getId(getContext()));
            reference.setMatch_id(partido.getId());
            reference.setTeam_id(equipo.getId());
            reference.setLongitude(String.valueOf(location.getLongitude()));
            reference.setLatitude(String.valueOf(location.getLatitude()));
        ProgressDialog progressDialog = BaseActivity.createProgressDialog(getContext());
        progressDialog.show();
            serviceCorona.setCheckIn(reference, (response)-> {
                progressDialog.dismiss();
                if(response.isSuccessful()){
                    ConstantsApplication.getInstances().setMakeChecking(true);
                    CheckPointResult resultData = response.body();
                    referenciaUser();
                    equipoActivoDetallePoints.setText(String.valueOf(resultData.points.total)+" PTS.");
                    Registro registro = new Registro();
                    registro.setFacebook_id(FacebookUtils.getId(getContext()));
                    resultData.checked.add(registro);
                    FragmentManager fragmentManager = getChildFragmentManager();
                    if(fragmentManager != null) {
                        fragmentManager.beginTransaction().replace(R.id.friendsCreateTeam, DetalleListaAmgigos.newInstance(equipo.getMembers(),resultData.checked,true)).commit();
                    }
                    if(resultData.points != null && resultData.points.location != null){
                        showBonoUbication(resultData.points.location);
                    }
                } else {
                    LocationError error = Utils.converterErrorLocation.call(response.errorBody(), ((ServiceCoronaImp)serviceCorona).retrofit);
                    LocationError.LocalError localError = null;
                    if(error.getTeam_id() != null && error.getTeam_id().get(0) != null){
                        localError = error.getTeam_id().get(0);
                    }
                    if(localError != null && localError.getReference() != null){
                        updateMap(localError.getReference(), equipo.getName());
                    }
                }
                updateData();
            });
    };

    private void referenciaUser() {
        serviceCorona.getReferenciaUsuario(FacebookUtils.getId(getContext()), (response)->{
            if(response.isSuccessful()){
                List<com.miercolesdefut.logic.dao.gps.Location> locations = response.body();
                if(locations == null || locations.isEmpty()){
                    return;
                }
                for(com.miercolesdefut.logic.dao.gps.Location loc: locations){
                    if(loc.getTeam().getId() == equipo.getId()){
                        updateMap(loc);
                    }
                }
            } else {
                /*
                try {
                //  Log.i(TAG, response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                */
            }
        });
    }

    private void partidosPuntos() {
        AcceptTeam teamH = new AcceptTeam();
        teamH.setTeam_id(equipo.getId());
        teamH.setFacebook_id(FacebookUtils.getId(getContext()));
        serviceCorona.getDetalleUsuario(FacebookUtils.getId(getContext()), (response)->{
            if(response.isSuccessful()){
                Perfil perfil = response.body();
                CheckedTeams detailMatch = perfil.getCheckedTeamUser();
                if(detailMatch.getTeam().getId() == equipo.getId()){
                    updateHistory(null, perfil.getCheckedTeamUser().getMember_checks());
                }
                AcceptTeam team = new AcceptTeam();
                team.setTeam_id(equipo.getId());
                team.setFacebook_id(FacebookUtils.getId(getContext()));
                serviceCorona.getPartidosPuntos(team, responseH->{
                    if(responseH.isSuccessful()){
                        HistoriUser history = responseH.body();
                        updateTotalScore(history.getPoints().getTotal());
                    }
                });
            }
        });
    }

    protected void updateHistory(String points, List<Registro> usersCheck){
        if(points != null){
            equipoActivoDetallePoints.setText(points+" PTS.");
        }
        if(usersCheck != null) {
            FragmentManager fm = getChildFragmentManager();
            if(fm != null){
                fm.beginTransaction().replace(R.id.friendsCreateTeam, DetalleListaAmgigos.newInstance(equipo.getMembers(),usersCheck, true)).commitAllowingStateLoss();//commit();
            }
        }
    }

    protected void updateMap(LocationError.ReferencesError reference, String team) {
        if(reference == null) {
            return;
        }
        mapView.getMapAsync((mMap)->{
            LatLng ref = new LatLng(reference.getLatitude(), reference.getLongitude());
            destino = mMap.addMarker(new MarkerOptions().position(ref).title(team));
            CameraPosition cameraPosition = new CameraPosition.Builder().target(ref).zoom(18).build();
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        });
    }

    protected void updateMap(com.miercolesdefut.logic.dao.gps.Location loc){
        if(loc == null) {
            return;
        }
        mapView.getMapAsync((mMap)->{
            LatLng ref = new LatLng(loc.getLatitude(), loc.getLongitude());
            CircleOptions circleOptions = new CircleOptions().center(ref).fillColor(getResources().getColor(R.color.pendiente_transparente)).strokeColor(getResources().getColor(R.color.pendiente)).radius(10);
            mMap.addMarker(new MarkerOptions().position(ref).title(loc.getTeam().getName()));
            radioChecking = mMap.addCircle(circleOptions);
            if(userMakeCheckin){
                radioCheckingValido();
            }
            CameraPosition cameraPosition = new CameraPosition.Builder().target(ref).zoom(18).build();
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        });
    }


    protected Action1<Location> setReference = new Action1<Location>() {
        @Override
        public void call(final Location location) {
            serviceCorona.getPartidos(new Action1<Response<List<Matche>>>() {
                @Override
                public void call(Response<List<Matche>> listResponse) {
                    if(listResponse.isSuccessful()){
                        Matche partido = listResponse.body().get(0);
                        Reference reference = new Reference();
                        reference.setFacebook_id(FacebookUtils.getId(getContext()));
                        reference.setMatch_id(partido.getId());
                        reference.setTeam_id(equipo.getId());
                        reference.setLongitude(String.valueOf(location.getLongitude()));
                        reference.setLatitude(String.valueOf(location.getLatitude()));
                        serviceCorona.setReference(reference, new Action1<Response<ResponseBody>>() {
                            @Override
                            public void call(Response<ResponseBody> responseBodyResponse) {
                                if(responseBodyResponse.isSuccessful()){
                                    Toast.makeText(getContext(), "Check In valido.", Toast.LENGTH_SHORT).show();
                                } else {
                                    try {
                                        Toast.makeText(getContext(), "Check In invalido."+responseBodyResponse.errorBody().string(), Toast.LENGTH_SHORT).show();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        });
                    }
                }
            });
        }
    };

    public static DetalleEquipoActivoFragment newInstance(Team equipo) {
        DetalleEquipoActivoFragment fragment = new DetalleEquipoActivoFragment();
        Bundle args = new Bundle();
        args.putSerializable(KEY_TEAM, equipo);
        fragment.setArguments(args);
        return fragment;
    }

    public static DetalleEquipoActivoFragment newInstance(Team equipo, Matche partido){
        DetalleEquipoActivoFragment fragment = new DetalleEquipoActivoFragment();
        Bundle args = new Bundle();
        args.putSerializable(KEY_TEAM, equipo);
        args.putSerializable(KEY_PARTIDO, partido);
        fragment.setArguments(args);
        return fragment;
    }

    public static DetalleEquipoActivoFragment newInstance(Team equipo, Matche partido, boolean autoChecking){
        DetalleEquipoActivoFragment fragment = new DetalleEquipoActivoFragment();
        Bundle args = new Bundle();
        args.putSerializable(KEY_TEAM, equipo);
        args.putSerializable(KEY_PARTIDO, partido);
        args.putBoolean(KEY_AUTO_CHECKING, autoChecking);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        showActionBar(getResources().getString(R.string.misEquipos));

        Tracker mTracker = ((App) getActivity().getApplication()).getDefaultTracker();
        mTracker.setScreenName(INIT_SCREEN);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        rxPermissions = new RxPermissions(getActivity());
        if (getArguments() != null) {
            equipo = (Team) getArguments().getSerializable(KEY_TEAM);
            partido = (Matche) getArguments().getSerializable(KEY_PARTIDO);
            autoChecking = getArguments().getBoolean(KEY_AUTO_CHECKING, false);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ProgressDialog progressDialog = BaseActivity.createProgressDialog(getContext());
        progressDialog.show();
        View view = inflater.inflate(R.layout.fragment_detalle_equipo, container, false);
        if(!checkGPSEnabled()){
            progressDialog.dismiss();
            Toast.makeText(getContext(), "Se requiere GPS encendido.", Toast.LENGTH_LONG).show();
            getFragmentManager().popBackStack();
            return view;
            //getFragmentManager().beginTransaction().replace(R.id.content_frame, InicioPartidosActivos.newInstance("","")).commit();
        }
        distanciaView = (TextView) view.findViewById(R.id.distanciaViewDetalle);
        configMainScroll(view);
        configPuntosPartido(view);
        configHistorial(view);
        updateNombreEquipo(view);
        configAhoraJuntos(view);
        configFbButton(view);
        configPuntosEquipo(view);
        configBonoUbicacion(view);
        updatePartidoView(view);
        rxPermissions
                .request(Manifest.permission.ACCESS_FINE_LOCATION)
                .subscribe((granted)->{
                    if(granted){
                        configMap(view, savedInstanceState);
                        serviceCorona.getDetalleUsuario(FacebookUtils.getId(getContext()), (response)->{
                            if(response.isSuccessful()){
                                Perfil perfil = response.body();
                                maxDistance = (perfil.getMemberDistance()<5) ? 200:perfil.getConfig().getMemberDistance();

                                if(perfil.hasMakeCheck()){
                                    userMakeCheckin = true;
                                    canSharePhoto = true;
                                    updateHistoryFromPerfil(perfil);
                                    if(distanciaView != null) {
                                        distanciaView.setVisibility(View.GONE);
                                    }
                                } else {
                                    trackPosition(onAccept);
                                    updateTotalScoreFromService();
                                    trackCurrenPosition();
                                }
                            }
                        });
                        referenciaUser();
                    } else {
                        Toast.makeText(getContext(), "Se requieren los permisos de GPS.", Toast.LENGTH_LONG);
                        if(getFragmentManager() != null) {
                            getFragmentManager().popBackStack();
                        }
                    }
                });
        updateData();
        getFragmentManager().beginTransaction().add(R.id.friendsCreateTeam, DetalleListaAmgigos.newInstance(equipo.getMembers(),true)).commit();
        progressDialog.dismiss();
        loadHistory();
        return view;
    }

    private void loadHistory() {
        //serviceCorona.
    }

    private void configBonoUbicacion(View view) {
        layoutPointsUbication = view.findViewById(R.id.layoutPointsUbication);
        pointsUbicacion = (TextView) view.findViewById(R.id.pointUbication);
    }

    private void trackCurrenPosition() {
        ReactiveLocationProvider locationProvider = new ReactiveLocationProvider(getContext());
        final LocationRequest req = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setNumUpdates(1);
        intervalPosition = Observable.interval(5, TimeUnit.SECONDS).timeInterval().subscribe((t)->{
            locationProvider.getUpdatedLocation(req).subscribe((location)->{
                if(currentPosition != null && destino != null){
                    LatLng posicion = new LatLng(location.getLatitude(), location.getLongitude());
                    currentPosition.setPosition(posicion);
                    LatLngBounds.Builder builder = new LatLngBounds.Builder();
                    builder.include(currentPosition.getPosition());
                    builder.include(destino.getPosition());
                    LatLngBounds bounds = builder.build();
                    mapView.getMapAsync((mMap)->{
                        int padding = 0;
                        mMap.setPadding(100,100,100,100);
                        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                        mMap.moveCamera(cu);
                        Location locationA = new Location("point A");
                        locationA.setLatitude(destino.getPosition().latitude);
                        locationA.setLongitude(destino.getPosition().longitude);
                        Location locationB = new Location("point B");
                        locationB.setLatitude(currentPosition.getPosition().latitude);
                        locationB.setLongitude(currentPosition.getPosition().longitude);
                        float distance = locationA.distanceTo(locationB);
                        updateDistanciaView(distance);
                    });

                } else {
                    mapView.getMapAsync((mMap)->{
                        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                        MarkerOptions marker = new MarkerOptions().position(latLng).alpha(0.0f);
                        currentPosition = mMap.addMarker(marker);
                    });
                }
            });
        });

    }


    private void updateDistanciaView(float distancia) {
        if(distanciaView != null){
            if(distancia > maxDistance){
                distanciaView.setTextColor(getResources().getColor(R.color.pendiente));
            } else {
                distanciaView.setTextColor(getResources().getColor(R.color.completado));
                trackPosition(onAccept);
                radioCheckingValido();
                intervalPosition.unsubscribe();
            }
            distanciaView.setText(String.valueOf(distancia));
        }
    }

    private void radioCheckingValido(){
        radioChecking.setFillColor(getResources().getColor(R.color.completado_trasnparente));
        radioChecking.setStrokeColor(getResources().getColor(R.color.completado));
    }

    protected void configMainScroll(View view){
        mainScroll = (ScrollView) view.findViewById(R.id.mainScroll);
    }

    protected void configPuntosEquipo(View view){
        totalScore = (TextView)view.findViewById(R.id.equipoActivoTotalScore);
    }

    protected void configPuntosPartido(View view){
        equipoActivoDetallePoints = (TextView) view.findViewById(R.id.equipoActivoDetallePoints);
    }

    protected void configHistorial(View view){
        Button historial = (Button) view.findViewById(R.id.historialBtn);
        subscription = RxView.clicks(historial).subscribe((vacio)->{
            ((App)getActivity().getApplication()).getDefaultTracker().send(new HitBuilders.EventBuilder().setCategory("button").setAction("touch").setLabel("team-historic").setValue(equipo.getId()).build());
            ((BaseActivity)getActivity()).change(HistorialFragment.newInstance(equipo));
        });
    }

    protected void updateNombreEquipo(View view){
        nombre = (TextView) view.findViewById(R.id.teamName);
        if(equipo != null){
            nombre.setText(equipo.getName());
        }
    }

    protected void configAhoraJuntos(View view){
        Spannable llegueBtnLabel = new SpannableString(getResources().getString(R.string.yaLlegue));
        llegueBtnLabel.setSpan(new ImageSpan(getContext(), R.drawable.check_in_match_active, ImageSpan.ALIGN_BASELINE), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        ahoraJuntos = view.findViewById(R.id.layoutAhoraJuntos);
    }


    protected void updateTotalScore(int puntos){
        if(totalScore == null){
            return;
        }
        totalScore.setText(getResources().getString(R.string.format_pts, String.valueOf(puntos)));
    }

    protected CheckedTeams hasMakeChecking(DetailMatchPerfil matchs){
        if(matchs == null){
            return null;
        }
        for(CheckedTeams team:matchs.getTeams()) {
            if(team.isUser_has_checked()){
                return team;
            }
        }
        return null;
    }

    protected boolean checkGPSEnabled(){
        final LocationManager manager = (LocationManager) getActivity().getSystemService( Context.LOCATION_SERVICE );
        if ( !manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
            /*CoronaAlertDialog alert = CoronaAlertDialog.newInstance(CoronaAlertDialog.ACTION_MESSAGE, "Se requiere GPS encendido.");
            alert.show(getFragmentManager(), null);*/
            return false;
        }
        return  true;
    }

    protected void configFbButton(View view){
        FacebookSdk.sdkInitialize(getContext());
        callBackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);
        shareDialog.registerCallback(callBackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
            }

            @Override
            public void onCancel() {
            }

            @Override
            public void onError(FacebookException error) {

            }
        });
        Spannable buttonLabel = new SpannableString("i   "+getResources().getString(R.string.compartirMomento));
        buttonLabel.setSpan(new ImageSpan(getContext(), R.drawable.fb_icon_blue, ImageSpan.ALIGN_BASELINE), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        fbCompartir = (Button) view.findViewById(R.id.fbCompartirPts);
        fbCompartir.setText(buttonLabel);

        compartir = RxView.clicks(fbCompartir).filter((vacio)->{
            /*
            if(!canSharePhoto){
                Toast.makeText(getContext(), "Necesitas estar Reunido con almenos 3 de tus amigos.", Toast.LENGTH_LONG).show();
            }
            */
            return true;
        }).subscribe((event)->{
            if(!Utils.isAppInstalled(getContext(), "com.facebook.katana")){
                Toast.makeText(getContext(), "No se encuentra la aplicación de Facebook instalada.", Toast.LENGTH_SHORT).show();
                return;
            }

            ((App)getActivity().getApplication()).getDefaultTracker().send(new HitBuilders.EventBuilder().setCategory("button").setAction("touch").setValue(equipo.getId()).build());
            Uri tempFile = configTempFile();
            rxPermissions.request(Manifest.permission.CAMERA).subscribe((granted)->{
                if(granted){
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, tempFile);
                    intent.setFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                    startActivityForResult(intent, CAMERA_REQUES);
                } else {
                    Toast.makeText(getContext(), "Se requieren los permisos de CAMARA, para poder compartir la foto.", Toast.LENGTH_SHORT).show();
                }
            });
        });
    }

    protected void updatePartidoView(View view){
        ImageView imViewA = (ImageView) view.findViewById(R.id.equipoAImagen);
        ImageView imViewB = (ImageView) view.findViewById(R.id.equipoBImagen);
        TextView equipoAN = (TextView) view.findViewById(R.id.equipoANombre);
        TextView equipoBN = (TextView) view.findViewById(R.id.equipoBNombre);
        TextView horarioPartido = (TextView) view.findViewById(R.id.horarioPartido);
        if(imViewA != null ){
            Picasso.with(getContext()).load(partido.getTeam_a().getImage()).into(imViewA);
        }
        if(imViewB != null ){
            Picasso.with(getContext()).load(partido.getTeam_b().getImage()).into(imViewB);
        }
        if(equipoAN != null){
            equipoAN.setText(partido.getTeam_a().getName());
        }
        if(equipoBN != null){
            equipoBN.setText(partido.getTeam_b().getName());
        }
        if(horarioPartido != null){
            horarioPartido.setText(partido.getStart_time_format());
        }
    }


    protected Uri configTempFile() {
        //Log.i(TAG, getContext().getFilesDir().toString());
        File imagePath = new File(getContext().getFilesDir(), "pictures");
        if(!imagePath.exists()){
            imagePath.mkdirs();
        }
        File newFile = new File(imagePath, "facebookSharePhoto.jpg");
        if(!newFile.exists()){
            try {
                newFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Uri contentUri = FileProvider.getUriForFile(getContext(), "com.miercolesdefut.fileprovider", newFile);
        return contentUri;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case 1:
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    trackPosition(onAccept);
                }
                break;
        }
    }


    protected void checkPerfilUser(){
        serviceCorona.getDetalleUsuario(FacebookUtils.getId(getContext()), (response)->{
            if(response.isSuccessful()){
                Perfil perfil = response.body();
                if(perfil.hasMakeCheck()){
                    CheckedTeams team = perfil.getCheckedTeamUser();
                    if(team.getTeam().getId() == equipo.getId()){
                        updateData();
                    }
                }
            }
        });
    }

    protected void updateData(){
        updatePerfilReceiver = new UpdatePerfilReceiver();
       getActivity().registerReceiver(updatePerfilReceiver, new IntentFilter(PROCCESS_DETAIL_PERFIL_RECEIVER));
    }

    protected void trackPosition(Action1<Location> action1){
        ProgressDialog progressDialog = BaseActivity.createProgressDialog(getContext());
        progressDialog.show();
        ReactiveLocationProvider locationProvider = new ReactiveLocationProvider(getContext());
        LocationRequest req = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setNumUpdates(3)
                .setInterval(10);
        countIntentRefresh = 0;
        subscriptionLocation = locationProvider.getUpdatedLocation(req)
                .subscribe((location)->{
                    if(countIntentRefresh == 2){
                        progressDialog.dismiss();
                        //Log.i(TAG,"Location alt:"+location.getLatitude()+", lng:"+location.getLongitude()+"acurazy:"+location.getAccuracy());
                        action1.call(location);
                        return;
                    }
                    countIntentRefresh++;
                },onError->onError.printStackTrace());
        //Log.i(TAG, ""+countIntentRefresh);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if(subscription != null){
            subscription.unsubscribe();
        }
        if(compartir != null){
            compartir.unsubscribe();
        }
        if(updateData != null) {
            updateData.unsubscribe();
        }
        if(updatePerfilReceiver != null){
            getActivity().unregisterReceiver(updatePerfilReceiver);
        }
        if(intervalPosition != null) {
            intervalPosition.unsubscribe();
        }

        if(updateLocation != null) {
            try{
                getActivity().unregisterReceiver(updateLocation);
            }catch(IllegalArgumentException ex){
                ex.printStackTrace();
            }
        }
        //Log.i(TAG, "onDetach()");
    }

    protected void sharePhoto(ShareContent content, File photo){
        FacebookSdk.sdkInitialize(getContext());
        callBackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);
        shareDialog.registerCallback(callBackManager, new FacebookCallback<Sharer.Result>() {
            @SuppressLint("LongLogTag")
            @Override
            public void onSuccess(Sharer.Result result) {
                result.getPostId();
                File photoL = saveBitmapToFile(photo);
                trackPosition((location)->{
                    //Log.i(TAG, "lat:"+location.getLatitude()+", lng:"+location.getLongitude());
                    //Log.i(TAG, "photo length:"+ photoL.length());
                    serviceCorona.sharePhoto(
                            FacebookUtils.getId(getContext()),
                                    partido.getId(),equipo.getId(),
                                    location.getLatitude(),
                                    location.getLongitude(),
                            photoL, (response)->{
                                if(response.isSuccessful()){
                                    PuntosPorCompartir puntos = response.body();
                                    updateHistory(String.valueOf(puntos.total),null);

                                    Map<String, String> parametros = new HashMap<>();
                                    parametros.put(CoronaAlertDialog.ACTION_AUTO_CHECKING_POINTS, String.valueOf(puntos.granted.share));
                                    parametros.put(CoronaAlertDialog.ACTION_AUTO_CHECKING_TEAM, equipo.getName());
                                    CoronaAlertDialog coronaAlert = CoronaAlertDialog.newInstance(CoronaAlertDialog.SHARE_PICTURE, parametros);
                                    coronaAlert.show(getFragmentManager(), null);
                                } else {
                                    try {
                                        //Log.i(TAG, response.errorBody().string());
                                        LocationError error = Utils.converterErrorLocation.call(response.errorBody(),((ServiceCoronaImp)serviceCorona).retrofit);
                                        if(error.photo != null && !error.photo.isEmpty()){
                                            DialogFragment alert = CoronaAlertDialog.newInstance(CoronaAlertDialog.ACTION_MESSAGE, error.getMessage());
                                            alert.show(getFragmentManager(), null);
                                            return;
                                        }
                                        Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                                        DialogFragment alert = CoronaAlertDialog.newInstance(CoronaAlertDialog.ACTION_CHECKIN_LEJOS);
                                        alert.show(getFragmentManager(),null);
                                        //Log.i(TAG, response.errorBody().string());
                                    } catch (Exception e) {
                                        Toast.makeText(getContext(), "Ocurrio un error inesperado.", Toast.LENGTH_SHORT).show();
                                        e.printStackTrace();
                                    }
                                }
                                photoL.delete();
                            });
                    });
            }

            @Override
            public void onCancel() {
                if(photo != null ) {
                    photo.delete();
                }
            }

            @Override
            public void onError(FacebookException error) {
                error.printStackTrace();
                photo.delete();
            }
        });
        ShareDialog.show(this, content);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == CAMERA_REQUES && resultCode == Activity.RESULT_OK) {
            File imagePath = new File(getContext().getFilesDir(), "pictures");
            File photoFile = new File(imagePath, "facebookSharePhoto.jpg");
            SharePhoto photo = new SharePhoto.Builder().setImageUrl(Uri.fromFile(photoFile)).build();
            ShareContent content = new SharePhotoContent.Builder().addPhoto(photo).build();
            sharePhoto(content, photoFile);
        } else {
            if(callBackManager != null){
                callBackManager.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    protected void configMap(View container, Bundle savedInstanceState){
        mapView = (MapView) container.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.onResume();

        MapsInitializer.initialize(getActivity().getApplicationContext());

        mapView.getMapAsync((mMap)->{
            mMap.setMyLocationEnabled(true);
            LatLng latLng= new LatLng(19.4284700, -99.1276600);
            CameraUpdate cameraPosition = CameraUpdateFactory.newLatLngZoom(latLng, 5);
            mMap.moveCamera(cameraPosition);
            mMap.animateCamera(cameraPosition);
        });

        mapView.setOnFocusChangeListener((v, hasFocus)->{
            if(hasFocus){
                mainScroll.requestDisallowInterceptTouchEvent(true);
            } else {
                mainScroll.requestDisallowInterceptTouchEvent(false);
            }
        });

        /*mapView.setOnTouchListener((v, event) ->{
                switch (event.getAction()) {
                    case MotionEvent.ACTION_MOVE:
                        mainScroll.requestDisallowInterceptTouchEvent(true);
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_CANCEL:
                        mainScroll.requestDisallowInterceptTouchEvent(false);
                }
                return mapView.onTouchEvent(event);
            });*/
    }

    @Override
    public void onPause() {
        super.onPause();
        if(mapView != null) {
            mapView.onPause();
        }
        if(updateLocation != null) {
            getActivity().unregisterReceiver(updateLocation);
        }
        //Log.i(TAG, "onPause()");
    }





    @Override
    public void onResume() {
        super.onResume();
        registeredEvents();
        //Log.i(TAG, "onResume()");
    }

    protected void registeredEvents(){
        updateLocation = new UpdateLocation();
        getActivity().registerReceiver(updateLocation, new IntentFilter(UpdateLocation.CURRENT_LOCATION_FROM_BACKGROUND));
        if(mapView != null) {
            mapView.onResume();
        }

        App.get().busProfile().toObservable()
                .compose(this.bindUntilEvent(FragmentEvent.PAUSE))
                .subscribe((profileObj)->{
                    Perfil perfilBus = (Perfil) profileObj;
                    updateHistoryFromPerfil(perfilBus);
                });
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if(mapView != null) {
            mapView.onDestroy();
        }
        if(updateData != null){
            try {
                updateData.unsubscribe();
                updateData= null;
            }catch(IllegalArgumentException ex){
                ex.printStackTrace();
            }
        }
        if(updateLocation != null) {
            try{
                getActivity().unregisterReceiver(updateLocation);
                updateLocation = null;
            }catch(IllegalArgumentException ex){}
        }
        //Log.i(TAG, "onDestroy()");
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if(mapView != null) {
            mapView.onLowMemory();
        }
    }

    public class UpdateLocation extends BroadcastReceiver{

        public static final String CURRENT_LOCATION_FROM_BACKGROUND="currentlocationformbackgroundundapter";

        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle b = intent.getExtras();
            //Log.i("MESSAGE", "Current position");
        }
    }

    public class UpdatePerfilReceiver extends BroadcastReceiver{
        @Override
        public void onReceive(Context context, Intent intent) {
            Perfil perfilT = (Perfil) intent.getSerializableExtra(KEY_PERFIL_UPDATE);
            if(getContext() == null){
                return;
            }
            updateHistoryFromPerfil(perfilT);
            CheckedTeams teamsChecked = perfilT.getCheckedTeamUser();
            if(teamsChecked != null && teamsChecked.getTeam().getId() == equipo.getId() && teamsChecked.getMember_checks().size()>=3){
                canSharePhoto = true;
            }
        }
    }

    protected void updateHistoryFromPerfil(Perfil perfil){
        if(perfil.hasMakeCheck()){
            Observable
                    .just(perfil.getCheckedTeamUser()).filter((team1)->team1 != null && team1.getTeam().getId() == equipo.getId())
                    .subscribe((teamChecked)->{
                        Registro registro = new Registro();
                        if(getContext() == null){
                            return;
                        }
                        registro.setFacebook_id(FacebookUtils.getId(getContext()));
                        teamChecked.getMember_checks().add(0, registro);
                        updateHistory(null, teamChecked.getMember_checks());
                        updateTotalScoreFromService();
                    });
        }
    }

    protected void updateTotalScoreFromService(){
        AcceptTeam acceptTeam = new AcceptTeam();
        acceptTeam.setFacebook_id(FacebookUtils.getId(getContext()));
        acceptTeam.setTeam_id(equipo.getId());
        serviceCorona.getPartidosPuntos(acceptTeam, (response)->{
            if(response.isSuccessful()){
                HistoriUser history = response.body();
                if(history != null && history.getPoints() != null){
                    if(history.getPoints().getTotal() != null ) {
                        List<Matche> partidos = history.getMatches();
                        if(partidos == null ) {
                            return;
                        }
                        for(Matche match:partidos){
                            if(match.getId().intValue() == partido.getId().intValue()){
                                updateHistory(String.valueOf(match.getPoints().getTotal()),null);
                                if(match.getPoints() != null && match.getPoints().getLocation() != null) {
                                    showBonoUbication(match.getPoints().getLocation());
                                }
                                break;
                            }
                        }
                        try{
                            totalScore.setText(getResources().getString(R.string.format_pts,String.valueOf(history.getPoints().getTotal())));
                        }catch (IllegalStateException ex){
                            ex.printStackTrace();
                        }
                    }
                }
            }
        });
    }

    public void showBonoUbication(CheckPointResult.PointsLocation pointsLocation){
        if(layoutPointsUbication == null || pointsUbicacion == null || pointsLocation == null) {
            return;
        }
        if(pointsLocation.points == 0 && (pointsLocation.name == null || pointsLocation.name.length()==0)){
            return;
        }
        layoutPointsUbication.setVisibility(View.VISIBLE);
        pointsUbicacion.setText(getResources().getString(R.string.format_pts, String.valueOf(pointsLocation.points)));
    }

    public File saveBitmapToFile(File file){
        try {
            // BitmapFactory options to downsize the image
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            o.inSampleSize = 8;
            // factor of downsizing the image
            FileInputStream inputStream = new FileInputStream(file);
            //Bitmap selectedBitmap = null;
            BitmapFactory.decodeStream(inputStream, null, o);
            inputStream.close();
            // The new size we want to scale to
            final int REQUIRED_SIZE=75;
            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while(o.outWidth / scale / 2 >= REQUIRED_SIZE &&
                    o.outHeight / scale / 2 >= REQUIRED_SIZE) {
                scale *= 2;
            }
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            inputStream = new FileInputStream(file);
            Bitmap selectedBitmap = BitmapFactory.decodeStream(inputStream, null, o2);
            inputStream.close();
            // here i override the original image file
            file.createNewFile();
            FileOutputStream outputStream = new FileOutputStream(file);
            selectedBitmap.compress(Bitmap.CompressFormat.JPEG, 100 , outputStream);
            return file;
        } catch (Exception e) {
            return null;
        }
    }
}
