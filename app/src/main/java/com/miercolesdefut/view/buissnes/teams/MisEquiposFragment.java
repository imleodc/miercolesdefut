package com.miercolesdefut.view.buissnes.teams;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.hardware.camera2.params.Face;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.analytics.Tracker;
import com.miercolesdefut.R;
import com.miercolesdefut.ServiceCoronaImp;
import com.miercolesdefut.logic.ServiceCorona;
import com.miercolesdefut.logic.configurations.FacebookUtils;
import com.miercolesdefut.logic.dao.Matche;
import com.miercolesdefut.logic.dao.Perfil;
import com.miercolesdefut.logic.dao.Team;
import com.miercolesdefut.logic.model.UpdaterListData;
import com.miercolesdefut.logic.model.rxbux.App;
import com.miercolesdefut.view.BaseActivity;
import com.miercolesdefut.view.BaseFragment;
import com.miercolesdefut.view.buissnes.matchs.PartidosActivosFragment;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MisEquiposFragment extends BaseFragment {
    public static final String KEY_MATCHE = "matcheKey";
    public static final String KEY_PERFIL = "perfilUserKey";

    public static final String IS_CAPITAN = "perfilCaptain";
    public static final String IS_PENDIENTE = "perfilPendiente";
    public static final String IS_INVITED = "perfilInvitado";



    RecyclerView recyclerView;
    ServiceCorona serviceCorona = new ServiceCoronaImp();
    private OnListFragmentInteractionListener mListener;
    ProgressDialog progressDialog;
    private List<Matche> partidosEnCurso = new ArrayList<>();
    Subscription subscription;
    Matche partido = null;
    Perfil perfil = null;
    boolean isMainScreen  = false;

    boolean paintCapitan;
    boolean paintPending;
    boolean paintInvited;





    public MisEquiposFragment() {
        setImageBackground(R.drawable.equipo);
    }

    public static MisEquiposFragment newInstance() {
        MisEquiposFragment fragment = new MisEquiposFragment();
        return fragment;
    }

    public static MisEquiposFragment newInstance(Matche partido) {
        MisEquiposFragment fragment = new MisEquiposFragment();
        Bundle args = new Bundle();
        args.putSerializable(KEY_MATCHE, partido);
        fragment.setArguments(args);
        return fragment;
    }

    public static MisEquiposFragment newInstance(Matche partido, boolean mainScreen) {
        MisEquiposFragment fragment = new MisEquiposFragment();
        Bundle args = new Bundle();
        args.putSerializable(KEY_MATCHE, partido);
        args.putBoolean(PartidosActivosFragment.KEY_MAIN_SCRREN, mainScreen);
        fragment.setArguments(args);

        return fragment;
    }


    public static MisEquiposFragment newInstance(Perfil perfilL) {
        MisEquiposFragment fragment = new MisEquiposFragment();
        Bundle args = new Bundle();
        args.putSerializable(KEY_PERFIL, perfilL);
        fragment.setArguments(args);
        return fragment;
    }

    public static MisEquiposFragment newInstance(Perfil perfilL, String lvlTeam) {
        MisEquiposFragment fragment = new MisEquiposFragment();
        Bundle args = new Bundle();
        args.putSerializable(KEY_PERFIL, perfilL);

        switch(lvlTeam){
            case "capitan": args.putBoolean(IS_CAPITAN, true); break;
            case "pendiente": args.putBoolean(IS_PENDIENTE, true); break;
            case "invited": args.putBoolean(IS_INVITED, true); break;
        }

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        showActionBar(getResources().getString(R.string.misEquipos));
        if(getArguments() != null){
            partido = (Matche) getArguments().getSerializable(KEY_MATCHE);
            perfil = (Perfil) getArguments().getSerializable(KEY_PERFIL);
            isMainScreen = getArguments().getBoolean(PartidosActivosFragment.KEY_MAIN_SCRREN, false);


            paintCapitan = getArguments().getBoolean(IS_CAPITAN, false);
            paintPending = getArguments().getBoolean(IS_PENDIENTE, false);
            paintInvited = getArguments().getBoolean(IS_INVITED, false);
        }
        getFragmentManager();//.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener(){});
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        showActionBar(getResources().getString(R.string.misEquipos));
        //getFragmentManager();//.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener(){});
    }

    private void loadEquipos(){

        serviceCorona.getEquipos(FacebookUtils.getId(getContext()), response -> {
            progressDialog.dismiss();

            if(response.isSuccessful()){
                List<Team> teamsL = response.body();
                if(teamsL == null || teamsL.isEmpty()){
                    showLayoutEmptyTeams();
                } else {
                    showSeleccionaEquipo();
                }
                UpdaterListData listEquipos = (UpdaterListData) recyclerView.getAdapter();
                if(listEquipos != null){
                    listEquipos.clearList();
                }

                if(paintPending){
                    List<Team> teamsList = new ArrayList<>();
                    Iterator<Team> it = teamsL.iterator();

                    while(it.hasNext()){
                        Team teamT = it.next();

                        if(teamT.isOnwerTeam(FacebookUtils.getId(getContext())) && (teamT.isPendiente() || teamT.isCompletado())){
                            teamsList.add(teamT);
                        }
                    }

                    teamsL = teamsList;
                }

                if(paintCapitan){
                    List<Team> teamsList = new ArrayList<>();
                    Iterator<Team> it = teamsL.iterator();

                    while(it.hasNext()){
                        Team teamT = it.next();

                        if(teamT.isValidForMatches()){
                            teamsList.add(teamT);
                        }
                    }

                    teamsL = teamsList;
                }

                if(paintInvited){
                    List<Team> teamsList = new ArrayList<>();
                    Iterator<Team> it = teamsL.iterator();

                    while(it.hasNext()){
                        Team teamT = it.next();

                        if(!teamT.isOnwerTeam(FacebookUtils.getId(getContext())) && (teamT.isPendiente() || teamT.isCompletado())){
                            teamsList.add(teamT);
                        }
                    }

                    teamsL = teamsList;
                }

                if(partido != null){
                    Iterator<Team> it = teamsL.iterator();
                    while(it.hasNext()){
                        Team teamT = it.next();

                        if(!teamT.isValidForMatches()){
                            it.remove();
                        }
                    }
                    if(teamsL.isEmpty()){
                        showLayoutEmptyTeams();
                    }
                }
                try {
                    ((UpdaterListData<List<Team>>) recyclerView.getAdapter()).addItemsToList(teamsL);
                }catch(NullPointerException ex){}
            } else {
                showLayoutEmptyTeams();
                /*
                try {
                    Log.i("EQUIPOS", response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                */
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_misequipos_list, container, false);
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            recyclerView = (RecyclerView) view;
            recyclerView.setLayoutManager(new LinearLayoutManager(context));


            if(!isConected()){
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage("Error en la conexión a internet, por favor inténtelo más tarde.");
                builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which){
                        //Do nothing here because we override this button later to change the close behaviour.
                        //However, we still need this because on older versions of Android unless we
                        //pass a handler the button doesn't get instantiated
                    }
                });

                final AlertDialog dialog = builder.create();
                dialog.show();
                dialog.setCancelable(false);

                Button button = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
                button.setOnClickListener(v -> {
                    if(isConected()) {
                        dialog.dismiss();
                        completeActivity();
                    }else{
                        button.setText("Reintentando");
                        Handler handler = new Handler();
                        handler.postDelayed(() -> {
                            button.setText("Aceptar");
                            if(isConected()) {
                                dialog.dismiss();
                                completeActivity();
                            }
                        }, 1000);
                    }
                });

                return view;
            }


            completeActivity();
        }
        return view;
    }

    private void completeActivity() {
        progressDialog = BaseActivity.createProgressDialog(getContext());
        progressDialog.show();
        subscription=rx.Observable.fromCallable(()->serviceCorona.getPartidos(FacebookUtils.getId(getContext())))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnCompleted(()->{
                    progressDialog.dismiss();
//                    loadEquipos();
                }).subscribe((response)->{
                    progressDialog.dismiss();
                    for(Matche partido: response){
                        if(partido.isActive()){
                            partidosEnCurso.add(partido);
                        }
                    }
                    if(partidosEnCurso.isEmpty()) {
                        recyclerView.setAdapter(new MyMisEquiposRecyclerViewAdapter(new ArrayList<>(), mListener));
                        loadEquipos();
                    } else {
                        //if(partido != null && partido.isActive()){
                        serviceCorona.getDetalleUsuario(FacebookUtils.getId(getContext()),(resposePerfil)->{
                            if(resposePerfil.isSuccessful()){
                                if(isMainScreen){
                                    MyMisEquiposRecyclerViewAdapter myMisEquiposRecyclerViewAdapter = new MyMisEquiposRecyclerViewAdapter(new ArrayList<>(), mListener, partidosEnCurso, partido, resposePerfil.body(), true);
                                    myMisEquiposRecyclerViewAdapter.setmTracker(((App)getActivity().getApplication()).getDefaultTracker());
                                    recyclerView.setAdapter(myMisEquiposRecyclerViewAdapter);
                                }else {
                                    recyclerView.setAdapter(new MyMisEquiposRecyclerViewAdapter(new ArrayList<>(), mListener, partidosEnCurso, partido, resposePerfil.body()));
                                }
                                loadEquipos();
                            }
                        });
                        //} /*else {
                            /*    recyclerView.setAdapter(new MyMisEquiposRecyclerViewAdapter(new ArrayList<>(), mListener, partidosEnCurso, partido, perfil));
                            }*/
                    }
                },(onError)->{
                    Toast.makeText(getContext(), "Error:"+onError.getMessage(),Toast.LENGTH_SHORT).show();
                });

    }

    public void showLayoutEmptyTeams(){
        View layout = getActivity().findViewById(R.id.layoutEmptyTeams);
        if(layout != null){
            layout.setVisibility(View.VISIBLE);
        }
    }

    public void showSeleccionaEquipo(){
        View layout = getActivity().findViewById(R.id.layoutSeleccionaEquipo);
        if(partido == null){
            return;
        }
        if(layout != null) {
            layout.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        if(subscription != null){
            subscription.unsubscribe();
        }
    }

    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(Team team, Matche partido, List<Matche> partidosActivos);
        void newChecking(Team team);
        void yallegueMainScreen(Team team, Matche partido, List<Matche> partidosActivos);
    }

}

