package com.miercolesdefut.view.buissnes;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.miercolesdefut.R;
import com.miercolesdefut.logic.configurations.InstruccionesMessages;
import com.miercolesdefut.logic.configurations.interfaces.Message;
import com.miercolesdefut.logic.model.rxbux.App;
import com.miercolesdefut.view.BaseFragment;
import com.miercolesdefut.view.buissnes.alerts.CoronaAlertDialog;
import com.miercolesdefut.view.initial.message.MessageFragment;
import com.jakewharton.rxbinding.support.v4.view.RxViewPager;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link InstruccionesFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link InstruccionesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
@SuppressWarnings("ResourceType")
public class InstruccionesFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static int INITIAL_ID_FRAME_LAYOUT = 10101010;

    private static final String INIT_SCREEN = "instructions";

    private OnFragmentInteractionListener mListener;

    public InstruccionesFragment() {
        setImageBackground(R.drawable.equipo);
    }

    public static InstruccionesFragment newInstance(String param1, String param2) {
        InstruccionesFragment fragment = new InstruccionesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        showActionBar(getResources().getString(R.string.instrucciones));

        Tracker mTracker = ((App) getActivity().getApplication()).getDefaultTracker();
        mTracker.setScreenName(INIT_SCREEN);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_instrucciones, container, false);

        ViewPager pager = (ViewPager) view.findViewById(R.id.commentsViewPager);
        pager.setAdapter(new InstruccionesFragment.MessagePagerAdapter(getChildFragmentManager()));

        TabLayout tabLayout = (TabLayout) view.findViewById(R.id.commentsTab);
        tabLayout.setupWithViewPager(pager);

        ImageView background= (ImageView) view.findViewById(R.id.imageActivity);
        View ubicacionesWebView = view.findViewById(R.id.ubicacionesWebView);
        ubicacionesWebView.setOnClickListener((event)->{
            DialogFragment alert = CoronaAlertDialog.newInstance(CoronaAlertDialog.CHECK_UBICACIONES);
            alert.show(getFragmentManager(), null);
        });
        RxViewPager.pageSelections(pager).subscribe((onNext)->{
            switch (onNext){
                case 0:
                    hideUbicacionesWebView(ubicacionesWebView);
                    background.setImageResource(R.drawable.instrucciones_2);
                    break;
                case 1:
                    hideUbicacionesWebView(ubicacionesWebView);
                    background.setImageResource(R.drawable.instrucciones_3);
                    break;
                case 2:
                    showUbicacionesWebView(ubicacionesWebView);
                    background.setImageResource(R.drawable.instrucciones_4);
                    break;
                case 3:
                    hideUbicacionesWebView(ubicacionesWebView);
                    background.setImageResource(R.drawable.instrucciones_5);
                    background.setPadding(0,0,0,0);
                    break;
                case 4:
                    hideUbicacionesWebView(ubicacionesWebView);
                    background.setPadding(0,0,0,120);
                    background.setImageResource(R.drawable.finstrucciones_6);
                    break;
            }
        });
        return view;
    }

    public void hideUbicacionesWebView(View ubicacionesWebView){
        if(ubicacionesWebView == null){
            return;
        }
        ubicacionesWebView.setVisibility(View.GONE);
    }

    public void showUbicacionesWebView(View ubicacionesWebView){
        if(ubicacionesWebView == null){
            return;
        }
        ubicacionesWebView.setVisibility(View.VISIBLE);
    }

    public FrameLayout addFragment(Context context, LinearLayout linearLayout,Message message) {
        FrameLayout layout = new FrameLayout(context);
        layout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1.0f));
        int id = INITIAL_ID_FRAME_LAYOUT++;
        layout.setId(id);
        linearLayout.addView(layout);
        if(message != null) {
            getFragmentManager().beginTransaction().add(id, MessageFragment.newInstance(message)).commit();
        }
        return layout;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        setVisibleImageLogoBottom(false);
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public  static class MessagePagerAdapter extends FragmentPagerAdapter {
        private InstruccionesMessages instrucciones = new InstruccionesMessages(true);

        public MessagePagerAdapter(FragmentManager fm){
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return MessageFragment.newInstance(instrucciones.getMessage(position));
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return "      ";
        }

        @Override
        public int getCount() {
            return instrucciones.getLength();
        }
    }
}
