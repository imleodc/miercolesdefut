package com.miercolesdefut.view.buissnes.historial;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.miercolesdefut.R;
import com.miercolesdefut.ServiceCoronaImp;
import com.miercolesdefut.logic.ServiceCorona;
import com.miercolesdefut.logic.configurations.FacebookUtils;
import com.miercolesdefut.logic.dao.AcceptTeam;
import com.miercolesdefut.logic.dao.HistoriUser;
import com.miercolesdefut.logic.dao.Team;
import com.miercolesdefut.logic.model.UpdaterListData;
import com.miercolesdefut.view.BaseActivity;
import com.miercolesdefut.view.BaseFragment;
import com.miercolesdefut.view.buissnes.historial.dummy.DummyContent.DummyItem;

import java.io.IOException;

import retrofit2.Response;
import rx.functions.Action1;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class HistorialFragmentList extends BaseFragment {
    ServiceCorona serviceCorona = new ServiceCoronaImp();
    RecyclerView recyclerView;
    private static final String KEY_EQUIPO = "historyEquipo";
    private Team equipo;
    private OnListFragmentInteractionListener mListener;

    public HistorialFragmentList() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static HistorialFragmentList newInstance() {
        HistorialFragmentList fragment = new HistorialFragmentList();
        return fragment;
    }

    public static HistorialFragmentList newInstance(final Team team) {
        Bundle args = new Bundle();
        HistorialFragmentList fragment = new HistorialFragmentList();;
        args.putSerializable(KEY_EQUIPO,team);
        fragment.setArguments(args);
        return fragment;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_historial_list, container, false);
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            recyclerView = (RecyclerView) view;
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            recyclerView.setAdapter(new HistorialRecyclerViewAdapter( new HistoriUser(), mListener));
        }
        if(getArguments() != null){
            equipo = (Team) getArguments().getSerializable(KEY_EQUIPO);
        }
        if(equipo != null) {
            AcceptTeam equipo = new AcceptTeam();
            equipo.setFacebook_id(FacebookUtils.getId(getContext()));
            equipo.setTeam_id(this.equipo.getId());

            if(!isConected()){
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage("Error en la conexión a internet, por favor inténtelo más tarde.");
                builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which){
                        //Do nothing here because we override this button later to change the close behaviour.
                        //However, we still need this because on older versions of Android unless we
                        //pass a handler the button doesn't get instantiated
                    }
                });

                final AlertDialog dialog = builder.create();
                dialog.show();
                dialog.setCancelable(false);

                Button button = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
                button.setOnClickListener(v -> {
                    if(isConected()) {
                        ProgressDialog progress = BaseActivity.createProgressDialog(getContext());
                        progress.show();

                        TextView historyTotalPoints = (TextView) getActivity().findViewById(R.id.puntuacionTotalEquipo);
                        View layoutScore = getActivity().findViewById(R.id.layoutScore);
                        serviceCorona.getPartidosPuntos(equipo, new Action1<Response<HistoriUser>>() {
                            @Override
                            public void call(Response<HistoriUser> historiUserResponse) {
                                progress.dismiss();
                                dialog.dismiss();
                                if(historiUserResponse.isSuccessful()){
                                    HistoriUser history = historiUserResponse.body();
                                    if(history.getMatches() == null || history.getMatches().isEmpty()){
                                        TextView historyEmpty = (TextView) getActivity().findViewById(R.id.historyEmpty);
                                        if(historyEmpty != null){
                                            historyEmpty.setVisibility(View.VISIBLE);
                                            if(historyTotalPoints != null) {
                                                layoutScore.setVisibility(View.GONE);
                                            }
                                        }
                                    }
                                    if(history.getPoints() != null) {
                                        if(historiUserResponse != null && historyTotalPoints != null) {
                                            historyTotalPoints.setText(getResources().getString(R.string.format_pts, String.valueOf(history.getPoints().getTotal())));
                                        }
                                    }
                                    ((UpdaterListData<HistoriUser>)recyclerView.getAdapter()).addItemsToList(history);
                                } else{
                                    try {
                                        Toast.makeText(getContext(), "Error: "+historiUserResponse.errorBody().string(),Toast.LENGTH_SHORT).show();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        });
                    }else{
                        button.setText("Reintentando");
                        Handler handler = new Handler();
                        handler.postDelayed(() -> {
                            button.setText("Aceptar");
                            if(isConected()) {
                                dialog.dismiss();
                                ProgressDialog progress=BaseActivity.createProgressDialog(getContext());
                                progress.show();
                                TextView historyTotalPoints = (TextView) getActivity().findViewById(R.id.puntuacionTotalEquipo);
                                View layoutScore = getActivity().findViewById(R.id.layoutScore);
                                serviceCorona.getPartidosPuntos(equipo, new Action1<Response<HistoriUser>>() {
                                    @Override
                                    public void call(Response<HistoriUser> historiUserResponse) {
                                        progress.dismiss();
                                        if(historiUserResponse.isSuccessful()){
                                            HistoriUser history = historiUserResponse.body();
                                            if(history.getMatches() == null || history.getMatches().isEmpty()){
                                                TextView historyEmpty = (TextView) getActivity().findViewById(R.id.historyEmpty);
                                                if(historyEmpty != null){
                                                    historyEmpty.setVisibility(View.VISIBLE);
                                                    if(historyTotalPoints != null) {
                                                        layoutScore.setVisibility(View.GONE);
                                                    }
                                                }
                                            }
                                            if(history.getPoints() != null) {
                                                if(historiUserResponse != null && historyTotalPoints != null) {
                                                    historyTotalPoints.setText(getResources().getString(R.string.format_pts, String.valueOf(history.getPoints().getTotal())));
                                                }
                                            }
                                            ((UpdaterListData<HistoriUser>)recyclerView.getAdapter()).addItemsToList(history);
                                        } else{
                                            try {
                                                Toast.makeText(getContext(), "Error: "+historiUserResponse.errorBody().string(),Toast.LENGTH_SHORT).show();
                                            } catch (IOException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }
                                });
                            }
                        }, 1000);
                    }
                });

                return view;
            }

            ProgressDialog progress=BaseActivity.createProgressDialog(getContext());
            progress.show();
            TextView historyTotalPoints = (TextView) getActivity().findViewById(R.id.puntuacionTotalEquipo);
            View layoutScore = getActivity().findViewById(R.id.layoutScore);
            serviceCorona.getPartidosPuntos(equipo, new Action1<Response<HistoriUser>>() {
                @Override
                public void call(Response<HistoriUser> historiUserResponse) {
                    progress.dismiss();
                    if(historiUserResponse.isSuccessful()){
                        HistoriUser history = historiUserResponse.body();
                        if(history.getMatches() == null || history.getMatches().isEmpty()){
                            TextView historyEmpty = (TextView) getActivity().findViewById(R.id.historyEmpty);
                            if(historyEmpty != null){
                                historyEmpty.setVisibility(View.VISIBLE);
                                if(historyTotalPoints != null) {
                                    layoutScore.setVisibility(View.GONE);
                                }
                            }
                        }
                        if(history.getPoints() != null) {
                            if(historiUserResponse != null && historyTotalPoints != null) {
                                historyTotalPoints.setText(getResources().getString(R.string.format_pts, String.valueOf(history.getPoints().getTotal())));
                            }
                        }
                        ((UpdaterListData<HistoriUser>)recyclerView.getAdapter()).addItemsToList(history);
                    } else{
                        try {
                            Toast.makeText(getContext(), "Error: "+historiUserResponse.errorBody().string(),Toast.LENGTH_SHORT).show();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(DummyItem item);
    }
}
