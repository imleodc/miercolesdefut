package com.miercolesdefut.view.buissnes.teams;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.miercolesdefut.R;
import com.miercolesdefut.logic.ConstantsApplication;
import com.miercolesdefut.logic.configurations.FacebookUtils;
import com.miercolesdefut.logic.dao.CheckedTeams;
import com.miercolesdefut.logic.dao.DetailMatchPerfil;
import com.miercolesdefut.logic.dao.Matche;
import com.miercolesdefut.logic.dao.Perfil;
import com.miercolesdefut.logic.dao.Team;
import com.miercolesdefut.logic.model.UpdaterListData;
import com.miercolesdefut.view.buissnes.teams.MisEquiposFragment.OnListFragmentInteractionListener;

import java.util.ArrayList;
import java.util.List;

public class MyMisEquiposRecyclerViewAdapter extends RecyclerView.Adapter<MyMisEquiposRecyclerViewAdapter.ViewHolder> implements UpdaterListData<List<Team>>{

    private final List<Team> equipos;
    private final OnListFragmentInteractionListener mListener;
    private List<Matche> currents;
    private boolean playing = false;
    private Matche partido;
    private Perfil perfil;
    private List<Team> teamsCheckeds;
    boolean mainScreen = false;

    private String lvl_team;

    private Tracker mTracker;

    public void setmTracker(Tracker mTracker) {
        this.mTracker = mTracker;
    }

    public MyMisEquiposRecyclerViewAdapter(List<Team> items, OnListFragmentInteractionListener listener) {
        equipos = items;
        mListener = listener;
    }

    public MyMisEquiposRecyclerViewAdapter(List<Team> items, OnListFragmentInteractionListener listner, List<Matche> currents){
        this.equipos = items;
        this.mListener = listner;
        this.currents = currents;
        if(!currents.isEmpty()){
            playing = true;
        }
    }
    public MyMisEquiposRecyclerViewAdapter(List<Team> items, OnListFragmentInteractionListener listner, List<Matche> currents, Matche partido){
        this(items, listner, currents);
        this.partido = partido;
    }

    public MyMisEquiposRecyclerViewAdapter(List<Team> items, OnListFragmentInteractionListener listner, List<Matche> currents, Matche partido, Perfil perfil){
        this(items, listner, currents, partido);
        this.perfil = perfil;
        if(perfil != null){
            teamsCheckeds = getCheckedTeams(perfil);
        }
    }

    public MyMisEquiposRecyclerViewAdapter(List<Team> items, OnListFragmentInteractionListener listner, List<Matche> currents, Matche partido, Perfil perfil, boolean mainScreen){
        this(items, listner, currents, partido, perfil);
        this.mainScreen = mainScreen;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_misequipos, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.equipo = equipos.get(position);
        holder.nombreEquipo.setText(holder.equipo.getName());
        if(holder.equipo.isPendiente()){
            configViewHolderPendiente(holder);
        } else {
            if(holder.equipo.isCompletado()){
                configViewHolderCompletado(holder);
            } else {
                if(perfil != null && (perfil.teamHasCheckedUser(holder.equipo)||(perfil.hasMakeCheck()&&perfil.getCheckedTeam().getId() == holder.equipo.getId()))){
                    configViewHolderJuntos(holder);
                } else {
                    if(currents != null && !currents.isEmpty()){
                        configViewHolderCanChecking(holder);
                    }else {
                        configViewHolderCompleto(holder);
                    }
                }
            }
        }
        holder.mView.setOnClickListener((v)->{

            if (null != mListener) {

                if(mTracker != null){
                    mTracker.send(new HitBuilders.EventBuilder().setCategory("button").setAction("touch").setLabel("teams").setValue(holder.equipo.getId()).build());
                }

                if(mainScreen && holder.equipo.isValidForMatches()){
                    if(ConstantsApplication.getInstances().isMakeChecking()){
                        mListener.onListFragmentInteraction(holder.equipo, partido,currents);
                    } else {
                        mListener.yallegueMainScreen(holder.equipo, partido, currents);
                    }
                    return;
                }

                if(perfil == null){
                    mListener.onListFragmentInteraction(holder.equipo,partido,currents);
                } else {
                    if(perfil.hasMakeCheck() || (perfil.usersHasChecked())){//(perfil.getCheckedTeamUser().getTeam().getId()==holder.equipo.getId())){
                        if(perfil.hasMakeCheck() && (perfil.getCheckedTeamUser().getTeam().getId()==holder.equipo.getId())){
                            Matche matche = perfil.getMatchCheckedTeam();
                            mListener.onListFragmentInteraction(holder.equipo, matche, currents);
                        } else {
                            if(perfil.teamHasCheckedUser(holder.equipo)){
                                DetailMatchPerfil matchPerfil = perfil.getCheckedTeamUsers(holder.equipo);
                                if(matchPerfil != null){
                                    mListener.onListFragmentInteraction(holder.equipo, matchPerfil.getMatch(),currents);
                                }
                            }else {
                                mListener.onListFragmentInteraction(holder.equipo, partido, currents);
                            }
                        }
                    } else {
                        if(partido == null){
                            if(holder.equipo.isValidForMatches()){
                                mListener.newChecking(holder.equipo);
                            } else {
                                mListener.onListFragmentInteraction(holder.equipo, partido, currents);
                            }
                        } else {
                            mListener.onListFragmentInteraction(holder.equipo, partido, currents);
                        }
                    }
                }
            }
        });
    }

    private boolean isTeamWithChecks(Team team){
        if(teamsCheckeds == null || teamsCheckeds.isEmpty()){
            return false;
        }
        for(Team teamCheck : teamsCheckeds){
            if(teamCheck.getId() == team.getId()){
                return true;
            }
        }
        return false;
    }

    private Matche getPartidoByTeam(Team team){
        if(perfil == null || perfil.getMatches() == null || perfil.getMatches().isEmpty()){
            return null;
        }

        for(DetailMatchPerfil matchPerfil: perfil.getMatches()){
            for(CheckedTeams teamf:matchPerfil.getTeams()){
                if(teamf.getTeam().getId() == team.getId() && teamf.hasCheckedMembers()){
                    return matchPerfil.getMatch();
                }
            }
        }
        return null;
    }

    private List<Team> getCheckedTeams(Perfil perfilL){
        List<DetailMatchPerfil> details = perfilL.getMatches();
        List<Team> teams = new ArrayList<>();
        if(details == null || details.isEmpty()) {
            return teams;
        }
        for(DetailMatchPerfil detail: details){
            List<CheckedTeams> checkedTeams= detail.getTeams();
            if(checkedTeams != null) {
                for(CheckedTeams checkedTeam: checkedTeams){
                    if(checkedTeam.hasCheckedMembers()){
                        teams.add(checkedTeam.getTeam());
                    }
                }
            }
        }
        return teams;
    }

    private boolean hayPartido() {
        return playing;
    }

    protected void configViewHolderCanChecking(ViewHolder holder) {
        configViewHolder(holder, holder.mView.getResources().getColor(R.color.color_text_fichajes), holder.mView.getResources().getColor(android.R.color.white), -1, "", true);
    }

    protected void configViewHolderCompleto(ViewHolder holder) {
        configViewHolder(holder, holder.mView.getResources().getColor(R.color.color_text_fichajes), holder.mView.getResources().getColor(android.R.color.white), -1, "", true);
    }

    protected void configViewHolderJuntos(ViewHolder holder) {
        int counter = holder.equipo.getMembers().size();
        String aux = String.valueOf(counter) + " ";
        aux += (counter==1) ? holder.mView.getResources().getString(R.string.personas_reunidas_sing):holder.mView.getResources().getString(R.string.personas_reunidas);
        configViewHolder(holder, holder.mView.getResources().getColor(R.color.color_text_fichajes), holder.mView.getResources().getColor(R.color.color_background_title), -1, aux, true);
    }

    protected  void configViewHolderCompletado(ViewHolder holder){
        int counter = holder.equipo.getMembers().size();
        String aux = String.valueOf(counter) + " ";
        aux += (counter==1) ? holder.mView.getResources().getString(R.string.suff_mis_equipos_sing):holder.mView.getResources().getString(R.string.suff_mis_equipos);
        configViewHolder(holder, holder.mView.getResources().getColor(R.color.pendiente),-1, -1, aux, false);
    }


    protected void configViewHolderPendiente(ViewHolder holder){
        int counter = holder.equipo.getMembers().size();
        String aux = String.valueOf(counter) + " ";
        aux += (counter==1) ? holder.mView.getResources().getString(R.string.suff_mis_equipos_sing):holder.mView.getResources().getString(R.string.suff_mis_equipos);
        configViewHolder(holder, holder.mView.getResources().getColor(R.color.pendiente),-1, -1, aux, false);
    }


    protected void configViewHolder(ViewHolder holder, int leftColor, int fillColor, int leftIcono, String status, boolean captainAvailable){
        holder.itemImageFriend.setBackgroundColor(leftColor);
        if(fillColor != -1) {
            holder.mView.setBackgroundColor(fillColor);
        }
        holder.ahoraJuntos.setText(status);
        holder.ahoraJuntos.setTextColor(leftColor);
        if(leftIcono != -1){
            holder.ahoraJuntos.setCompoundDrawablesWithIntrinsicBounds(leftIcono,0,0,0);
        }

        if(!captainAvailable){
            holder.markCapitan.setVisibility(View.INVISIBLE);
            return;
        }

        holder.markCapitan.setTextColor(fillColor);
        holder.markCapitan.setBackgroundColor(leftColor);
        if(holder.equipo.isOnwerTeam(FacebookUtils.getId(holder.mView.getContext()))){
            holder.markCapitan.setVisibility(View.VISIBLE);
        } else {
            holder.markCapitan.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return equipos.size();
    }

    @Override
    public void addItemsToList(List<Team> data) {
        equipos.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public void clearList() {
        equipos.clear();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView nombreEquipo;
        public final ImageView itemImageFriend;
        public final TextView ahoraJuntos;
        public final ImageView arrow;
        public final TextView markCapitan;
        public Team equipo;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            nombreEquipo = (TextView) view.findViewById(R.id.fichajeNombreEquipo);
            itemImageFriend = (ImageView) view.findViewById(R.id.itemImageFriend);
            ahoraJuntos = (TextView) view.findViewById(R.id.fichajeName);
            arrow = (ImageView) view.findViewById(R.id.imageView8);
            markCapitan = (TextView) view.findViewById(R.id.markCapitan);
        }
    }
}
