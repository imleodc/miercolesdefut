package com.miercolesdefut.view.buissnes.teams.detalle;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.miercolesdefut.R;
import com.miercolesdefut.logic.configurations.FacebookUtils;
import com.miercolesdefut.logic.dao.HolderView;
import com.miercolesdefut.logic.dao.Picture;
import com.miercolesdefut.logic.dao.Registro;
import com.miercolesdefut.logic.model.UpdaterListData;
import com.miercolesdefut.logic.model.facebook.ServiceFacebook;
import com.miercolesdefut.logic.model.facebook.ServiceFacebookImp;
import com.miercolesdefut.view.buissnes.teams.detalle.amigos.AmigosCerrarRecyclerView;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by lauro on 19/02/2017.
 */

public class AmigosRecyclerViewChecking<T extends Registro & Picture & HolderView> extends RecyclerView.Adapter<AmigosRecyclerViewChecking.ViewHolder> implements UpdaterListData<List<T>> {
    ServiceFacebook serviceFacebook = new ServiceFacebookImp();
    private final List<T> mValues;
    public AmigosRecyclerViewChecking(List<T> items) {
        mValues = items;
    }

    @Override
    public void addItemsToList(List<T> friends) {
        mValues.addAll(friends);
        notifyDataSetChanged();
    }

    @Override
    public void clearList() {
        mValues.clear();
    }

    @Override
    public AmigosRecyclerViewChecking.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_createteamfriends, parent, false);
        return new AmigosRecyclerViewChecking.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final AmigosRecyclerViewChecking.ViewHolder holder, final int position) {
        holder.cancelInvitationBtn.setVisibility(View.GONE);
        holder.friend = mValues.get(position);
        /*synchronized (holder.friend) {
            ((HolderView)holder.friend).setHolder(holder);
        }*/
        holder.invitationBtn.setVisibility(View.GONE);
        //holder.cancelInvitationBtn.setVisibility(View.GONE);
        holder.nombreFriend.setText(mValues.get(position).getFull_name());
        if(holder.friend instanceof  DetalleListaAmgigos.WrapperRegistro){
            if(((DetalleListaAmgigos.WrapperRegistro)holder.friend).getPicture() == null){
                serviceFacebook.completeFriendPicture(FacebookUtils.restoreCredentials(holder.mView.getContext()), holder.friend.getFacebook_id(), new Picture() {
                    @Override
                    public void setPicture(String url) {
                        ((DetalleListaAmgigos.WrapperRegistro)holder.friend).setPicture(url);
                        Picasso.with(holder.mView.getContext()).load(((DetalleListaAmgigos.WrapperRegistro) holder.friend).getPicture()).into(holder.imageFriend);
                    }

                    @Override
                    public String getPicture() {
                        return null;
                    }
                });
            }
            DetalleListaAmgigos.WrapperRegistro wrapper = (DetalleListaAmgigos.WrapperRegistro)holder.friend;
            if(wrapper.isChecked()){
                // check in
                holder.pinFriend.setVisibility(View.VISIBLE);
                setRowNormal(holder);
            } else {
                setRowTransparent(holder);
            }
            return ;
        }
        //setRowTransparent(holder);
    }

    public void setRowTransparent(AmigosRecyclerViewChecking.ViewHolder holder){
        holder.imageFriend.setAlpha(AmigosCerrarRecyclerView.TRANSPARENCI_ROW);
        holder.mView.setAlpha(0.3f);//getBackground().setAlpha(AmigosCerrarRecyclerView.TRANSPARENCI_ROW);
        holder.imageFriend.setBackgroundColor(Color.TRANSPARENT);
    }

    public void setRowNormal(AmigosRecyclerViewChecking.ViewHolder holder) {
        holder.imageFriend.setAlpha(255);
        holder.mView.setBackgroundColor(Color.WHITE);
    }



    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView nombreFriend;
        public final ImageView imageFriend;
        public final ImageButton cancelInvitationBtn;
        public final Button invitationBtn;
        public T friend;

        public final ImageView pinFriend;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            nombreFriend = (TextView) view.findViewById(R.id.fichajeNombreEquipo);
            imageFriend = (ImageView) view.findViewById(R.id.itemImageFriend);
            cancelInvitationBtn = (ImageButton) view.findViewById(R.id.cancelBtn);
            invitationBtn = (Button) view.findViewById(R.id.invitarBtn);

            pinFriend = (ImageView) view.findViewById(R.id.itemPinFriend);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + nombreFriend.getText() + "'";
        }

        public void updatePicture() {
            Picasso.with(mView.getContext()).load(friend.getPicture()).into(imageFriend);
        }
    }
}
