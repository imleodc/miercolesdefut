package com.miercolesdefut.view.buissnes.teams.detalle;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.miercolesdefut.R;
import com.miercolesdefut.ServiceCoronaImp;
import com.miercolesdefut.logic.ServiceCorona;
import com.miercolesdefut.logic.configurations.FacebookUtils;
import com.miercolesdefut.logic.dao.AcceptTeam;
import com.miercolesdefut.logic.dao.HistoriUser;
import com.miercolesdefut.logic.dao.Registro;
import com.miercolesdefut.logic.dao.Team;
import com.miercolesdefut.logic.model.rxbux.App;
import com.miercolesdefut.view.BaseActivity;
import com.miercolesdefut.view.BaseFragment;
import com.miercolesdefut.view.buissnes.historial.HistorialFragment;
import com.jakewharton.rxbinding.view.RxView;

import java.util.Collections;
import java.util.List;

import rx.Subscription;

public class DetalleEquipo extends BaseFragment {
    public static final String KEY_TEAM ="teamKey";
    private TextView nombre;
    private Team equipo;
    Subscription subscription;
    ServiceCorona serviceCorona = new ServiceCoronaImp();

    private OnFragmentInteractionListener mListener;

    private final String INIT_SCREEN = "team";

    public DetalleEquipo() {

    }

    public static DetalleEquipo newInstance(Team equipo) {
        DetalleEquipo fragment = new DetalleEquipo();
        Bundle args = new Bundle();
        args.putSerializable(KEY_TEAM, equipo);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Tracker mTracker = ((App) getActivity().getApplication()).getDefaultTracker();
        mTracker.setScreenName(INIT_SCREEN);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        if (getArguments() != null) {
            equipo = (Team) getArguments().getSerializable(KEY_TEAM);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        showActionBar(getResources().getString(R.string.misEquipos));
        View view = inflater.inflate(R.layout.fragment_detalle_equipo2, container, false);
        nombre = (TextView) view.findViewById(R.id.teamName);
        Button historial = (Button) view.findViewById(R.id.historialBtn);
        subscription = RxView
                .clicks(historial)
                .subscribe((vacio) -> {
                    ((BaseActivity) getActivity()).change(HistorialFragment.newInstance(equipo));
                });
        if(equipo != null){
            nombre.setText(equipo.getName());
        }

        List<Registro> members = equipo.getMembers();
        Collections.sort(members, (o1, o2) -> o1.getFull_name().compareTo(o2.getFull_name()));
        getFragmentManager().beginTransaction().add(R.id.friendsCreateTeam, DetalleListaAmgigos.newInstance(members)).commit();
        setVisibleImageLogoBottom(false);
        cargarPuntuacion(view);
        return view;
    }


    private void cargarPuntuacion(View view) {
        AcceptTeam accepTeam = new AcceptTeam();
        accepTeam.setFacebook_id(FacebookUtils.getId(getContext()));
        accepTeam.setTeam_id(equipo.getId());
        serviceCorona.getPartidosPuntos(accepTeam, (response)->{
            if(response.isSuccessful()){
                HistoriUser history = response.body();
                TextView puntuacion = (TextView)view.findViewById(R.id.puntuacionEquipoDetalle);
                if(puntuacion != null) {
                    if(getContext() == null){
                        return;
                    }
                    String pntStr = getContext().getResources().getString(R.string.pts);
                    puntuacion.setText(String.valueOf(history.getPoints().getTotal())+" "+pntStr);
                }
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        subscription.unsubscribe();
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
