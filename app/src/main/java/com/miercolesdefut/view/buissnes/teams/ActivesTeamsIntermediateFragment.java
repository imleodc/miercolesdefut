package com.miercolesdefut.view.buissnes.teams;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.miercolesdefut.R;
import com.miercolesdefut.logic.dao.Matche;
import com.miercolesdefut.logic.dao.Team;
import com.miercolesdefut.logic.model.rxbux.App;
import com.miercolesdefut.view.buissnes.matchs.PartidosActivosFragment;

public class ActivesTeamsIntermediateFragment extends Fragment {

    private boolean mainScreen  = false;
    private Team team;

    private final String INIT_SCREEN = "selectTeam";


    public static ActivesTeamsIntermediateFragment newInstance() {
        ActivesTeamsIntermediateFragment fragment = new ActivesTeamsIntermediateFragment();
        return fragment;
    }

    public static ActivesTeamsIntermediateFragment newInstance(Matche partido) {
        ActivesTeamsIntermediateFragment fragment = new ActivesTeamsIntermediateFragment();
        Bundle args = new Bundle();
        args.putSerializable(MisEquiposFragment.KEY_MATCHE, partido);
        fragment.setArguments(args);
        return fragment;
    }

    public static ActivesTeamsIntermediateFragment newInstance(Matche partido, Team team, boolean mainScreen) {
        ActivesTeamsIntermediateFragment fragment = new ActivesTeamsIntermediateFragment();
        Bundle args = new Bundle();
        args.putSerializable(MisEquiposFragment.KEY_MATCHE, partido);
        args.putBoolean(PartidosActivosFragment.KEY_MAIN_SCRREN, mainScreen);
        args.putSerializable(PartidosActivosFragment.KEY_TEAM, team);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Tracker mTracker = ((App) getActivity().getApplication()).getDefaultTracker();
        mTracker.setScreenName(INIT_SCREEN);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_actives_teams_intermediate, container, false);
        Matche matche = null;
        if(getArguments() != null){
            matche = (Matche) getArguments().getSerializable(MisEquiposFragment.KEY_MATCHE);
            mainScreen = getArguments().getBoolean(PartidosActivosFragment.KEY_MAIN_SCRREN, false);
            team = (Team) getArguments().getSerializable(PartidosActivosFragment.KEY_TEAM);
        }

        if(mainScreen){
            getFragmentManager().beginTransaction().replace(R.id.layoutActivesTeams, MisEquiposFragment.newInstance(matche, true)).commit();
        } else {
            getFragmentManager().beginTransaction().replace(R.id.layoutActivesTeams, MisEquiposFragment.newInstance(matche)).commit();
        }
        return view;
    }
}
