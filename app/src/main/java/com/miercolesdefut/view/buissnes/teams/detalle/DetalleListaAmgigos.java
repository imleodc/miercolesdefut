package com.miercolesdefut.view.buissnes.teams.detalle;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.miercolesdefut.R;
import com.miercolesdefut.ServiceCoronaImp;
import com.miercolesdefut.logic.ServiceCorona;
import com.miercolesdefut.logic.configurations.FacebookUtils;
import com.miercolesdefut.logic.dao.HolderView;
import com.miercolesdefut.logic.dao.Picture;
import com.miercolesdefut.logic.dao.Registro;
import com.miercolesdefut.logic.model.UpdaterListData;
import com.miercolesdefut.logic.model.facebook.ServiceFacebook;
import com.miercolesdefut.logic.model.facebook.ServiceFacebookImp;
import com.miercolesdefut.view.BaseFragment;
import com.facebook.AccessToken;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by lauro on 24/01/2017.
 */

public class DetalleListaAmgigos extends BaseFragment {
    public static final String KEY_LISTA_AMIGOS = "listaAMigosEquipo";
    public static final String KEY_VIEW_CHECKING = "listaAMigosEquipoChecking";
    public static final String KEY_LISTA_AMIGOS_CHECKED ="listaAMigosEquipoChecked";
    protected ServiceFacebook serviceFacebook = new ServiceFacebookImp();
    protected ServiceCorona serviceCorona = new ServiceCoronaImp();
    protected RecyclerView recyclerView;
    protected boolean checkinView = false;


    public static DetalleListaAmgigos newInstance(List<Registro> amigos) {
        Bundle args = new Bundle();
        DetalleListaAmgigos fragment = new DetalleListaAmgigos();
        args.putSerializable(KEY_LISTA_AMIGOS, (ArrayList)amigos);
        fragment.setArguments(args);
        return fragment;
    }

    public static DetalleListaAmgigos newInstance(List<Registro> amigos, boolean checkinView) {
        Bundle args = new Bundle();
        DetalleListaAmgigos fragment = new DetalleListaAmgigos();
        args.putSerializable(KEY_LISTA_AMIGOS, (ArrayList)amigos);
        args.putBoolean(KEY_VIEW_CHECKING, checkinView);
        fragment.setArguments(args);
        return fragment;
    }

    public static DetalleListaAmgigos newInstance(List<Registro> amigos, List<Registro> checked) {
        Bundle args = new Bundle();
        DetalleListaAmgigos fragment = new DetalleListaAmgigos();
        args.putSerializable(KEY_LISTA_AMIGOS, (ArrayList)amigos);
        args.putSerializable(KEY_LISTA_AMIGOS_CHECKED, (ArrayList)checked);
        fragment.setArguments(args);
        return fragment;
    }

    public static DetalleListaAmgigos newInstance(List<Registro> amigos, List<Registro> checked, boolean checkinView) {
        Bundle args = new Bundle();
        DetalleListaAmgigos fragment = new DetalleListaAmgigos();
        args.putSerializable(KEY_LISTA_AMIGOS, (ArrayList)amigos);
        args.putSerializable(KEY_LISTA_AMIGOS_CHECKED, (ArrayList)checked);
        args.putBoolean(KEY_VIEW_CHECKING, checkinView);
        fragment.setArguments(args);
        return fragment;
    }


    protected void init(){
        if(getArguments() != null){
            List<WrapperRegistro> temp = new ArrayList<>();
            AccessToken token = FacebookUtils.restoreCredentials(getContext());
            for(Registro registro: (List<Registro>)getArguments().getSerializable(KEY_LISTA_AMIGOS)){
                List<Registro> checked = (List<Registro>) getArguments().getSerializable(KEY_LISTA_AMIGOS_CHECKED);
                if(checked == null){
                    checked = new ArrayList<>();
                }
                WrapperRegistro wrapper = new WrapperRegistro();
                wrapper.setChecked(containt(checked,registro.getFacebook_id()));
                wrapper.setFacebook_id(registro.getFacebook_id());
                wrapper.setFull_name(registro.getFull_name());
                temp.add(wrapper);
            }
            Collections.sort(temp, (o1, o2) -> o1.getFull_name().compareTo(o2.getFull_name()));
            ((UpdaterListData<List<WrapperRegistro>>)recyclerView.getAdapter()).addItemsToList(temp);
        }
    }

    protected boolean containt(List<Registro> source, String id){
        for(Registro registro:source){
            if(registro.getFacebook_id().equals(id)){
                return true;
            }
        }
        return false;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null){
            checkinView = getArguments().getBoolean(KEY_VIEW_CHECKING, false);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_createteamfriends_list, container, false);
        if (view instanceof RecyclerView) {
            initRecyclerView(view);
        }
        init();
        return view;
    }

    protected void initRecyclerView(View view){
        Context context = view.getContext();
        recyclerView = (RecyclerView) view;
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        if(checkinView){
            recyclerView.setAdapter(new AmigosRecyclerViewChecking(new ArrayList<WrapperRegistro>()));
        } else {
            recyclerView.setAdapter(new AmigosRecyclerViewAdapter(new ArrayList<WrapperRegistro>()));
        }
    }


    public class WrapperRegistro extends Registro implements Picture,HolderView{
        RecyclerView.ViewHolder holder;
        String picture;
        boolean checked = false;
        @Override
        public void setHolder(RecyclerView.ViewHolder holder) {
            this.holder = holder;
        }

        @Override
        public void setPicture(String url) {
            this.picture = url;
            if(holder != null) {
                ((AmigosRecyclerViewAdapter.ViewHolder) holder).updatePicture();
            }
        }

        public void setChecked(boolean checked){
            this.checked = checked;
        }

        public boolean isChecked(){
            return checked;
        }

        @Override
        public String getPicture() {
            return picture;
        }
    }

}
