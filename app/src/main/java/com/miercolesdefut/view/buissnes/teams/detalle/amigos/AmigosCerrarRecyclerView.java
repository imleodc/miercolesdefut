package com.miercolesdefut.view.buissnes.teams.detalle.amigos;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.miercolesdefut.R;
import com.miercolesdefut.ServiceCoronaImp;
import com.miercolesdefut.Utils;
import com.miercolesdefut.logic.ServiceCorona;
import com.miercolesdefut.logic.configurations.FacebookUtils;
import com.miercolesdefut.logic.dao.FriendDAO;
import com.miercolesdefut.logic.dao.InvitarAEquipo;
import com.miercolesdefut.logic.dao.StandarError;
import com.miercolesdefut.logic.dao.Team;
import com.miercolesdefut.logic.model.UpdaterListData;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.List;

public class AmigosCerrarRecyclerView extends RecyclerView.Adapter<AmigosCerrarRecyclerView.ViewHolder> implements UpdaterListData<List<FriendDAO>>,View.OnClickListener{
    public static final int TRANSPARENCI_ROW = 120;
    private final List<FriendDAO> mValues;
    private final AmigosCerrarFragment.OnListFragmentInteractionListenerInvitados listenerInvitados;
    private Team equipo;
    ServiceCorona serviceCorona = new ServiceCoronaImp();

    public AmigosCerrarRecyclerView(List<FriendDAO> items, AmigosCerrarFragment.OnListFragmentInteractionListenerInvitados listener) {
        this(items, listener, null);
    }

    public AmigosCerrarRecyclerView(List<FriendDAO> items, AmigosCerrarFragment.OnListFragmentInteractionListenerInvitados listener, Team team){
        this.mValues = items;
        this.listenerInvitados = listener;
        if(team != null){
            this.equipo = team;
        } else {
            this.equipo = new Team();
        }
    }

    @Override
    public void addItemsToList(List<FriendDAO> friends) {
        if(friends == null) {
            return;
        }

        //Collections.sort(friends, (o1, o2) -> o1.getName().compareTo(o2.getName()));
        mValues.addAll(friends);
        listenerInvitados.update();
        notifyItemRangeInserted(0,friends.size());
    }

    @Override
    public void clearList() {
        mValues.clear();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_createteamfriends, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.friend = mValues.get(position);
        holder.nombreFriend.setText(mValues.get(position).getName());
        holder.updatePicture();
        {
            if(equipo.getInvitationAccepted().contains(holder.friend.getId())){
                accepted(holder);
            } else {
                if(equipo.getInvitationSended().contains(holder.friend.getId())){
                    invitado(holder);
                } else {
                    Boolean canBeInvited = equipo.getCanBeInvited().get(holder.friend.getId());
                    if(canBeInvited == null){
                        canBeInvited = true;
                    }
                    if(!canBeInvited){
                        notInvitable(holder);
                    } else {
                        invitar(holder);
                    }
                }
            }
        }
        holder.invitationBtn.setOnClickListener((event)->{
            if(equipo.getInvitationSended().contains(holder.friend.getId())){
                return;
            }
            if(listenerInvitados.addCerrarFragment(holder.friend)){
                InvitarAEquipo invitarAEquipo = new InvitarAEquipo();
                invitarAEquipo.setFacebook_id(FacebookUtils.getId(holder.mView.getContext()));
                invitarAEquipo.setTeam_id(equipo.getId());
                invitarAEquipo.setInvited(holder.friend.getId());
                serviceCorona.invitarAEquipo(invitarAEquipo, (responseInv)->{
                    if(responseInv.isSuccessful()){
                        equipo.getInvitationSended().add(holder.friend.getId());
                        notifyItemChanged(mValues.indexOf(holder.friend));
                    } else {
                        try {
                            StandarError standarError = Utils.converterError.call(responseInv.errorBody(),(((ServiceCoronaImp)serviceCorona).retrofit));
                            Toast.makeText(holder.mView.getContext(), standarError.toString(), Toast.LENGTH_SHORT).show();
                            Log.i("AMIGOS_CERRAR", responseInv.errorBody().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
                notifyItemChanged(mValues.indexOf(holder.friend));
            }
        });

        holder.cancelInvitationBtn.setVisibility(View.GONE);
        /*
        holder.invitationBtn.setOnClickListener((event)->{
            int index = mValues.indexOf(holder.friend);
            if(mValues.remove(holder.friend)){
                notifyItemRemoved(index);
            }
        });
        */

    }

    private void notInvitable(ViewHolder holder){
        setRowTransparent(holder);
        //holder.cancelInvitationBtn.setVisibility(View.GONE);
        holder.invitationBtn.setVisibility(View.GONE);
    }

    private void invitar(ViewHolder holder) {
        setRowNormal(holder);
        holder.invitationBtn.setBackgroundColor(holder.mView.getContext().getResources().getColor(R.color.color_btn));
        holder.invitationBtn.setText(R.string.agregar);
        holder.invitationBtn.getBackground().setAlpha(255);
        holder.invitationBtn.setVisibility(View.VISIBLE);
        //holder.cancelInvitationBtn.setVisibility(View.VISIBLE);
    }

    private void invitado(ViewHolder holder) {
        setRowTransparent(holder);
        holder.invitationBtn.setBackgroundColor(holder.mView.getContext().getResources().getColor(R.color.item_lista));
        holder.invitationBtn.setText(R.string.invitado);
        holder.invitationBtn.setVisibility(View.VISIBLE);
        holder.invitationBtn.getBackground().setAlpha(AmigosCerrarRecyclerView.TRANSPARENCI_ROW);
        //holder.cancelInvitationBtn.setVisibility(View.GONE);
    }

    private void accepted(ViewHolder holder) {
        setRowTransparent(holder);
        holder.invitationBtn.setBackgroundColor(holder.mView.getContext().getResources().getColor(R.color.completado));
        holder.invitationBtn.setText(R.string.aceptado);
        holder.invitationBtn.setVisibility(View.VISIBLE);
        holder.invitationBtn.getBackground().setAlpha(AmigosCerrarRecyclerView.TRANSPARENCI_ROW);
        //holder.cancelInvitationBtn.setVisibility(View.GONE);
    }

    public void setRowTransparent(ViewHolder holder){
        holder.imageFriend.setAlpha(AmigosCerrarRecyclerView.TRANSPARENCI_ROW);
        holder.mView.getBackground().setAlpha(AmigosCerrarRecyclerView.TRANSPARENCI_ROW);
        holder.imageFriend.setBackgroundColor(Color.TRANSPARENT);
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    @Override
    public void onClick(View v) {
        v.setBackgroundColor(v.getResources().getColor(R.color.item_lista));
        Log.i("buttonValue", String.valueOf(v.getId()));
    }

    public void setRowNormal(ViewHolder holder) {
        holder.imageFriend.setAlpha(255);
        holder.mView.setBackgroundColor(Color.WHITE);
        //holder.imageFriend.setBackgroundColor(Color.TRANSPARENT);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView nombreFriend;
        public final ImageView imageFriend;
        public final ImageButton cancelInvitationBtn;
        public final Button invitationBtn;
        public FriendDAO friend;


        public ViewHolder(View view) {
            super(view);
            mView = view;
            nombreFriend = (TextView) view.findViewById(R.id.fichajeNombreEquipo);
            imageFriend = (ImageView) view.findViewById(R.id.itemImageFriend);
            cancelInvitationBtn = (ImageButton) view.findViewById(R.id.cancelBtn);
            invitationBtn = (Button) view.findViewById(R.id.invitarBtn);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + nombreFriend.getText() + "'";
        }

        public void updatePicture(){
            Picasso.with(mView.getContext()).load(friend.getPicture()).into(imageFriend);
        }
    }
}
