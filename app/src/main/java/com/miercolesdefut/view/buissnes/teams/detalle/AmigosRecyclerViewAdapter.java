package com.miercolesdefut.view.buissnes.teams.detalle;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.miercolesdefut.R;
import com.miercolesdefut.logic.configurations.FacebookUtils;
import com.miercolesdefut.logic.dao.HolderView;
import com.miercolesdefut.logic.dao.Picture;
import com.miercolesdefut.logic.dao.Registro;
import com.miercolesdefut.logic.model.UpdaterListData;
import com.miercolesdefut.logic.model.facebook.ServiceFacebook;
import com.miercolesdefut.logic.model.facebook.ServiceFacebookImp;
import com.miercolesdefut.view.buissnes.teams.detalle.amigos.AmigosCerrarRecyclerView;
import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.List;

/**
 * Created by lauro on 24/01/2017.
 */

public class AmigosRecyclerViewAdapter<T extends Registro & Picture & HolderView> extends RecyclerView.Adapter<AmigosRecyclerViewAdapter.ViewHolder> implements UpdaterListData<List<T>> {
    ServiceFacebook serviceFacebook = new ServiceFacebookImp();
    private final List<T> mValues;
    public AmigosRecyclerViewAdapter(List<T> items) {
        mValues = items;
    }

    @Override
    public void addItemsToList(List<T> friends) {
        Collections.sort(friends, ((o1, o2) -> o1.getFull_name().compareTo(o2.getFull_name())));

        mValues.addAll(friends);
        notifyDataSetChanged();
    }

    @Override
    public void clearList() {
        mValues.clear();
    }

    @Override
    public AmigosRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_createteamfriends, parent, false);

        return new AmigosRecyclerViewAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final AmigosRecyclerViewAdapter.ViewHolder holder, final int position) {
        holder.friend = mValues.get(position);
        synchronized (holder.friend) {
            ((HolderView)holder.friend).setHolder(holder);
        }
        holder.invitationBtn.setVisibility(View.GONE);
        holder.cancelInvitationBtn.setVisibility(View.GONE);
        holder.nombreFriend.setText(mValues.get(position).getFull_name());
        if(holder.friend instanceof DetalleListaAmgigos.WrapperRegistro){
            DetalleListaAmgigos.WrapperRegistro registroTemp = (DetalleListaAmgigos.WrapperRegistro)holder.friend;
            if(registroTemp.getPicture() == null) {
                serviceFacebook.completeFriendPicture(FacebookUtils.restoreCredentials(holder.mView.getContext()), registroTemp.getFacebook_id(), new Picture() {
                    @Override
                    public void setPicture(String url) {
                        registroTemp.setPicture(url);
                        Picasso.with(holder.mView.getContext()).load(url).into(holder.imageFriend);
                    }

                    @Override
                    public String getPicture() {
                        return null;
                    }
                });
            }
        }
        setRowNormal(holder);

    }

    public void setRowTransparent(ViewHolder holder){
        holder.imageFriend.setAlpha(AmigosCerrarRecyclerView.TRANSPARENCI_ROW);
        holder.mView.getBackground().setAlpha(AmigosCerrarRecyclerView.TRANSPARENCI_ROW);
        holder.imageFriend.setBackgroundColor(Color.TRANSPARENT);
    }

    public void setRowNormal(ViewHolder holder) {
        holder.imageFriend.setAlpha(255);
        holder.mView.setBackgroundColor(Color.WHITE);
    }



    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView nombreFriend;
        public final ImageView imageFriend;
        public final ImageButton cancelInvitationBtn;
        public final Button invitationBtn;
        public T friend;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            nombreFriend = (TextView) view.findViewById(R.id.fichajeNombreEquipo);
            imageFriend = (ImageView) view.findViewById(R.id.itemImageFriend);
            cancelInvitationBtn = (ImageButton) view.findViewById(R.id.cancelBtn);
            invitationBtn = (Button) view.findViewById(R.id.invitarBtn);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + nombreFriend.getText() + "'";
        }

        public void updatePicture() {
            Picasso.with(mView.getContext()).load(friend.getPicture()).into(imageFriend);
        }
    }
}
