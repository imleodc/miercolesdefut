package com.miercolesdefut.view.buissnes.historial;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.miercolesdefut.R;
import com.miercolesdefut.logic.dao.Team;
import com.miercolesdefut.logic.model.rxbux.App;
import com.miercolesdefut.view.BaseFragment;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link HistorialFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link HistorialFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HistorialFragment extends BaseFragment {

    public static final String KEY_TEAM_HISTORY = "keyTeamHistory";
    Team equipo;

    private OnFragmentInteractionListener mListener;

    public HistorialFragment() {
         setImageBackground(R.drawable.historial);
    }

    // TODO: Rename and change types and number of parameters
    public static HistorialFragment newInstance() {
        HistorialFragment fragment = new HistorialFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public static HistorialFragment newInstance(Team team){
        HistorialFragment fragment = new HistorialFragment();
        Bundle args = new Bundle();
        args.putSerializable(KEY_TEAM_HISTORY, team);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        showActionBar(getResources().getString(R.string.historial));

        Tracker mTracker = ((App) getActivity().getApplication()).getDefaultTracker();
        mTracker.setScreenName("team/historic");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_historial, container, false);
        TextView nombreEquipo = (TextView) view.findViewById(R.id.hTeamName);
        TextView noHistory = (TextView) view.findViewById(R.id.historyEmpty);
        if(getArguments() != null) {
            equipo = (Team) getArguments().getSerializable(KEY_TEAM_HISTORY);
        }
        if(equipo != null) {
            nombreEquipo.setText(equipo.getName());
            if(noHistory != null){
                noHistory.setText(getResources().getString(R.string.sinHistorial, equipo.getName()));
            }
        }
        getFragmentManager().beginTransaction().add(R.id.historialListView, HistorialFragmentList.newInstance(equipo)).commit();
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
