package com.miercolesdefut.view.buissnes.matchs;

import android.content.Context;
import android.content.DialogInterface;
import android.content.IntentSender;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.miercolesdefut.R;
import com.miercolesdefut.ServiceCoronaImp;
import com.miercolesdefut.logic.ConstantsApplication;
import com.miercolesdefut.logic.ServiceCorona;
import com.miercolesdefut.logic.dao.Matche;
import com.miercolesdefut.logic.dao.Perfil;
import com.miercolesdefut.logic.model.rxbux.App;
import com.miercolesdefut.view.BaseFragment;
import com.miercolesdefut.view.buissnes.teams.ProxPartidosFragment;
import com.miercolesdefut.view.buissnes.teams.RegistrarEquipo;
import com.miercolesdefut.view.initial.MakeTeamsFragment;
import com.jakewharton.rxbinding.view.RxView;
import com.trello.rxlifecycle.android.FragmentEvent;

import java.util.List;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

public class InicioPartidosActivos extends BaseFragment {
    private static final String INIT_SCREEN = "home";

    private Subscription profileBus;
    private OnFragmentInteractionListener mListener;
    private Subscription subscription;

    ServiceCorona serviceCorona = new ServiceCoronaImp();

    public InicioPartidosActivos() {
    }

    public static InicioPartidosActivos newInstance(String param1, String param2) {
        InicioPartidosActivos fragment = new InicioPartidosActivos();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ConstantsApplication.getInstances().setMakeChecking(false);

        Tracker mTracker = ((App) getActivity().getApplication()).getDefaultTracker();
        mTracker.setScreenName(INIT_SCREEN);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        createLocationRequest();
    }

    protected void createLocationRequest() {
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        GoogleApiClient mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {

                    @Override
                    public void onConnected(@Nullable Bundle bundle) {}

                    @Override
                    public void onConnectionSuspended(int i) {}
                })
                .addOnConnectionFailedListener(connectionResult -> {})
                .build();

        mGoogleApiClient.connect();

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());

        builder.setAlwaysShow(true);

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result
                        .getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can
                        // initialize location
                        // requests here.
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be
                        // fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling
                            // startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(getActivity(), 1000);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have
                        // no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }

                mGoogleApiClient.disconnect();
            }
        });

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        showActionBar(getResources().getString(R.string.miercoles_de_fut));
        View view = inflater.inflate(R.layout.fragment_inicio_partidos_activos,container, false);

        if(!isConected()){
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage("Error en la conexión a internet, por favor inténtelo más tarde.");
            builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which){
                    //Do nothing here because we override this button later to change the close behaviour.
                    //However, we still need this because on older versions of Android unless we
                    //pass a handler the button doesn't get instantiated
                }
            });

            final AlertDialog dialog = builder.create();
            dialog.show();
            dialog.setCancelable(false);

            Button button = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
            button.setOnClickListener(v -> {
                if(isConected()) {
                    dialog.dismiss();
                    completeActivity(view);
                }else{
                    button.setText("Reintentando");
                    Handler handler = new Handler();
                    handler.postDelayed(() -> {
                        button.setText("Aceptar");
                        if(isConected()) {
                            dialog.dismiss();
                            completeActivity(view);
                        }
                    }, 1000);
                }
            });

            return view;
        }

        completeActivity(view);
        return view;
    }

    private void completeActivity(View view) {
        getChildFragmentManager()
                .beginTransaction()
                .add(R.id.framePartidosActivos, PartidosActivosFragment.newInstance(true))
                .add(R.id.frameProxPart, ProxPartidosFragment.newInstance(false))
                .commit();

        /***/
        Button crearEquipo = (Button) view.findViewById(R.id.crearEquipoBtn);
        RxView.clicks(crearEquipo).subscribe((vacio)->{
            ((App)getActivity().getApplication()).getDefaultTracker().send(new HitBuilders.EventBuilder().setCategory("menu").setAction("touch").setLabel("home-create").build());
            getFragmentManager().beginTransaction().addToBackStack(null).replace(R.id.content_frame, RegistrarEquipo.newInstance(), MakeTeamsFragment.TAG_REGISTRA_EQUIPO).commit();
        });
        if(!ConstantsApplication.getInstances().isCreateTeams()){
            crearEquipo.setVisibility(View.GONE);
        }

        /*serviceCorona.getPartidos((response)->{
            if(response.isSuccessful()){
                List<Matche> partidos = response.body();
                if(partidos == null || partidos.isEmpty()){
                    View messageEmpty = getActivity().findViewById(R.id.messageInicioPartidosActivos);
                    View layoutEquipos = getActivity().findViewById(R.id.scrollEquipos);
                    messageEmpty.setVisibility(View.VISIBLE);
                    layoutEquipos.setVisibility(View.GONE);
                }
            }
        });*/

        App.get().busProfile().toObservable()
                .observeOn(AndroidSchedulers.mainThread())
                .doOnUnsubscribe(()->{
                    //Log.i(TAG, "Unsubcribing subcription from onResume()");
                })
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .subscribe((perfil)->{
                    if(perfil instanceof Perfil){
                        //Log.i(TAG, ((Perfil) perfil).getEmail());
                    }
                });
        App.get().busMatch().toObservable()
                .observeOn(AndroidSchedulers.mainThread())
                .compose(this.bindUntilEvent(FragmentEvent.DESTROY_VIEW))
                .subscribe((partidosEvent)->{
                    if(partidosEvent instanceof List<?>){
                        List<Matche> partidos = (List<Matche>) partidosEvent;
                        if(partidos == null || partidos.isEmpty()){
                            View messageEmpty = getActivity().findViewById(R.id.messageInicioPartidosActivos);
                            View layoutEquipos = getActivity().findViewById(R.id.scrollEquipos);
                            messageEmpty.setVisibility(View.VISIBLE);
                            layoutEquipos.setVisibility(View.GONE);
                        }
                    }
                });
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        App.get().updateWithCache();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
