package com.miercolesdefut.view.buissnes.teams;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.miercolesdefut.R;
import com.miercolesdefut.logic.dao.Matche;
import com.miercolesdefut.logic.model.UpdaterListData;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MyProxPartidosRecyclerViewAdapter extends RecyclerView.Adapter<MyProxPartidosRecyclerViewAdapter.ViewHolder> implements UpdaterListData<List<Matche>>{

    private final List<Matche> mValues;
    String fechaPivote;

    public MyProxPartidosRecyclerViewAdapter(List<Matche> items) {
        mValues = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_proxpartidos, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.partido = mValues.get(position);
        holder.equipoANombre.setText(holder.partido.getTeam_a().getName());
        holder.equipoBNombre.setText(holder.partido.getTeam_b().getName());
        Picasso.with(holder.mView.getContext()).load(holder.partido.getTeam_a().getImage()).into(holder.equipoAImage);
        Picasso.with(holder.mView.getContext()).load(holder.partido.getTeam_b().getImage()).into(holder.equipoBImage);
        holder.horarioPartido.setText(holder.partido.getStart_time_format());
        if(fechaPivote == null || !fechaPivote.equals(holder.partido.getStart_date_format())){
            fechaPivote = holder.partido.getStart_date_format();
            holder.layoutFechaPartido.setVisibility(View.VISIBLE);
            holder.fechaPartido.setText(holder.partido.getStart_date_format().toUpperCase());
        }
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    @Override
    public void addItemsToList(List<Matche> data) {
        mValues.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public void clearList() {
        mValues.clear();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView equipoANombre;
        public final TextView equipoBNombre;
        public final ImageView equipoAImage;
        public final ImageView equipoBImage;
        public final View layoutFechaPartido;
        public final TextView fechaPartido;
        public final TextView horarioPartido;
        public Matche partido;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            equipoANombre = (TextView) view.findViewById(R.id.equipoANombre);
            equipoBNombre = (TextView) view.findViewById(R.id.equipoBNombre);
            equipoAImage = (ImageView) view.findViewById(R.id.equipoAImagen);
            equipoBImage = (ImageView) view.findViewById(R.id.equipoBImagen);
            layoutFechaPartido = view.findViewById(R.id.layoutFechaPartidoActivo);
            fechaPartido = (TextView) view.findViewById(R.id.fechaPartido);
            horarioPartido = (TextView) view.findViewById(R.id.horarioPartido);
        }
    }
}
