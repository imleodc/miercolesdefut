package com.miercolesdefut.view.buissnes.teams.detalle;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ImageSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.applinks.AppLinkData;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.miercolesdefut.R;
import com.miercolesdefut.ServiceCoronaImp;
import com.miercolesdefut.Utils;
import com.miercolesdefut.logic.ServiceCorona;
import com.miercolesdefut.logic.configurations.FacebookUtils;
import com.miercolesdefut.logic.dao.AcceptTeam;
import com.miercolesdefut.logic.dao.FriendDAO;
import com.miercolesdefut.logic.dao.StandarError;
import com.miercolesdefut.logic.dao.Team;
import com.miercolesdefut.logic.model.rxbux.App;
import com.miercolesdefut.view.BaseActivity;
import com.miercolesdefut.view.buissnes.teams.MisEquiposIntermediFragment;
import com.miercolesdefut.view.buissnes.teams.RegistrarEquipo;
import com.miercolesdefut.view.buissnes.teams.detalle.amigos.AmigosCerrarFragment;
import com.facebook.FacebookSdk;
import com.facebook.share.model.AppInviteContent;
import com.facebook.share.widget.AppInviteDialog;
import com.jakewharton.rxbinding.view.RxView;

import bolts.AppLinks;
import rx.Subscription;

public class CerrarEquipoFragment extends RegistrarEquipo implements AmigosCerrarFragment.OnListFragmentInteractionListenerInvitados {
    public static final String TAG_CERRAR_EQUIPO="tagCerrarEquipoMio";

    public static final String KEY_TEAM_COMPLETE = "keyTeamCompleteData";
    public static final String KEY_TEAM_MEMBERS_STATUS = "keyTeamStatusMembersData";
    private Team completar = null;
    private ServiceCorona serviceCorona;
    private TextView nombreEquipo;

    private final String INIT_SCREEN = "create/edit";

    public CerrarEquipoFragment() {
        serviceCorona = new ServiceCoronaImp();
        setImageBackground(R.drawable.fondo);
    }

    public static CerrarEquipoFragment newInstance(Team team){
        CerrarEquipoFragment fragment = new CerrarEquipoFragment();
        Bundle args = new Bundle();
        args.putSerializable(KEY_TEAM_COMPLETE, team);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        showActionBar(getResources().getString(R.string.misEquipos));
        Tracker mTracker = ((App) getActivity().getApplication()).getDefaultTracker();
        mTracker.setScreenName(INIT_SCREEN);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        if(getArguments() != null){
            completar = (Team) getArguments().getSerializable(KEY_TEAM_COMPLETE);
        } else {
            completar = new Team();
            completar.setNewTeam(true);
        }


        FacebookSdk.sdkInitialize(getContext());
        Uri targetUrl =
                AppLinks.getTargetUrlFromInboundIntent(getContext(), getActivity().getIntent());
        AppLinkData.fetchDeferredAppLinkData(
                getActivity(),
                new AppLinkData.CompletionHandler() {
                    @Override
                    public void onDeferredAppLinkDataFetched(AppLinkData appLinkData) {
                    }
                });

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cerrar_equipo, container, false);
        completar.setOwner(null);
        getFragmentManager()
                .beginTransaction()
                .add(R.id.friendsCreateTeam, AmigosCerrarFragment.newInstance(completar)).commit();
        initializeTextView(view);
        nombreEquipo.setText(completar.getName());
        nombreEquipo.setEnabled(false);
        configButtonCerrar(view);
        configInvitarBtn(view);
        return view;
    }

    private void initializeTextView(View view){
        nombreEquipo = (TextView)view.findViewById(R.id.nuevoEquipoName);
    }


    private void configButtonCerrar(View view){
        Button cerrarEquipo = (Button) view.findViewById(R.id.cerrarEquipoBtn);
        if(!completar.isCompletado()){
            cerrarEquipo.setEnabled(false);
            cerrarEquipo.setAlpha(0.5f);
            TextView textView = (TextView) view.findViewById(R.id.textView16);
            textView.setText(R.string.messageCerrarEquipo);
        }
        cerrarEquipo.setOnClickListener((event)->{
            ((App)getActivity().getApplication()).getDefaultTracker().send(new HitBuilders.EventBuilder().setCategory("button").setAction("touch").setLabel("create-confirm").build());
            final ProgressDialog progressDialog = BaseActivity.createProgressDialog(getContext());
            progressDialog.show();
            AcceptTeam equipo = new AcceptTeam();
            equipo.setFacebook_id(FacebookUtils.getId(getContext()));
            equipo.setTeam_id(completar.getId());
            serviceCorona.cerrarEquipo(equipo, (response)->{
                progressDialog.dismiss();
                if(response.isSuccessful()){
                    getFragmentManager().beginTransaction().addToBackStack(null).replace(R.id.content_frame, MisEquiposIntermediFragment.newInstance()).commit();
                    Toast.makeText(getContext(), "Se ha cerrado correctamente el equipo", Toast.LENGTH_LONG).show();
                } else {
                    StandarError error = Utils.converterError.call(response.errorBody(), ((ServiceCoronaImp)serviceCorona).retrofit);
                    Toast.makeText(getContext(), error.toString(), Toast.LENGTH_LONG).show();
                }
            });
        });
    }

    @Override
    public void update() {
        updateSizeView();
    }

    @Override
    public boolean addCerrarFragment(FriendDAO friend) {
        boolean complete = completar.add(friend);
        if(complete == false){
            updateSizeViewError();
        } else {
            updateSizeView();
        }
        return complete;
    }

    private int max_members = 6;

    private void updateSizeView() {
        TextView textView = (TextView) getActivity().findViewById(R.id.sizeRecyclerViewCerrar);
        int totalInv = max_members - completar.getEspacio();
        textView.setText(totalInv+"/"+max_members);
        textView.setTextColor(getContext().getResources().getColor(R.color.item_lista));
    }

    private void updateSizeViewError() {
        TextView textView = (TextView) getActivity().findViewById(R.id.sizeRecyclerViewCerrar);
        int totalInv = max_members - completar.getEspacio();
        textView.setText(totalInv+"/"+max_members);
        textView.setTextColor(getContext().getResources().getColor(R.color.pendiente));
    }

    protected void configInvitarBtn(View view){
        Button invitar = (Button) view.findViewById(R.id.invitar);
        Spannable buttonLabel = new SpannableString("i   "+getResources().getString(R.string.fb_invite));
        buttonLabel.setSpan(new ImageSpan(getContext(), R.drawable.fb, ImageSpan.ALIGN_BASELINE), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        invitar.setText(buttonLabel);

        String isOwnerAux = FacebookUtils.getId(getContext());
        if(completar != null && !completar.isOnwerTeam(isOwnerAux)){
            invitar.setVisibility(View.GONE);
            return;
        }

        Subscription subscriptionInvitar = RxView.clicks(invitar).subscribe((vacio)->{
            ((App)getActivity().getApplication()).getDefaultTracker().send(new HitBuilders.EventBuilder().setCategory("button").setAction("touch").setLabel("create-invite").setValue(Long.parseLong(isOwnerAux)).build());

            FacebookSdk.sdkInitialize(getContext());

            if(AppInviteDialog.canShow()){
                AppInviteContent.Builder content = new AppInviteContent.Builder();
                content.setApplinkUrl(getContext().getResources().getString(R.string.facebook_applink));
                content.setPreviewImageUrl("https://www.miercolesdefut.com.mx/site-content/f_invite.png");
                AppInviteContent appInviteContent = content.build();

                AppInviteDialog appInviteDialog = new AppInviteDialog(CerrarEquipoFragment.this);
                //appInviteDialog.registerCallback(callbackManager, facebookCallback);
                appInviteDialog.show(CerrarEquipoFragment.this, appInviteContent);


                /*
                AppInviteContent content = new AppInviteContent.Builder()
                        .setApplinkUrl(getContext().getResources().getString(R.string.facebook_applink))
                        .setPreviewImageUrl("https://www.miercolesdefut.com.mx/site-content/f_invite.png")
                        .build();

                AppInviteDialog.show(getActivity(), content);
                */
            }
        });
    }
}
