package com.miercolesdefut.view.buissnes.placeholder;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.miercolesdefut.R;
import com.miercolesdefut.view.BaseFragment;

public class SinDatosLista extends BaseFragment {

    public static final String KEY_MESSAGE = "keyMessageFragment";

    public static final String DEFAULT_MESSAGE =    "Sin información";

    private String message;

    public SinDatosLista() {
    }

    public static SinDatosLista newInstance() {
        SinDatosLista fragment = new SinDatosLista();
        return fragment;
    }

    public static SinDatosLista newInstance(String message) {
        SinDatosLista fragment = new SinDatosLista();
        Bundle args = new Bundle();
        args.putString(KEY_MESSAGE, message);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null) {
            message = getArguments().getString(KEY_MESSAGE,DEFAULT_MESSAGE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sin_ningun_partido, container, false);
        TextView messageFragment = (TextView) view.findViewById(R.id.messageFragment);
        if(messageFragment != null) {
            messageFragment.setText(message);
        }
        return view;
    }
}
