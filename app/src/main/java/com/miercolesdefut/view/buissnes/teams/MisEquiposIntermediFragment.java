package com.miercolesdefut.view.buissnes.teams;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.miercolesdefut.R;
import com.miercolesdefut.ServiceCoronaImp;
import com.miercolesdefut.logic.ServiceCorona;
import com.miercolesdefut.logic.configurations.FacebookUtils;
import com.miercolesdefut.logic.dao.Perfil;
import com.miercolesdefut.logic.model.rxbux.App;
import com.miercolesdefut.view.BaseFragment;
import com.miercolesdefut.view.initial.MakeTeamsFragment;
import com.jakewharton.rxbinding.view.RxView;

import rx.Subscription;

/**
 * Created by lauro on 29/01/2017.
 */

public class MisEquiposIntermediFragment extends BaseFragment {

    Subscription subscription;
    ServiceCorona serviceCorona = new ServiceCoronaImp();

    private static final String INIT_SCREEN = "myteams";

    public static MisEquiposIntermediFragment newInstance() {
        MisEquiposIntermediFragment fragment = new MisEquiposIntermediFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Tracker mTracker = ((App) getActivity().getApplication()).getDefaultTracker();
        mTracker.setScreenName(INIT_SCREEN);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_misequipos_mensaje, container, false);
        Button crearEquipo = (Button) view.findViewById(R.id.crearEquipo);
        if(crearEquipo != null){
            subscription= RxView.clicks(crearEquipo).subscribe((vacio)->{
                getFragmentManager().beginTransaction().addToBackStack(null).replace(R.id.content_frame, RegistrarEquipo.newInstance(), MakeTeamsFragment.TAG_REGISTRA_EQUIPO).commit();
            });
        }

        if(!isConected()){
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage("Error en la conexión a internet, por favor inténtelo más tarde.");
            builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which){
                    //Do nothing here because we override this button later to change the close behaviour.
                    //However, we still need this because on older versions of Android unless we
                    //pass a handler the button doesn't get instantiated
                }
            });

            final AlertDialog dialog = builder.create();
            dialog.show();
            dialog.setCancelable(false);

            Button button = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
            button.setOnClickListener(v -> {
                if(isConected()) {
                    dialog.dismiss();
                    completeActivity();
                }else{
                    button.setText("Reintentando");
                    Handler handler = new Handler();
                    handler.postDelayed(() -> {
                        button.setText("Aceptar");
                        if(isConected()) {
                            dialog.dismiss();
                            completeActivity();
                        }
                    }, 1000);
                }
            });

            return view;
        }

        completeActivity();

        return view;
    }

    private void completeActivity() {
        serviceCorona.getDetalleUsuario(FacebookUtils.getId(getContext()), (response)->{
            Perfil perfil = null;
            if(response.isSuccessful()){
                perfil = response.body();
            }
            if(getFragmentManager() == null ){
                return;
            }
        });
        getFragmentManager().beginTransaction().replace(R.id.friendListFragment_capitan, MisEquiposFragment.newInstance(null, "capitan")).commit();
        getFragmentManager().beginTransaction().replace(R.id.friendListFragment_pendiente, MisEquiposFragment.newInstance(null, "pendiente")).commit();
        getFragmentManager().beginTransaction().replace(R.id.friendListFragment_invited, MisEquiposFragment.newInstance(null, "invited")).commit();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if(subscription != null){
            subscription.unsubscribe();
        }
    }

    /*
    @Override
    public void onResume() {
        super.onResume();
        completeActivity();
        System.out.println("------------------------------------------------------------------------------------------------------------------------RESUMING ACTIVITY");
    }
    */

    /*
    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        //completeActivity();
        //onCreate(savedInstanceState);
    }*/

}
