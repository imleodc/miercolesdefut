package com.miercolesdefut.view;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ImageSpan;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.miercolesdefut.R;
import com.miercolesdefut.ServiceCoronaImp;
import com.miercolesdefut.Utils;
import com.miercolesdefut.logic.ConstantsApplication;
import com.miercolesdefut.logic.ServiceCorona;
import com.miercolesdefut.logic.ServiceUser;
import com.miercolesdefut.logic.ServiceUserImp;
import com.miercolesdefut.logic.configurations.FacebookUtils;
import com.miercolesdefut.logic.configurations.GPSLocationPermission;
import com.miercolesdefut.logic.dao.Matche;
import com.miercolesdefut.logic.dao.Perfil;
import com.miercolesdefut.logic.dao.Reference;
import com.miercolesdefut.logic.dao.StandarError;
import com.miercolesdefut.logic.dao.gps.CheckPointResult;
import com.miercolesdefut.logic.model.rxbux.App;
import com.miercolesdefut.view.buissnes.alerts.CoronaAlertDialog;
import com.miercolesdefut.view.buissnes.alerts.FichajesDialogFragment;
import com.miercolesdefut.view.buissnes.matchs.InicioPartidosActivos;
import com.google.android.gms.location.LocationRequest;
import com.pushwoosh.fragment.PushEventListener;
import com.pushwoosh.fragment.PushFragment;
import com.tbruyelle.rxpermissions.RxPermissions;
import com.trello.rxlifecycle.android.ActivityEvent;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import pl.charmas.android.reactivelocation.ReactiveLocationProvider;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MainActivity extends BaseActivity implements PushEventListener{
    ServiceUser user;
    ServiceCorona serviceCorona;
    Subscription subscription;
    BroadcastReceiver autoCheckingReceiver;
    BroadcastReceiver notificationNoConnected;
    boolean onPause = false;
    boolean showMakeTeam = false;
    boolean onSaveInstances = false;

    public static final String TAG = "MAIN_ACTIVITY";
    public static final String INIT_SCREEN = "initScreen";

    Subscription locationListener;
    private int maximaDistancia = 200;
    private List<com.miercolesdefut.logic.dao.gps.Location> locationsReferences;
    private Subscription locationReferenceUpdate;
    private RxPermissions rxPermissions;
    private int refreshInterval = 20;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /**pushwoosh notification*/
        PushFragment.init(this);
        /**RxPermission*/
        rxPermissions = new RxPermissions(this);
        /****/
        serviceCorona = new ServiceCoronaImp(getApplicationContext());
        showMakeTeam = false;
        user = new ServiceUserImp(this.getApplicationContext());
        setContentView(R.layout.activity_main);
        initMenu();
        if(!user.hasBirthday()){
            restarApp();
        }
        Bundle extras = getIntent().getExtras();
        if(extras != null){
            if(extras.containsKey(INIT_SCREEN)){
                showMakeTeam = true;
            }
        }
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        registrationNotificationPartido(savedInstanceState);
        setTranslucentStatusBar();
        customActionBar();

        if(!isConected()){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Error en la conexión a internet, por favor inténtelo más tarde.");
            builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which){
                            //Do nothing here because we override this button later to change the close behaviour.
                            //However, we still need this because on older versions of Android unless we
                            //pass a handler the button doesn't get instantiated
                        }
                    });

            final AlertDialog dialog = builder.create();
            dialog.show();
            dialog.setCancelable(false);

            Button button = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
            button.setOnClickListener(v -> {
                if(isConected()) {
                    dialog.dismiss();
                    completeActivity();
                }else{
                    button.setText("Reintentando");
                    Handler handler = new Handler();
                    handler.postDelayed(() -> {
                        button.setText("Aceptar");
                        if(isConected()) {
                            dialog.dismiss();
                            completeActivity();
                        }
                    }, 1000);
                }
            });

            return;
        }
        configVerBaseBtn();
        completeActivity();
    }

    private void configVerBaseBtn() {
        Button verBases = (Button) findViewById(R.id.btnVerBasesFooter);
        if(verBases == null){
            return;
        }
        Spannable buttonLabel = new SpannableString("A   "+getResources().getString(R.string.verBases));
        Drawable icon = getResources().getDrawable(R.drawable.arrow_normal);
        icon.setBounds(0,0,(int)(verBases.getLineHeight()/2.4), (int)(verBases.getLineHeight()/1.5));
        buttonLabel.setSpan(new ImageSpan(icon, ImageSpan.ALIGN_BASELINE), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        verBases.setText(buttonLabel);
        verBases.setOnClickListener(event->{
            showTerminos();
        });
    }


    private void completeActivity(){
        change(InicioPartidosActivos.newInstance("",""),null,true);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        //navigationView.getMenu().getItem(0).setChecked(true);

        Observable.fromCallable(()->{
            tieneFichajes();
            //scheduleCheckin();
            registerLocalReceiver();
            serviceCorona.getDetalleUsuario(FacebookUtils.getId(getApplicationContext()), (response)->{
                if(response.isSuccessful()){
                    Perfil perfil = response.body();

                    /*
                    WebView webview = (WebView) findViewById(R.id.iframe_webview);
                    if(webview!=null) {
                        String id = "12345678";
                        String html = "<iframe src=\"https://afiliacion.net/p.ashx?o=527&e=84&t=" + id + "\" height=\"1\" width=\"1\" frameborder=\"0\"></iframe>";
                        webview.loadData(html, "text/html", null);
                    }
                    */


                    if(!perfil.isCanCreateTeam()){
                        //NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
                        MenuItem menu = navigationView.getMenu().findItem(R.id.nav_make);
                        if(menu != null){
                            menu.setVisible(false);
                        }
                        ConstantsApplication.getInstances().setCreateTeams(false);
                    }
                }
            });
            return true;
        }).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doAfterTerminate(()->askPermition())
                .subscribe();
    }

    private void registerLocalReceiver() {
        IntentFilter intentFilter = new IntentFilter(CheckingRequestReceiver.PROCESS_CHECKING);
        autoCheckingReceiver =new CheckingRequestReceiver();
        registerReceiver(autoCheckingReceiver, intentFilter);

        //IntentFilter intentFilterNoInternet = new IntentFilter(NotificationNoConnected.PROCESS_DISCONECTED_NETWORK);
        //notificationNoConnected = new NotificationNoConnected();
        //registerReceiver(notificationNoConnected, intentFilterNoInternet);
    }

    private void askPermition(){
        GPSLocationPermission gpsPermission = new GPSLocationPermission();
        if(!gpsPermission.isValid(MainActivity.this)){
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},1);
        }
    }

    private void tieneFichajes() {
        serviceCorona.getInvitaciones(FacebookUtils.getId(getApplicationContext()),(invitaciones)->{
            if(invitaciones.isSuccessful()){
                if(!invitaciones.body().isEmpty()){
                    DialogFragment alert = FichajesDialogFragment.newInstance();
                    if(!onPause) {
                        alert.show(getSupportFragmentManager(), null);
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Ocurrio un error", Toast.LENGTH_SHORT);
                }
            }else {
                StandarError error = Utils.converterError.call(invitaciones.errorBody(), ((ServiceCoronaImp)serviceCorona).retrofit);
                if(error.toString().contains("facebook id es inválido")){
                    restarApp();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if(drawer != null){
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                super.onBackPressed();
            }
        }
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if(subscription != null){
            subscriptions.unsubscribe();
        }
        if(autoCheckingReceiver != null){
            unregisterReceiver(autoCheckingReceiver);
        }

        if(notificationNoConnected != null){
            unregisterReceiver(notificationNoConnected);
        }
    }

    public void scheduleCheckin(){
        if(locationListener != null){
            return;
        }
        LocationRequest request = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(1000);
        ReactiveLocationProvider locationProvider = new ReactiveLocationProvider(getApplicationContext());
        locationListener = locationProvider
                .getUpdatedLocation(request)
                .doOnUnsubscribe(()->{
                    //Log.i(TAG, "unsubcribing from LocationUpdater");
                    locationReferenceUpdate.unsubscribe();
                    locationReferenceUpdate = null;
                    locationListener = null;
                })
                .subscribe((currentLocation)->{
                    if(locationsReferences == null){
                        return;
                    }
                    for(com.miercolesdefut.logic.dao.gps.Location locationFor: locationsReferences){
                        android.location.Location referencesTeam = new android.location.Location("location");
                        referencesTeam.setLatitude(locationFor.getLatitude());
                        referencesTeam.setLongitude(locationFor.getLongitude());
                        if(currentLocation.distanceTo(referencesTeam) < maximaDistancia){
                            //Log.i(TAG,"En el Rango");
                            makeChecking(currentLocation, locationFor);
                            locationListener.unsubscribe();
                        } else {
                            //Log.i(TAG,"Fuera de rango");
                        }
                    }
                });
        locationReferenceUpdate = Observable.interval(0,3, TimeUnit.MINUTES)
                .subscribe((num)->{
                    serviceCorona.getReferenciaUsuario(FacebookUtils.getId(getApplicationContext()),(response)->{
                        if(response.isSuccessful()){
                            locationsReferences = response.body();
                        }
                    });
                });
    }

    private void makeChecking(android.location.Location currentLocation, com.miercolesdefut.logic.dao.gps.Location referenceLocation) {
        Reference reference = new Reference();
        reference.setFacebook_id(FacebookUtils.getId(getApplicationContext()));
        reference.setTeam_id(referenceLocation.getTeam().getId());
        reference.setMatch_id(referenceLocation.getMatch().getId());
        reference.setLatitude(String.valueOf(currentLocation.getLatitude()));
        reference.setLongitude(String.valueOf(currentLocation.getLongitude()));
        serviceCorona.setCheckIn(reference,checkInResponse->{
            if(checkInResponse.isSuccessful()){
                CheckPointResult ckeckPoint = checkInResponse.body();
                if(onPause){
                    return;
                }
                if(getSupportFragmentManager() != null) {
                    Map<String, String> parameters = new HashMap<>();
                    parameters.put(CoronaAlertDialog.ACTION_AUTO_CHECKING_TEAM, referenceLocation.getTeam().getName());
                    if(ckeckPoint.granted != null){
                        parameters.put(CoronaAlertDialog.ACTION_AUTO_CHECKING_POINTS, String.valueOf(ckeckPoint.granted.member));
                    } else {
                        parameters.put(CoronaAlertDialog.ACTION_AUTO_CHECKING_POINTS, String.valueOf(ckeckPoint.points.member));
                    }
                    DialogFragment alert = CoronaAlertDialog.newInstance(CoronaAlertDialog.ACTION_AUTO_CHECKING, parameters);
                    alert.show(getSupportFragmentManager(), null);
                }
                //sendBroadcast(ckeckPoint.reference.team.getName(), ckeckPoint.points.total);
                //Log.i("CheckingService", "Checking con exito.");
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        onPause = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        onPause = false;
        Observable.interval(0,refreshInterval, TimeUnit.SECONDS)
                .compose(this.bindUntilEvent(ActivityEvent.PAUSE))
                .subscribe(num->{
                    serviceCorona.getDetalleUsuario(FacebookUtils.getId(getApplicationContext()),(response)->{
                        if(response.isSuccessful()){
                            Perfil perfilBus = response.body();
                            App.get().busProfile().send(perfilBus);
                            App.get().setPerfileCache(perfilBus);
                        }
                    });
                    serviceCorona.getPartidos(FacebookUtils.getId(getApplicationContext()), (response)->{
                        if(response.isSuccessful()){
                            List<Matche> matchsBus = response.body();
                            App.get().busMatch().send(matchsBus);
                            App.get().setMatchesCache(matchsBus);
                        }
                    });
                });
        App.get().busProfile()
                .toObservable()
                .compose(this.bindUntilEvent(ActivityEvent.PAUSE))
                .subscribe((perfilObj)->{
                    Perfil perfilL = (Perfil) perfilObj;
                    int maximaDistL = perfilL.getConfig().getMemberDistance();
                    if(maximaDistL != 0){
                        maximaDistancia = maximaDistL;
                    }
                    if(!perfilL.hasMakeCheck()&&perfilL.usersHasChecked() ){
                        //Log.i(TAG, "scheduleChecking()");
                        rxPermissions.request(Manifest.permission.ACCESS_FINE_LOCATION).subscribe((granted)->{
                            if(granted){
                                scheduleCheckin();
                            }
                        });
                    }
                });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
    }

    @Override
    public void doOnRegistered(String registrationId){
        //Log.i(TAG, "Registered for pushes: " + registrationId);
    }

    @Override
    public void doOnRegisteredError(String errorId){
        //Log.e(TAG, "Failed to register for pushes: " + errorId);
    }

    @Override
    public void doOnMessageReceive(String message){
        //Log.i(TAG, "Notification opened: " + message);
    }

    @Override
    public void doOnUnregistered(final String message){
        //Log.i(TAG, "Unregistered from pushes: " + message);
    }

    @Override
    public void doOnUnregisteredError(String errorId){
        //Log.e(TAG, "Failed to unregister from pushes: " + errorId);
    }

    public class CheckingRequestReceiver extends BroadcastReceiver{
        public static final String PROCESS_CHECKING = "com.corona.processrequest.checking";

        @Override
        public void onReceive(Context context, Intent intent) {
            if(onPause){
                return;
            }
            //Log.i("CheckingService", "Se proceso el checking");
            Map<String, String> parameters = new HashMap<>();
            parameters.put(CoronaAlertDialog.ACTION_AUTO_CHECKING_TEAM, intent.getStringExtra(CoronaAlertDialog.ACTION_AUTO_CHECKING_TEAM));
            parameters.put(CoronaAlertDialog.ACTION_AUTO_CHECKING_POINTS, intent.getStringExtra(CoronaAlertDialog.ACTION_AUTO_CHECKING_POINTS));
            DialogFragment alert = CoronaAlertDialog.newInstance(CoronaAlertDialog.ACTION_AUTO_CHECKING, parameters);
            alert.show(getSupportFragmentManager(), null);
        }
    }

    public class NotificationNoConnected extends BroadcastReceiver {

        public static final String PROCESS_DISCONECTED_NETWORK = "com.corona.no.connection.to.internet";

        @Override
        public void onReceive(Context context, Intent arg1) {
            if(onPause){
                return;
            }
            DialogFragment alert = CoronaAlertDialog.newInstance(CoronaAlertDialog.ACTION_MESSAGE, "No cuenta con internet.");
            alert.show(getSupportFragmentManager(), null);

        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        PushFragment.onNewIntent(this, intent);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        onSaveInstances = false;
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        //Log.i("MAIN_ACTIVITY", "onSaveInstanceState");
        super.onSaveInstanceState(outState, outPersistentState);
        onSaveInstances = true;
        //super.onSaveInstanceState(outState, outPersistentState);
        //Log.i("MAIN_ACTIVITY", "onSaveInstanceState");
    }
}
