package com.miercolesdefut.view;

import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.IntentCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ImageSpan;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.miercolesdefut.R;
import com.miercolesdefut.ServiceCoronaImp;
import com.miercolesdefut.logic.ServiceCorona;
import com.miercolesdefut.logic.ServiceUser;
import com.miercolesdefut.logic.ServiceUserImp;
import com.miercolesdefut.logic.configurations.FacebookUtils;
import com.miercolesdefut.logic.configurations.GPSLocationPermission;
import com.miercolesdefut.logic.dao.FriendDAO;
import com.miercolesdefut.logic.dao.Matche;
import com.miercolesdefut.logic.dao.Team;
import com.miercolesdefut.logic.model.rxbux.App;
import com.miercolesdefut.view.buissnes.InstruccionesFragment;
import com.miercolesdefut.view.buissnes.alerts.CoronaAlertDialog;
import com.miercolesdefut.view.buissnes.matchs.ActivesMatchesIntermediateFragment;
import com.miercolesdefut.view.buissnes.matchs.InicioPartidosActivos;
import com.miercolesdefut.view.buissnes.matchs.PartidosActivosFragment;
import com.miercolesdefut.view.buissnes.teams.ActivesTeamsIntermediateFragment;
import com.miercolesdefut.view.buissnes.teams.CreateTeamFriendsFragment;
import com.miercolesdefut.view.buissnes.teams.MisEquiposFragment;
import com.miercolesdefut.view.buissnes.teams.MisEquiposIntermediFragment;
import com.miercolesdefut.view.buissnes.teams.RegistrarEquipo;
import com.miercolesdefut.view.buissnes.teams.detalle.CerrarEquipoFragment;
import com.miercolesdefut.view.buissnes.teams.detalle.DetalleEquipo;
import com.miercolesdefut.view.buissnes.teams.detalle.DetalleEquipoActivoFragment;
import com.miercolesdefut.view.buissnes.teams.detalle.YaLlegueFragment;
import com.miercolesdefut.view.buissnes.teams.detalle.amigos.AmigosCerrarFragment;
import com.miercolesdefut.view.initial.BirthdayActivity;
import com.miercolesdefut.view.initial.ControlsActivity;
import com.miercolesdefut.view.initial.InstruccionesActivity;
import com.miercolesdefut.view.initial.MakeTeamsFragment;
import com.miercolesdefut.view.initial.RegistrationActivity;
import com.miercolesdefut.view.user.FichajesFragment;
import com.miercolesdefut.view.user.FichajesIntermediateFragment;
import com.miercolesdefut.view.user.UserDataFragment;
import com.miercolesdefut.view.user.dummy.DummyContent;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.location.LocationRequest;
import com.jakewharton.rxbinding.support.design.widget.RxNavigationView;
import com.jakewharton.rxbinding.view.RxView;
import com.raizlabs.universalfontcomponents.UniversalFontComponents;
import com.trello.rxlifecycle.components.support.RxAppCompatActivity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import layout.ProximoPartidoIntermediateFragment;
import pl.charmas.android.reactivelocation.ReactiveLocationProvider;
import rx.Observable;
import rx.Subscription;
import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by lauro on 02/01/2017.
 */

public class BaseActivity extends RxAppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,UserDataFragment.OnFragmentInteractionListener, //AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,UserDataFragment.OnFragmentInteractionListener,
        FichajesFragment.OnListFragmentInteractionListener,
        RegistrarEquipo.OnFragmentInteractionListener,
        MisEquiposFragment.OnListFragmentInteractionListener,CreateTeamFriendsFragment.OnListFragmentInteractionListenerInvitados,
        PartidosActivosFragment.OnListFragmentInteractionListener,InicioPartidosActivos.OnFragmentInteractionListener,
        YaLlegueFragment.OnRequestYaLlegueListener, AmigosCerrarFragment.OnListFragmentInteractionListenerInvitados{

    private FragmentManager manager;
    private ImageView imageBackground;
    private ServiceUser serviceUser;
    private TextView titleBar;
    private boolean canUseGPS = false;
    private View current;
    CompositeSubscription subscriptions;
    ShareDialog shareDialog;
    ServiceCorona serviceCorona = new ServiceCoronaImp();

    protected boolean isConected(){
        ConnectivityManager conMgr = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo i = conMgr.getActiveNetworkInfo();
        if (i == null)
            return false;
        if (!i.isConnected())
            return false;
        if (!i.isAvailable())
            return false;
        return true;
    }

    public BaseActivity(){
        subscriptions = new CompositeSubscription();
    }

    CallbackManager callBackManager ;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UniversalFontComponents.init(this);
        serviceUser = new ServiceUserImp(this);
    }

    public void initMenu(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        toggle.getDrawerArrowDrawable().setColor(getResources().getColor(R.color.color_btn));
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        configListMenuItem(navigationView);
        Observable<MenuItem> menuActions = RxNavigationView.itemSelections(navigationView).share();
        Subscription menuItemAction = menuActions.subscribe((menuItem)->{
            onNavigationItemSelected(menuItem);
        });
        subscriptions.add(menuItemAction);
        Subscription logginSub = menuActions.subscribe((menuItem)->{
            //Log.i("MENU_ITEM", "Item seleccionado");
        });
        subscriptions.add(logginSub);
        Subscription menuItemChanged = menuActions.map((menuItem)->menuItem.getActionView()).filter((view)->view !=null).subscribe((actionLayout)->{
            if(current != null){
                configView((TextView) current.findViewById(R.id.menuItemText),(ImageView) current.findViewById(R.id.menuItemIcon), Color.WHITE);
            }
            current = actionLayout;
            configView((TextView) current.findViewById(R.id.menuItemText),(ImageView) current.findViewById(R.id.menuItemIcon), getResources().getColor(R.color.color_btn));
        },(error)->{
            Toast.makeText(this, error.getMessage(), Toast.LENGTH_SHORT);
        });
        subscriptions.add(menuItemChanged);
        /**Configuración menu compartir boton**/
        FacebookSdk.sdkInitialize(getApplicationContext());
        callBackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);
        shareDialog.registerCallback(callBackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
            }

            @Override
            public void onCancel() {
            }

            @Override
            public void onError(FacebookException error) {
                error.printStackTrace();
            }
        });
        Spannable buttonLabel = new SpannableString("A   "+getResources().getString(R.string.comparteLaApp));
        buttonLabel.setSpan(new ImageSpan(getApplicationContext(), R.drawable.fb, ImageSpan.ALIGN_BASELINE), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        Button fbCompartir = (Button) findViewById(R.id.fbCompartirApp);
        fbCompartir.setText(buttonLabel);
        Subscription compartir = RxView.clicks(fbCompartir).subscribe((event)->{
            if(shareDialog.canShow(ShareLinkContent.class)){
                ((App)getApplication()).getDefaultTracker().send(new HitBuilders.EventBuilder().setCategory("menu").setAction("touch").setLabel("menu-section").build());
                ShareLinkContent content = new ShareLinkContent.Builder().setContentUrl(Uri.parse(getApplicationContext().getResources().getString(R.string.link_app_android))).build();
                ShareDialog.show(this, content);
            }
            /*FacebookSdk.sdkInitialize(getApplicationContext());
            if(AppInviteDialog.canShow()) {
                AppInviteContent content = new AppInviteContent.Builder()
                        .setApplinkUrl(getApplicationContext().getResources().getString(R.string.facebook_applink))
                        .build();
                AppInviteDialog.show(this, content);
            }*/
        });
        subscriptions.add(compartir);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(callBackManager != null){
            callBackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    protected void configListMenuItem(NavigationView navigationView){
        configMenuItem(navigationView.getMenu().findItem(R.id.nav_init), R.drawable.home, R.string.inicio);
        configMenuItem(navigationView.getMenu().findItem(R.id.nav_make), R.drawable.add_team_menu, R.string.btn_make_team);
        configMenuItem(navigationView.getMenu().findItem(R.id.nav_teams), R.drawable.teams_menu, R.string.misEquipos);
        configMenuItem(navigationView.getMenu().findItem(R.id.nav_proxPartido), R.drawable.next_matches_menu, R.string.proxPartidos);
        configMenuItem(navigationView.getMenu().findItem(R.id.nav_instrucciones), R.drawable.instructions_menu, R.string.instrucciones);
        configMenuItem(navigationView.getMenu().findItem(R.id.nav_controles), R.drawable.configuracion, R.string.controles);
    }

    protected void configMenuItem(MenuItem menuItem, int image, int text){
        View actionLayout = menuItem.getActionView();
        if(actionLayout == null){
            return;
        }
        TextView textView = (TextView)actionLayout.findViewById(R.id.menuItemText);
        ImageView imageView = (ImageView)actionLayout.findViewById(R.id.menuItemIcon);
        textView.setText(text);
        imageView.setImageResource(image);
    }

    protected void configView(TextView itemText, ImageView icon, int color){
        itemText.setTextColor(color);
        icon.setColorFilter(color, PorterDuff.Mode.MULTIPLY);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id){
            case R.id.nav_init:
                change(InicioPartidosActivos.newInstance("",""));
                break;
            case R.id.nav_make:
                change(RegistrarEquipo.newInstance(),MakeTeamsFragment.TAG_REGISTRA_EQUIPO);
                break;
            case R.id.nav_signins:
                callSignins();
                break;
            case R.id.nav_proxPartido:
                callProxPartidos();
                break;
            case R.id.nav_teams:
                callMisEquipos();
                break;
            case R.id.nav_instrucciones:
                change(InstruccionesFragment.newInstance("",""));
                break;
            case R.id.nav_controles:
                change(ControlsActivity.newInstance());
                break;
        }

        Tracker mTracker = ((App) getApplication()).getDefaultTracker();
        mTracker.send(new HitBuilders.EventBuilder().setCategory("menu").setAction("touch").setLabel("menu-section").build());
        mTracker.send(new HitBuilders.EventBuilder().setCategory("menu").setAction("touch").setLabel("menu").build());

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void posicion() {
        try {
            GPSLocationPermission gpsPermission = new GPSLocationPermission();
            if(!gpsPermission.isValid(this)){
                gpsPermission.permissionAskGranted(this);
                return;
            }

            trackPosition();
        }catch (Throwable th){
            //Log.e("LOCATION", th.getMessage());
        }
    }

    private void trackPosition(){
        LocationRequest req= LocationRequest
                .create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setNumUpdates(5)
                .setInterval(100);
        ReactiveLocationProvider locationProvider = new ReactiveLocationProvider(this);
        Subscription subscription = locationProvider.getUpdatedLocation(req)
                .subscribe(new Action1<Location>() {
                    @Override
                    public void call(Location location) {
                        //Log.i("LOCATION", location.getLatitude() + "," + location.getLongitude());
                    }
                },(onError)->{onError.printStackTrace();});
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case 1:
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    canUseGPS = true;
                    trackPosition();
                }
                break;
        }
    }

    public void showTerminos(){
        Tracker mTracker = ((App) getApplication()).getDefaultTracker();
        mTracker.setScreenName("register/terms");

        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        mTracker.send(new HitBuilders.EventBuilder().setCategory("link").setAction("touch").setLabel("terms").build());
        DialogFragment alert = CoronaAlertDialog.newInstance(CoronaAlertDialog.TERMINOS_Y_CONDICIONES);
        alert.show(getSupportFragmentManager(), null);
    }

    public void showError(String error){
        DialogFragment alert = CoronaAlertDialog.newInstance(CoronaAlertDialog.ERROR_MESSAGE, error);
        alert.show(getSupportFragmentManager(),null);
    }

    public void showMessage(String message){
        DialogFragment alert = CoronaAlertDialog.newInstance(CoronaAlertDialog.ACTION_MESSAGE, message);
        alert.show(getSupportFragmentManager(),null);
    }

    private void callSignins() {
        change(FichajesIntermediateFragment.newInstance());
    }

    public void change(Fragment framgent){
        change(framgent, null);
    }

    public void change(Fragment fragment, String tag){
        change(fragment, tag, false);
    }

    public void change(Fragment fragment, String tag, boolean noBack) {
        if(manager == null){
            manager = getSupportFragmentManager();
        }
        FragmentTransaction transaction = manager.beginTransaction();
        if(!noBack){
            transaction.addToBackStack(null);
        }
        transaction.replace(R.id.content_frame, fragment, tag).commit();
    }

    protected void callMakeTeams(){
        Fragment fragment = new MakeTeamsFragment();
        change(fragment);
    }

    protected void callProxPartidos(){
        change(ProximoPartidoIntermediateFragment.newInstance());//ProxPartidosFragment.newInstance(true));
    }

    protected void callUserData(){
        callActivy(RegistrationActivity.class);
    }

    public void callMainActivity(){
        callActivy(MainActivity.class, null, Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
    }

    public void callInitMakeTeamOfMainActivity(){
        Map<String, String> parameter = new HashMap<>();
        parameter.put(MainActivity.INIT_SCREEN, "true");
        callActivy(InstruccionesActivity.class, parameter, Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
    }

    public void callInitMakeTeamOfMainActivity(boolean aux){
        Map<String, String> parameter = new HashMap<>();
        parameter.put(MainActivity.INIT_SCREEN, "true");
        callActivy(MainActivity.class, parameter, Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
    }

    protected void callBirthday(){
        callActivy(BirthdayActivity.class);
    }

    protected void restarApp(){
        serviceUser.clear();
        PackageManager packageManager = getApplicationContext().getPackageManager();
        Intent intent = packageManager.getLaunchIntentForPackage(getApplicationContext().getPackageName());
        ComponentName componentName = intent.getComponent();
        Intent mainIntent = IntentCompat.makeRestartActivityTask(componentName);
        getApplicationContext().startActivity(mainIntent);
        System.exit(0);
        //callActivy(BirthdayActivity.class, null, Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
    }

    public void callMisEquipos(){
        change(MisEquiposIntermediFragment.newInstance());//¿change(new MisEquiposFragment());
    }

    final public  void callActivy(Class klass) {
        callActivy(klass, null);
    }

    final public void callActivy(Class klass, Map<String, String> parameters){
        Intent intent = new Intent(this, klass);
        if(parameters != null){
            for(String key:parameters.keySet())
            intent.putExtra(key, parameters.get(key));
        }
        startActivity(intent);
    }

    final protected void callActivy(Class klass, Map<String, String> parameters, int flags){
        Intent intent = new Intent(this, klass);
        intent.setFlags(flags);
        if(parameters != null){
            for(String key:parameters.keySet())
                intent.putExtra(key, parameters.get(key));
        }
        startActivity(intent);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
    }

    @Override
    public void dissmisAlertDialogFichajes(DummyContent.DummyItem item) {
    }

    @Override
    public void onListFragmentInteraction(Team team,Matche partido, List<Matche> partidosActivos) {
        if(team.isPendiente()||team.isCompletado()){
            if(team.isOnwerTeam(FacebookUtils.getId(getApplicationContext()))){
                change(CerrarEquipoFragment.newInstance(team),CerrarEquipoFragment.TAG_CERRAR_EQUIPO);
            } else {
                change(RegistrarEquipo.newInstance(team), MakeTeamsFragment.TAG_REGISTRA_EQUIPO);
            }
        } else {
            if(partido == null) {
                if(partidosActivos != null && partidosActivos.size()>0 && team.isValidForMatches()){
                    change(ActivesMatchesIntermediateFragment.newInstance(team));//change(PartidosActivosFragment.newInstance(team));
                } else {
                    change(DetalleEquipo.newInstance(team));
                }
            } else {
                if(partido != null){
                    if(team.isValidForMatches()){
                        change(DetalleEquipoActivoFragment.newInstance(team, partido));
                    }
                    return;
                }
            }
        }
    }

    @Override
    public void newChecking(Team team){
        change(YaLlegueFragment.newInstance(team));
    }

    @Override
    public void yallegueMainScreen(Team team, Matche partido, List<Matche> partidosActivos) {
        change(YaLlegueFragment.newInstance(team, partido), null, false);

    }

    protected void registrationNotificationPartido(Bundle savedInstanceState){
        /*if(savedInstanceState != null){
            if(savedInstanceState.get(NotificationPartido.LAUCH_BY_NOTIFICATION) != null && savedInstanceState.getBoolean(NotificationPartido.LAUCH_BY_NOTIFICATION)){
                return;
            }
        }
        AlarmManager am = (AlarmManager) getApplicationContext().getSystemService(ALARM_SERVICE);
        Intent intent = new Intent(getApplicationContext(), TestNotificationGPS.class);
        PendingIntent piAlarmn = PendingIntent.getBroadcast(
                this,
                0,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        am.setInexactRepeating(
                AlarmManager.ELAPSED_REALTIME_WAKEUP,
                AlarmManager.INTERVAL_FIFTEEN_MINUTES/5,//AlarmManager.INTERVAL_HALF_HOUR,
                AlarmManager.INTERVAL_FIFTEEN_MINUTES/5,//AlarmManager.INTERVAL_HALF_HOUR,
                piAlarmn
        );*/
    }

    protected void setTranslucentStatusBar(){
        /*getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        if(Build.VERSION.SDK_INT >= 21) {
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }*/
        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow(); // in Activity's onCreate() for instance
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }*/
        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
    }

    protected void customActionBar(){
        //getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        LayoutInflater inflater = LayoutInflater.from(this);
        View v = inflater.inflate(R.layout.custom_action_bar, null);
        titleBar  = (TextView) v.findViewById(R.id.title_bar);
        getSupportActionBar().setCustomView(v);
    }

    protected void updateTitleBar(String title) {
        if(titleBar != null) {
            titleBar.setText(title);
        }
    }

    @Override
    public boolean add(FriendDAO friend) {
        RegistrarEquipo equipo = (RegistrarEquipo) getSupportFragmentManager().findFragmentByTag(MakeTeamsFragment.TAG_REGISTRA_EQUIPO);
        return equipo.add(friend);
    }

    @Override
    public boolean remove(FriendDAO friend) {
        RegistrarEquipo equipo = (RegistrarEquipo) getSupportFragmentManager().findFragmentByTag(MakeTeamsFragment.TAG_REGISTRA_EQUIPO);
        return equipo.remove(friend);
    }

    @Override
    public boolean containt(FriendDAO friend) {
        RegistrarEquipo equipo = (RegistrarEquipo) getSupportFragmentManager().findFragmentByTag(MakeTeamsFragment.TAG_REGISTRA_EQUIPO);
        return equipo.containt(friend);
    }

    public static ProgressDialog createProgressDialog(Context context){
        if(context != null) {
            ProgressDialog progressDialog = new ProgressDialog(context,R.style.progressDialog);
            progressDialog.setMessage(context.getResources().getString(R.string.loading));
            progressDialog.setCancelable(false);

            timerDelayRemoveDialog(10000, progressDialog);
            return progressDialog;
        }
        return null;
    }

    public static void timerDelayRemoveDialog(long time, final ProgressDialog d){
        new Handler().postDelayed(new Runnable() {
            public void run() {
                d.dismiss();
            }
        }, time);
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        subscriptions.unsubscribe();
    }

    @Override
    public void onListFragmentInteraction(Matche partido, Team equipo) {
        if(equipo != null) {
            change(DetalleEquipoActivoFragment.newInstance(equipo,partido));
        } else {
            if(partido != null){
                change(ActivesTeamsIntermediateFragment.newInstance(partido));//change(MisEquiposFragment.newInstance(partido));
            }
        }
    }

    @Override
    public void toDetalleEquipoActivo(Matche partido, Team team) {
        change(DetalleEquipoActivoFragment.newInstance(team, partido, true));
    }

    @Override
    public void onListFragmentMainScreen(Matche partido, Team team) {
        //fragmentManager.beginTransaction().replace(R.id.content_frame, ActivesTeamsIntermediateFragment.newInstance(partido, team, true)).commit();
        change(ActivesTeamsIntermediateFragment.newInstance(partido, team, true));
    }

    @Override
    public void requestMatch(Team team) {
        change(PartidosActivosFragment.newInstance(team,true));
    }

    @Override
    public void detailtViewMap(Team team, Matche matche) {
        //getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, DetalleEquipoActivoFragment.newInstance(team, matche, true)).commit();
        change(DetalleEquipoActivoFragment.newInstance(team, matche, true));
    }

    @Override
    public void update() {
        CerrarEquipoFragment cerrarEquipoFragment = (CerrarEquipoFragment) getSupportFragmentManager().findFragmentByTag(CerrarEquipoFragment.TAG_CERRAR_EQUIPO);
        if(cerrarEquipoFragment != null) {
            cerrarEquipoFragment.update();
        }
    }

    @Override
    public boolean addCerrarFragment(FriendDAO friend) {
        CerrarEquipoFragment cerrarEquipoFragment = (CerrarEquipoFragment) getSupportFragmentManager().findFragmentByTag(CerrarEquipoFragment.TAG_CERRAR_EQUIPO);
        if(cerrarEquipoFragment != null) {
            return cerrarEquipoFragment.addCerrarFragment(friend);
        }
        return false;
    }
}