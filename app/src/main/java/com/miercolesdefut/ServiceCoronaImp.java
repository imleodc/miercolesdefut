package com.miercolesdefut;

import android.content.Context;

import com.miercolesdefut.logic.ServiceCorona;
import com.miercolesdefut.logic.dao.AcceptTeam;
import com.miercolesdefut.logic.dao.DeclineTeam;
import com.miercolesdefut.logic.dao.HistoriUser;
import com.miercolesdefut.logic.dao.Invitacion;
import com.miercolesdefut.logic.dao.InvitarAEquipo;
import com.miercolesdefut.logic.dao.Invited;
import com.miercolesdefut.logic.dao.Matche;
import com.miercolesdefut.logic.dao.Perfil;
import com.miercolesdefut.logic.dao.PuntosPorCompartir;
import com.miercolesdefut.logic.dao.Reference;
import com.miercolesdefut.logic.dao.Registro;
import com.miercolesdefut.logic.dao.Team;
import com.miercolesdefut.logic.dao.TeamStatusMembers;
import com.miercolesdefut.logic.dao.gps.CheckPointResult;
import com.miercolesdefut.logic.dao.gps.Location;
import com.miercolesdefut.logic.model.CoronaAPIService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okio.Buffer;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.functions.Action1;

/**
 * Created by lauro on 23/01/2017.
 */

public class ServiceCoronaImp implements ServiceCorona{
    Context context;

    public Retrofit retrofit;
    public ServiceCoronaImp(){
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.connectTimeout(60,TimeUnit.SECONDS);
        httpClient.readTimeout(60, TimeUnit.SECONDS);
        httpClient.retryOnConnectionFailure(false);
        httpClient.addInterceptor(new Interceptor() {


            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                Request request = original.newBuilder()
                        .header("Accept","application/json")
                        .header("Authorization", " Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImQ2NDhlYTU2MmUzZTRmNDQwNDViZjE1ODAwYmFlZjM3NDc4ZjMwNmIxYjdhNDIzM2EwZTA0NzE1MzI2M2JlMTc3MDFkOTNmYjAzZTc0Mjc4In0.eyJhdWQiOiIxIiwianRpIjoiZDY0OGVhNTYyZTNlNGY0NDA0NWJmMTU4MDBiYWVmMzc0NzhmMzA2YjFiN2E0MjMzYTBlMDQ3MTUzMjYzYmUxNzcwMWQ5M2ZiMDNlNzQyNzgiLCJpYXQiOjE0ODUyMTY1NjYsIm5iZiI6MTQ4NTIxNjU2NiwiZXhwIjoxODAwNzQ5MzY2LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.ZZoNpEdIxzZ_Fb8sR4kvGPwk1PMGPdukbkRLE5o7IqSnBYDgsUNmxt80YXw7jsERRYf5GekkjXrUhjizbYera3T33CITH_4vPgsmnaxRk-vY656mV4aORXAz7FQ5XkTXJ8loPsjBZ7mbxvhtyt4WG3m9DuCXjYmB5b3ZEsQGerwJbFrRWkKHUybTxZFzoD_K56SesFFohl3ZQ71bnlcrC6kA9FiDqlUxDRQ2q_sIJzNNjJNTWmlvzo205m5Z-TKr5am5M-O-bDbi9vMoiKqSwZYAxNcuH_ggbCmIKYHwHk8hAx14YKcEO6vuHx1Icoaoknv-oF5y9PkN4BIKfKJU1FWxpqJyqyOYqJPjWUhO_Bod10SZO0j0DWrCOFMmBiRxVDTbJizDyOjg1Deywo043gvnZFgunZ9gSrAexxB2RVlE1pJSa3eTqabRUvWsFbqj6N0qD7N4keKSPIFVEGLPXYyW3h5xF4YTzyLLHb2HA0kAJAzNM4eBAyJ8YZeDgyTxox8jhi3FtfiTcm_FjS3k1Rrzgpged0m3NtGU3dygWOiSX9uxKshT7fhBEn8mmVqfyax0O4rwsWwqRTQGESg0YfePq5UG8PMQ2jkVpKPn1PbL8xp-P8vSKbOf-8Iwz_tff_X9H4Hy7SuIOL3SEnVuYmMaw-JG80UrQYsTLlT5kVA")
                        .method(original.method(), original.body())
                        .build();

                return chain.proceed(request);

            }

            private String bodyToString(final RequestBody request) {
                try {
                    final RequestBody copy = request;
                    final Buffer buffer = new Buffer();
                    if (copy != null)
                        copy.writeTo(buffer);
                    else
                        return "";
                    return buffer.readUtf8();
                } catch (final IOException e) {
                    return "did not work";
                }
            }
        });



        OkHttpClient client = httpClient.build();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

        retrofit = new Retrofit.Builder()
                //.baseUrl("http://develop.element.com.mx/corona-miercoles-de-fut/ws/public/")
                .baseUrl("https://www.miercolesdefut.com.mx/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
    }

    public ServiceCoronaImp(Context context){
        this();
        this.context = context;
    }

    @Override
    public void registroUsuario(Registro registro, final Action1<Response<ResponseBody>> action) {
        CoronaAPIService coronaAPIService = retrofit.create(CoronaAPIService.class);
        final Call<ResponseBody> call = coronaAPIService.registrar(registro);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                action.call(response);
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
                //Log.i("CORONA_API", String.valueOf(t.getMessage()));
                if(t instanceof UnknownHostException){
                    notInternet();
                }
            }
        });
    }

    @Override
    public void registraEquipo(Team equipo,final Action1<Response<ResponseBody>> action) {
        CoronaAPIService coronaAPIService = retrofit.create(CoronaAPIService.class);
        final Call<ResponseBody> call = coronaAPIService.createTeam(equipo);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                action.call(response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
                //Log.i("CORONA_API", String.valueOf(t.getMessage()));
                if(t instanceof UnknownHostException){
                    notInternet();
                }
            }
        });
    }

    @Override
    public void getInvitaciones(String idFacebook, final Action1<Response<List<Invitacion>>> action) {
        CoronaAPIService coronaAPIService = retrofit.create(CoronaAPIService.class);
        final Call<List<Invitacion>> call = coronaAPIService.getInvitacionesPendientes(idFacebook);
        call.enqueue(new Callback<List<Invitacion>>() {
            @Override
            public void onResponse(Call<List<Invitacion>> call, Response<List<Invitacion>> response) {
                action.call(response);
            }

            @Override
            public void onFailure(Call<List<Invitacion>> call, Throwable t) {
                t.printStackTrace();
                if(t instanceof UnknownHostException){
                    notInternet();
                }
            }
        });
    }

    @Override
    public void aceptarInvitacion(AcceptTeam acceptTeam, final Action1<Response<ResponseBody>> action) {
        CoronaAPIService coronaAPIService =retrofit.create(CoronaAPIService.class);
        final Call<ResponseBody> call = coronaAPIService.acceptTeam(acceptTeam);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                action.call(response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
                if(t instanceof UnknownHostException){
                    notInternet();
                }
            }
        });
    }

    @Override
    public void cancelarInvitacion(DeclineTeam declineTeam,final Action1<Response<ResponseBody>> action) {
        CoronaAPIService coronaAPIService =retrofit.create(CoronaAPIService.class);
        final Call<ResponseBody> call = coronaAPIService.declineTeam(declineTeam);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                action.call(response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
                if(t instanceof UnknownHostException){
                    notInternet();
                }
            }
        });
    }

    @Override
    public void getEquipos(String idFb, final Action1<Response<List<Team>>> action) {
        CoronaAPIService coronaAPIService = retrofit.create(CoronaAPIService.class);
        Call<List<Team>> call = coronaAPIService.getTeams(idFb);
        call.enqueue(new Callback<List<Team>>() {
            @Override
            public void onResponse(Call<List<Team>> call, Response<List<Team>> response) {
                action.call(response);
            }
            @Override
            public void onFailure(Call<List<Team>> call, Throwable t) {
                t.printStackTrace();
                if(t instanceof UnknownHostException){
                    notInternet();
                }
            }
        });
    }

    @Override
    public List<Team> getEquipos(String idFb) {
        CoronaAPIService coronaAPIService = retrofit.create(CoronaAPIService.class);
        Call<List<Team>> call = coronaAPIService.getTeams(idFb);
        try {
            Response<List<Team>> response = call.execute();
            if(response.isSuccessful()){
                return response.body();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    @Override
    public void getPartidos(final Action1<Response<List<Matche>>> action) {
        CoronaAPIService coronaAPIService = retrofit.create(CoronaAPIService.class);
        Call<List<Matche>> call = coronaAPIService.getMatches();
        call.enqueue(new Callback<List<Matche>>() {
            @Override
            public void onResponse(Call<List<Matche>> call, Response<List<Matche>> response) {
                action.call(response);
            }

            @Override
            public void onFailure(Call<List<Matche>> call, Throwable t) {
                t.printStackTrace();
                if(t instanceof UnknownHostException){
                    notInternet();
                }
            }
        });
    }

    @Override
    public void getPartidos(String facebookId, Action1<Response<List<Matche>>> action) {
        CoronaAPIService coronaAPIService = retrofit.create(CoronaAPIService.class);
        Call<List<Matche>> call = coronaAPIService.getMatches(facebookId);
        call.enqueue(new Callback<List<Matche>>() {
            @Override
            public void onResponse(Call<List<Matche>> call, Response<List<Matche>> response) {
                action.call(response);
            }

            @Override
            public void onFailure(Call<List<Matche>> call, Throwable t) {
                t.printStackTrace();
                if(t instanceof UnknownHostException){
                    notInternet();
                }
            }
        });
    }

    @Override
    public List<Matche> getPartidos() {
        CoronaAPIService coronaAPIService = retrofit.create(CoronaAPIService.class);
        Call<List<Matche>> call = coronaAPIService.getMatches();
        try {
            Response<List<Matche>> response = call.execute();
            if(response.isSuccessful()){
                return response.body();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    @Override
    public List<Matche> getPartidos(String facebookId) {
        CoronaAPIService coronaAPIService = retrofit.create(CoronaAPIService.class);
        Call<List<Matche>> call = coronaAPIService.getMatches(facebookId);
        try {
            Response<List<Matche>> response = call.execute();
            if(response.isSuccessful()){
                return response.body();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }



    @Override
    public void setCheckIn(Reference reference, final Action1<Response<CheckPointResult>> action) {
        CoronaAPIService coronaAPIService = retrofit.create(CoronaAPIService.class);
        Call<CheckPointResult> call = coronaAPIService.setCheckIn(reference);
        call.enqueue(new Callback<CheckPointResult>() {
            @Override
            public void onResponse(Call<CheckPointResult> call, Response<CheckPointResult> response) {
                action.call(response);
            }

            @Override
            public void onFailure(Call<CheckPointResult> call, Throwable t) {
                t.printStackTrace();
                if(t instanceof UnknownHostException){
                    notInternet();
                }
            }
        });
    }

    @Override
    public void setReference(Reference reference, final Action1<Response<ResponseBody>> aciton) {
        CoronaAPIService coronaAPIService = retrofit.create(CoronaAPIService.class);
        Call<ResponseBody> call = coronaAPIService.setPuntoReferencia(reference);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                aciton.call(response);
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
                if(t instanceof UnknownHostException){
                    notInternet();
                }
            }
        });
    }

    @Override
    public void getPartidosPuntos(AcceptTeam acceptTeam, final Action1<Response<HistoriUser>> action1) {
        CoronaAPIService coronaAPIService = retrofit.create(CoronaAPIService.class);
        Call<HistoriUser> call = coronaAPIService.getHistoriPoints(acceptTeam);
        call.enqueue(new Callback<HistoriUser>() {
            @Override
            public void onResponse(Call<HistoriUser> call, Response<HistoriUser> response) {
                action1.call(response);
            }

            @Override
            public void onFailure(Call<HistoriUser> call, Throwable t) {
                t.printStackTrace();
                if(t instanceof UnknownHostException){
                    notInternet();
                }
            }
        });
    }

    @Override
    public void cerrarEquipo(AcceptTeam acceptTeam, Action1<Response<ResponseBody>> action) {
        CoronaAPIService coronaAPIService = retrofit.create(CoronaAPIService.class);
        Call<ResponseBody> call = coronaAPIService.setCloseTeam(acceptTeam);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                action.call(response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
                if(t instanceof UnknownHostException){
                    notInternet();
                }
            }
        });
    }

    @Override
    public void invitarAEquipo(InvitarAEquipo invitar, Action1<Response<ResponseBody>> action) {
        CoronaAPIService coronaAPIService = retrofit.create(CoronaAPIService.class);
        Call<ResponseBody> call = coronaAPIService.inviteToTeam(invitar);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                action.call(response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
                if(t instanceof UnknownHostException){
                    notInternet();
                }
            }
        });
    }

    @Override
    public void invitacionesPendientesEquipo(int equipoId, Action1<Response<TeamStatusMembers>> action) {
        CoronaAPIService coronaAPIService = retrofit.create(CoronaAPIService.class);
        Call<TeamStatusMembers> call = coronaAPIService.pendingInvitationTeam(equipoId);
        call.enqueue(new Callback<TeamStatusMembers>() {
            @Override
            public void onResponse(Call<TeamStatusMembers> call, Response<TeamStatusMembers> response) {
                action.call(response);
            }

            @Override
            public void onFailure(Call<TeamStatusMembers> call, Throwable t) {
                t.printStackTrace();
                if(t instanceof UnknownHostException){
                    notInternet();
                }
            }
        });
    }

    @Override
    public void getDetalleUsuario(String facebookId, Action1<Response<Perfil>> action) {
        CoronaAPIService coronaAPIService = retrofit.create(CoronaAPIService.class);
        Call<Perfil> call = coronaAPIService.getProfileUser(facebookId);
        call.enqueue(new Callback<Perfil>() {
            @Override
            public void onResponse(Call<Perfil> call, Response<Perfil> response) {
                action.call(response);
            }

            @Override
            public void onFailure(Call<Perfil> call, Throwable t) {
                t.printStackTrace();
                if(t instanceof UnknownHostException){
                    notInternet();
                }
            }
        });
    }

    @Override
    public Response<Perfil> getDetalleUsuario(String facebookId) {
        CoronaAPIService coronaAPIService = retrofit.create(CoronaAPIService.class);
        Call<Perfil> call = coronaAPIService.getProfileUser(facebookId);
        try {
            return call.execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void canBeInvited(Invited invited, Action1<Response<Map<String, Boolean>>> action) {
        CoronaAPIService coronaAPIService = retrofit.create(CoronaAPIService.class);
        Call<Map<String, Boolean>> call = coronaAPIService.canBeInvited(invited);
        call.enqueue(new Callback<Map<String, Boolean>>() {
            @Override
            public void onResponse(Call<Map<String, Boolean>> call, Response<Map<String, Boolean>> response) {
                action.call(response);
            }

            @Override
            public void onFailure(Call<Map<String, Boolean>> call, Throwable t) {
                t.printStackTrace();
                if(t instanceof UnknownHostException){
                    notInternet();
                }
            }
        });
    }

    @Override
    public void getReferenciaUsuario(String facebookIs, Action1<Response<List<Location>>> action) {
        CoronaAPIService coronaAPIService = retrofit.create(CoronaAPIService.class);
        Call<List<Location>> call = coronaAPIService.getReferencesUser(facebookIs);
        call.enqueue(new Callback<List<Location>>() {
            @Override
            public void onResponse(Call<List<Location>> call, Response<List<Location>> response) {
                action.call(response);
            }

            @Override
            public void onFailure(Call<List<Location>> call, Throwable t) {
                t.printStackTrace();
                if(t instanceof UnknownHostException){
                    notInternet();
                }
            }
        });
    }

    @Override
    public void sharePhoto(String facebookId, int matchId, int teamId, double latitude, double longitude, byte[] photo, Action1<Response<PuntosPorCompartir>> action) {
        CoronaAPIService coronaAPIService = retrofit.create(CoronaAPIService.class);

        RequestBody rbFb = RequestBody.create(MediaType.parse("text/plain"), facebookId);
        RequestBody rbMatch = RequestBody.create(MediaType.parse("text/plain"), Integer.toString(matchId));
        RequestBody rbTeam = RequestBody.create(MediaType.parse("text/plain"), Integer.toString(teamId));
        RequestBody rblatitude = RequestBody.create(MediaType.parse("text/plain"), Double.toString(latitude));
        RequestBody rblongitude = RequestBody.create(MediaType.parse("text/plain"), Double.toString(longitude));
        HashMap<String, RequestBody> map = new HashMap<>();
        map.put("facebook_id", rbFb);
        map.put("match_id", rbMatch);
        map.put("team_id", rbTeam);
        map.put("latitude", rblatitude);
        map.put("longitude", rblongitude);
        MultipartBody.Part photoPart = prepareFilePart("photo", photo);
        Call<PuntosPorCompartir> call = coronaAPIService.sharePhoto(map, photoPart);
        call.enqueue(new Callback<PuntosPorCompartir>() {
            @Override
            public void onResponse(Call<PuntosPorCompartir> call, Response<PuntosPorCompartir> response) {
                action.call(response);
            }

            @Override
            public void onFailure(Call<PuntosPorCompartir> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    @Override
    public void sharePhoto(String facebookId, int matchId, int teamId, double latitude, double longitude, File photo, Action1<Response<PuntosPorCompartir>> action) {
        CoronaAPIService coronaAPIService = retrofit.create(CoronaAPIService.class);
        RequestBody rbFb = RequestBody.create(MediaType.parse("text/plain"), facebookId);
        RequestBody rbMatch = RequestBody.create(MediaType.parse("text/plain"), Integer.toString(matchId));
        RequestBody rbTeam = RequestBody.create(MediaType.parse("text/plain"), Integer.toString(teamId));
        RequestBody rblatitude = RequestBody.create(MediaType.parse("text/plain"), Double.toString(latitude));
        RequestBody rblongitude = RequestBody.create(MediaType.parse("text/plain"), Double.toString(longitude));
        HashMap<String, RequestBody> map = new HashMap<>();
        map.put("facebook_id", rbFb);
        map.put("match_id", rbMatch);
        map.put("team_id", rbTeam);
        map.put("latitude", rblatitude);
        map.put("longitude", rblongitude);
        MultipartBody.Part photoPart = prepareFilePart("photo", photo);
        Call<PuntosPorCompartir> call = coronaAPIService.sharePhoto(map, photoPart);
        call.enqueue(new Callback<PuntosPorCompartir>() {
            @Override
            public void onResponse(Call<PuntosPorCompartir> call, Response<PuntosPorCompartir> response) {
                action.call(response);
            }

            @Override
            public void onFailure(Call<PuntosPorCompartir> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    @Override
    public void confirmPostPhoto(String facebookId, int matchId, int teamId, String postId, Action1<Response> action) {
        CoronaAPIService coronaAPIService = retrofit.create(CoronaAPIService.class);
        Call<ResponseBody> call = coronaAPIService.confirmPostPhoto(facebookId, matchId, teamId, postId);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                action.call(response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
                if(t instanceof UnknownHostException){
                    //notInternet();
                }
            }
        });
    }

    MultipartBody.Part prepareFilePart(String partName, byte []photo){
        RequestBody requestBody = RequestBody.create(MediaType.parse("image/jpeg"), photo);
        return MultipartBody.Part.createFormData(partName, "photo.jpeg", requestBody);
    }

    MultipartBody.Part prepareFilePart(String partName, File photo){
        RequestBody requestBody = RequestBody.create(MediaType.parse("image/jpg"), photo);
        return MultipartBody.Part.createFormData(partName, "photo.jpg", requestBody);
    }



    private void notInternet(){
        /*
        if(context != null){
            Intent intent = new Intent();
            intent.setAction(MainActivity.NotificationNoConnected.PROCESS_DISCONECTED_NETWORK);
            context.sendBroadcast(intent);
        }
        */
    }
}
