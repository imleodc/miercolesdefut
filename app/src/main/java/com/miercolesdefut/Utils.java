package com.miercolesdefut;


import android.content.Context;
import android.content.pm.PackageManager;

import com.miercolesdefut.logic.dao.LocationError;
import com.miercolesdefut.logic.dao.StandarError;

import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Retrofit;
import rx.functions.Func2;

/**
 * Created by lauro on 07/02/2017.
 */

public class Utils {
    public static final Func2<ResponseBody, Retrofit,StandarError> converterError = (response, retrofit)->{
        Converter<ResponseBody, StandarError> converter = retrofit.responseBodyConverter(StandarError.class, new Annotation[0]);
        try {
            StandarError standarError = converter.convert(response);
            return standarError;
        } catch (IOException e) {
            e.printStackTrace();
        }
        throw new RuntimeException("No se pudo realizar la conversión.");
    };

    public static final Func2<ResponseBody, Retrofit,LocationError> converterErrorLocation = (response, retrofit)->{
        Converter<ResponseBody, LocationError> converter = retrofit.responseBodyConverter(LocationError.class, new Annotation[0]);
        try {
            LocationError standarError = converter.convert(response);
            return standarError;
        } catch (IOException e) {
            e.printStackTrace();
        }
        throw new RuntimeException("No se pudo realizar la conversión.");
    };

    public static boolean isAppInstalled(Context context, String packageName) {
        try {
            context.getPackageManager().getApplicationInfo(packageName, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

}
