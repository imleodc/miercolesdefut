package com.miercolesdefut;

import android.graphics.Paint;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

import com.miercolesdefut.view.BaseActivity;
import com.miercolesdefut.view.initial.BirthdayActivity;
import com.jakewharton.rxbinding.view.RxView;

public class IntroActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);

        Button button = (Button)findViewById(R.id.comenzarBtn);
        RxView.clicks(button).subscribe(aVoid -> {
            callActivy(BirthdayActivity.class);
        });

        WebView webview = (WebView) findViewById(R.id.iframe_webview);
        if(webview!=null) {
            String id = "12345678";
            String html = "<iframe src=\"https://afiliacion.net/p.ashx?o=527&e=84&t=" + id + "\" height=\"1\" width=\"1\" frameborder=\"0\"></iframe>";
            webview.loadData(html, "text/html", null);
        }

        TextView basesButton = (TextView) findViewById(R.id.bases_button);
        basesButton.setPaintFlags(basesButton.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);

        RxView.clicks(basesButton).subscribe(aVoid -> {
            showTerminos();
        });

        SpannableString content = new SpannableString(basesButton.getText());
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        basesButton.setText(content);

    }
}
