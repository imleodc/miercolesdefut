package layout;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.miercolesdefut.R;
import com.miercolesdefut.logic.model.rxbux.App;
import com.miercolesdefut.view.buissnes.teams.ProxPartidosFragment;

public class ProximoPartidoIntermediateFragment extends Fragment {

    private static final String INIT_SCREEN = "matches";

    public ProximoPartidoIntermediateFragment() {
    }

    public static ProximoPartidoIntermediateFragment newInstance() {
        ProximoPartidoIntermediateFragment fragment = new ProximoPartidoIntermediateFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Tracker mTracker = ((App) getActivity().getApplication()).getDefaultTracker();
        mTracker.setScreenName(INIT_SCREEN);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_proximo_partido_intermediate, container, false);
        getFragmentManager().beginTransaction().replace(R.id.proxPartIntFrame, ProxPartidosFragment.newInstance(true)).commit();
                //proxPartIntFrameproxPartIntFrameproxPartIntFrame
        return view;
    }
}
